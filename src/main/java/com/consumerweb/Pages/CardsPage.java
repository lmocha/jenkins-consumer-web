package com.consumerweb.Pages;

import org.testng.Assert;
import com.aventstack.extentreports.Status;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
public class CardsPage extends ConsumerWebBaseClass{
	
	public CardsPage(WebDriver driver){
        this.driver = driver;
        //  This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 15);   
    }
	public void clickOnCards() throws InterruptedException {
		String cardsXpath =  "//*[text()='Cards']";
		clickElement(By.xpath(cardsXpath));
		Thread.sleep(2000);
	}

	
	public void assertOncardsPage() {
		String header ="//*[@id='app-container-body']//div[contains(text(), 'Cards')]";
		String txt = "Cards";
		assertCorrectText(By.xpath(header), txt);
		
	}
	
	public void assertCardLimitsPage(String labeltxt) {
		String header ="//*[@id='app-container-body']//div[contains(text(), '"+labeltxt+"')]";
		//String txt = "Cards";
		assertCorrectText(By.xpath(header), labeltxt);
		
	}
	
	public void assertCardLabels(String labeltxt) {
		String header ="//div[contains(text(), '"+labeltxt+"')]";
		//String txt = "Cards";
		assertCorrectText(By.xpath(header), labeltxt);
		
	}
	public void assertLimitErrors(String labeltxt) {
		String header ="//*[@id=\"box-container\"]//span[contains(text(), 'The amount exceeds the limit of €115,000.00')]";
		//String txt = "Cards";
		assertCorrectText(By.xpath(header), labeltxt);
		
	}
	
	public void clickCardLink() {
		
		String cardsLink="//*[@id=\"root\"]//span[contains(text(),'Cards')]";
		clickElement(By.xpath(cardsLink));
		
	}
	// select the first card in the list of cards
	public void selectCard() {
		String selectcardXpath = "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		clickOnARandomULListElement(By.xpath(selectcardXpath));
	}
	//  click the card image
	public void clickcardImage() {
		String cardImage = "//*[@class = \"card-image\"]";
		clickElement(By.xpath(cardImage));
	}
	
	public void clickAllAcountsLink() {
		String cardImage = "//*[@id=\"app-container-body\"]//span[contains(text(), 'All accounts')] ";
		clickElement(By.xpath(cardImage));
	}
	
	public void asserAccountLabelPresent() {
		String accountName= "//div/ul/li[2]/div/div/div[2]/div[1]/div";
		assertElementIsDisplayed(By.xpath(accountName));
	}
	
	public void AssertallAccountsLable(String labeltxt) {
		String correctlabel= "//div[contains(text(),'Cards from all accounts')]";
		assertCorrectText(By.xpath(correctlabel), labeltxt);
		
	}
	// filters cards
	public void FiltreAccounts(String searchName) {
		String link = "//*[@id=\"app-container-body\"]//div[contains(text(), 'Filter')] ";
		clickElement(By.xpath(link));
		String inputName = "name";
		sendText(By.name(inputName), searchName);
		
	}
	
	// to check any checkbox
	public void clickCheckBox(String CheckboxName) {
		String checkbox= "//span[contains(text(), '"+CheckboxName+"')] ";
		clickElement(By.xpath(checkbox));
		
	}
	// check card status
	public void AssertalCardStatus(String labeltxt) {
		String correctlabel= "//*[@id=\"app-container-body\"]//span[contains(text(), '"+labeltxt+"')]";
		assertCorrectText(By.xpath(correctlabel), labeltxt);

		
	}
	
	public void clickShowPin() {
		String button = "//*[@id=\"app-container-body\"]//div[contains(text(), 'Show PIN')]";
		clickElement(By.xpath(button));
	}
	
	public void checkcardInfo(String cardTypeName, String Status, String OwnerName) {
		String cardType="//*[@id=\"box-container\"]//div[contains(text(), '"+cardTypeName+"')]";
		assertCorrectText(By.xpath(cardType), cardTypeName);
		String cardStatus ="//*[@id=\"box-container\"]//p[contains(text(), '"+Status+"')]";
		assertCorrectText(By.xpath(cardStatus), Status);
		String cardOwnertxt = "//*[@id=\"box-container\"]//div[contains(text(), '"+OwnerName+"')]";
		assertCorrectText(By.xpath(cardOwnertxt), Status);
		
	}
	
	public void assertCardInfoDisplayed() {
		String date="//*[@id=\"box-container\"]//div[contains(text(), 'Issue date')]";
		String issueDate = "Issue date";
		assertCorrectText(By.xpath(date), issueDate);
		String label ="//*[@id=\"box-container\"]//div[contains(text(), 'Recipient')]";
		String recipientLabel = "Recipient address";
		assertCorrectText(By.xpath(label), recipientLabel);
		String elm = "//*[@id=\"box-container\"]//div[contains(text(), 'Carrier')]";
		String carrierlabel = "Carrier";
		assertCorrectText(By.xpath(elm), carrierlabel);
	}
	
	public void selectparticulerCard(String cardName) {
		String card = "//*[@id=\"box-container\"]//div[contains(text(), '"+cardName+"')]";
		clickElement(By.xpath(card));
	}
	
	// enter security key
	public void enterSecurityKey(String secCode) {
		String keybox= "//input[@name=\"code\"]";
		sendText(By.xpath(keybox), secCode);
		
	}
	
	public void validateErroMsg(String errorMsg) {
		String errorlabel= "//form/div[1]/span";
		assertCorrectText(By.xpath(errorlabel), errorMsg);
		
	}
	public void clickOnDialogBox() {
		String errorlabel= "//div[contains(text(), 'Enter your security code')]";
		clickElement(By.xpath(errorlabel));
		
	}
	
	public void clickMoreDetails() {
		String elemnt= "//*[text()='More Details']";
		clickElement(By.xpath(elemnt));
	}
	
	public void clickMoreBTN() {
		String elemnt= "//*[@id=\"app-container-body\"]//div[contains(text(), 'More')]";
		clickElement(By.xpath(elemnt));
	}
	
	public void clickModifyLimits() {
		String elemnt= "//*[text()='Modify limits']";
		clickElement(By.xpath(elemnt));
	}
	//*[@id="app-container-body"]//div[contains(text(), 'Modify limits')]
	
	public void assertMyCardslist() {
		String label = "//*[@id=\"box-container\"]//div[contains(text(), 'My cards')]";
		String txt = "My cards";
		assertCorrectText(By.xpath(label), txt);
	}
	
	public void clickOnIssueANewCardButton() {
		String issueANewCard = "//*[text()='Issue new card']";
		clickElement(By.xpath(issueANewCard));
	}
	
	public void clickOnPhysicalComboBox() {
		String physcalbox = "//*[@id=\"box-container\"]//div[contains(text(),'Physical')]";
		clickElement(By.xpath(physcalbox));
	}
	
	public void clickNext(String btnName ) {
		String nextButton = "//*[text()='"+btnName+"']";
		clickElement(By.xpath(nextButton));
	}
	
	public void confirmButton () throws InterruptedException {
		String confirmBTN= "//*[text()='Confirm']";
		clickElement(By.xpath(confirmBTN));
		Thread.sleep(3000);
	}
	
	public void enterCardName() {
		String textBox = "//*[@id=\'box-container\']//input";
		String newName = "Auto Myles";
		sendText(By.xpath(textBox), newName);
		//testtextarea(newName);
	}
	
	
	public void enterDeliveryName(String deliveryNm) {
		String deliveryName = "deliveryName";
		sendText(By.name(deliveryName), deliveryNm);
	}
	
	public void enterAddressLine(String addrLn) {
		String AddressLine1 = "addressline1";
		sendText(By.name(AddressLine1), addrLn);
	}
	
	public void enterPostalCode(String postalCod) {
		String postalCode = "postalCode";
		sendText(By.name(postalCode),postalCod);
	}
	
	public void enterRegion(String reg) {
		String region ="region";
		sendText(By.name(region),reg);
	}
	
	public void enterCity(String cty) {
		String city = "city";
		sendText(By.name(city),cty);
	}
	
	public void enterCountry(String Country) {
		String country = "//*[@id=\"box-container\"]//select";
		sendText(By.xpath(country),Country);
		
	}
	public void confirmcard () {
		String confirmBTN= "//form//button[contains(text(),'Confirm')]";
		clickElement(By.xpath(confirmBTN));
	}
	public void selectMore() {
		String moreBTN= "button-text";
		clickElement(By.className(moreBTN));
	}
	
	public void selectCancle() {
		String confirmBTN= "//*[@id=\"app-container-body\"]//div[contains(text(),'Cancel card')]";
		clickElement(By.xpath(confirmBTN));
	}
	
	public void selectModify() {
		String modifyBTN= "//*[@id=\"box-container\"]/form/div[3]/div/div/div/div[2]/div[2]";
		clickElement(By.xpath(modifyBTN));
		String txt = "Modify";
		assertCorrectText(By.xpath(modifyBTN), txt);
	}
	
	public void selectModifycardHolder() {
		String modify= "//*[@id=\"box-container\"]/form/div[2]/div/div/div[2]/div";
		clickElement(By.xpath(modify));
	}
	public void assertCardHolderPoppage() {
		String pageLabel = "//div[contains(text(),'Select')]";
		String txt = "Select a card holder";
		assertCorrectText(By.xpath(pageLabel), txt);
	}
	
	public void enterCardLimits(String limitper,String daily_limits, String monthly_limits) {
		String limit_transaction= "cardLimitTransaction";
		sendText(By.name(limit_transaction),limitper);
		String daily_limit= "cardLimitDaily";
		sendText(By.name(daily_limit),daily_limits);
		String monthly_limit="cardLimitMonthly";
		sendText(By.name(monthly_limit),monthly_limits);
		
	}
	public void refreshBrowser() throws InterruptedException {
//		String closeCardDetailsClassName = "lum-icon-component-fill close_icon clickable";
//		clickElement(By.className(closeCardDetailsClassName));
		driver.navigate().refresh();
		Thread.sleep(3000);
	}
	
	public void tearDown() {
		
		tearDown();
	}
	
}