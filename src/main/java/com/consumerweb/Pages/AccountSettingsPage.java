package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AccountSettingsPage extends ConsumerWebBaseClass{
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement accountSettings;
	Select userRole;

	    
	public AccountSettingsPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	
	public void clickOnAccountSettings() throws InterruptedException {
		String accountSettingsXpath = "(//a[@href='/account-settings']/div)[1]";
		clickElement(By.xpath(accountSettingsXpath));
	}

}
