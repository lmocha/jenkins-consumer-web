package com.consumerweb.Pages;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;


public class ExpensesPage {
	WebDriver driver;
	WebDriverWait wait;
	WebElement expenses,categoryTab,addNewExpenseButton,firstExpense,backButtonOnExpenseDetailPage,firstCategoryItem,backButtonOnFirstCategory,
	firstCategoryExpense,addAttachmentLink,addNotes,addComments,saveButton,sendButton,gbp,currencySelectorSearch,firstCurrencyOption,
	expenseAmount,expenseMerchant,expenseAddress,expenseDate,expenseTime,expenseSaveButton,deleteExpenseButton,confirmButton,uploadButton,
	deleteReceipt,invoiceDetail,isThisAPurchaseInvoiceButton,addNewInvoice,supplierName,companyNumber,postalCode,invoiceNumber,invoiceDate,
	dueDate,addLine,description,lineAmountwithoutVAT,lineVAT,lineAmount,addAlineConfirmButton,discountApplied,totalDiscount,totalWithoutVAT,
	VAT,Total,savePurchaseInvoiceButton,descriptionLocator,withoutVATLocator,lineVATLocator,lineAmountLocator,confirmButtonLocator,deleteInvoice,
	invoiceLocator,personOneExpenseOne,editAnExpense,uploadAreceipt,scanAreceipt,deleteAttachmentLocator,deleteAttachment,analysis;
	Select drpRegCountry;

	    
	public ExpensesPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	public void clickonExpenses() {
		expenses = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Expenses")));
		expenses.click();
		
	}
	public void clickOnAnalysis() {
		analysis = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Analysis")));
		analysis.click();
	}
	public void clickOnCategory() {
		categoryTab = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Category")));
		categoryTab.click();
	}
	//Add new expense
	public void clickOnAddNewExpense() {
		addNewExpenseButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Add new expense')]")));
		addNewExpenseButton.click();
	}
	public void clickOnFirstExpense() {
		firstExpense = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#\\30  > div")));
		firstExpense.click();
	}
	public void clickOnBackButtonOnExpenseDetailPage() {
		backButtonOnExpenseDetailPage = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.lum-back-btn.display-flex.center-y.clickable.m-b-2 > svg")));
		backButtonOnExpenseDetailPage.click();
	}
	public void clickOnFirstCategory() {
		firstCategoryItem = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > ul > li:nth-child(1)")));
		firstCategoryItem.click();
	}
	public void clickOnBackButtonOfFirstCategory() {
		backButtonOnFirstCategory = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.lum-back-btn.display-flex.center-y.clickable.m-b-2 > svg")));
		backButtonOnFirstCategory.click();
	}
	public void clickOnFirstCategoryExpense() {
		firstCategoryExpense = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > ul > li:nth-child(1) > div:nth-child(2) > ul > li")));
		firstCategoryExpense.click();
	}
	public void clickOnUploadAreceipt(String path) {
		addAttachmentLink = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("input[accept^='image/jpg']")));
		addAttachmentLink.sendKeys(path);
		
		//addAttachmentLink.click();
		
	}
	
	public void clickOnUpload() {
		uploadButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[id='modal-confirm-button']")));
		uploadButton.click();
	}
	//#
	public void deleteAreceipt() {
		deleteReceipt = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.m-t-1.info-container > div.display-flex.center-y.item-container > div > div.file-container.lum-semi-bold.clickable.display-flex.center-y > svg")));
		deleteReceipt.click();
	}
	public void addNotes(String notes) {
		addNotes = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.m-t-1.info-container > div.display-flex.column.lum-input-box > input")));
		//addNotes.clear();
		addNotes.sendKeys(notes);
	}
	public void clickOnSave() {
		saveButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Save')]")));
		saveButton.click();
	}
	public void addComments(String comments) {
		// wait.until(ExpectedConditions.elementToBeClickable(By.name("description")));
		addComments = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div.lum-tx-detail-comments.m-b-2 > ul > form > div.display-flex.column.lum-input-box.comment-input-box.m-l-1")));
		System.out.println("The add comments text area is displayed?......"+addComments.isDisplayed());
//		JavascriptExecutor executor = (JavascriptExecutor)driver;
//		executor.executeScript("arguments[0].click();", addComments);
		addComments.click();
		addComments.click();
		addComments.sendKeys(comments);
	}
	public void clickOnSend() {
		sendButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div.lum-tx-detail-comments.m-b-2 > ul > form > button")));
		sendButton.click();
	}
	public void clickOnGBP() {
		gbp = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div > form > div.m-b-1.center-y > div > svg")));
		gbp.click();
	}
	public void selectCurrency(String curre) {
		currencySelectorSearch = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("search-currency")));
		currencySelectorSearch.sendKeys(curre);
		
	}
	public void clickOnFirstCurrencyOption() {
		firstCurrencyOption = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("li.currency-item.display-flex.clickable.center-y")));
		firstCurrencyOption.click();
	}
	public void enterNewExpenseAmount(String amt) {
		expenseAmount = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("total")));
		expenseAmount.sendKeys(amt);
	}
	public void enterNewExpenseMerchant(String mercha) {
		expenseMerchant = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("txTitle")));
		expenseMerchant.sendKeys(mercha);
	}
	public void enterNewExpenseAddress(String addr) {
		expenseAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("address")));
		expenseAddress.sendKeys(addr);
	}
	public void enterNewExpenseDate(String dat) {
		expenseDate = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("date")));
		expenseDate.sendKeys(dat);
		
	}
	public void enterNewExpenseTime(String tm) {
		expenseTime = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("time")));
		expenseTime.sendKeys(tm);
	}
	//By.cssSelector("button[onclick^=addToSelected]")
	public void clickOnExpenseSaveButton() {
		expenseSaveButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div > form > div.display-flex.center-x.m-t-2 > button")));
		expenseSaveButton.click();	
		expenseSaveButton.click();	
	}
	public void clickOnDeleteExpense() {
		deleteExpenseButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.delete-button")));
		deleteExpenseButton.click();
	}
	public void clickOnConfirmButton() {
		confirmButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Confirm')]")));
		confirmButton.click();
	}
	public void editInvoiceDetail() {
		invoiceDetail = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.column-lt-m.info-container > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div.display-flex.item-label.center-y")));
		invoiceDetail.click();
		
	}
	public void toggleTheIsThisAPurchaseInvoiceButton(){
		isThisAPurchaseInvoiceButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div[style= 'position: relative; display: inline-block; text-align: left; opacity: 1; direction: ltr; border-radius: 14px; transition: opacity 0.25s ease 0s; touch-action: none; -webkit-tap-highlight-color: rgba(0, 0, 0, 0); user-select: none;']")));
		isThisAPurchaseInvoiceButton.click();
		
		
	}
	public void clickOnAddNewInvoiceButton() {
		addNewInvoice = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.btn.btn-big.btn-green.lum-invoice-add")));
		addNewInvoice.click();
		
	}
	public void enterSupplierName(String name) {
		supplierName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("supplier")));
		supplierName.sendKeys(name);
		
	}
	public void enterCompanyNumber(String no) {
		companyNumber = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("cif")));
	    companyNumber.sendKeys(no);
	}
	public void enterPostalCode(String postalco) {
		postalCode = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("postalCode")));
		postalCode.sendKeys(postalco);
	}
	public void enterInvoiceNumber(String invNo) {
		invoiceNumber = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("invoiceNumber")));
		invoiceNumber.sendKeys(invNo);
	}
	public void enterInvoiceDate(String invDate) {
		invoiceDate = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("invoiceDate")));
		invoiceDate.sendKeys(invDate);
	}
	public void enterDueDate(String duedat) {
		dueDate = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dueDate")));
		dueDate.sendKeys(duedat);
	}
	public void clickOnaddLine() {
		addLine = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.lum-invoice-form > form > div.display-flex.flex.center-x.m-y-2 > div")));
		addLine.click();
	}
	public void addDescription(String desc) {
		descriptionLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".display-flex.column.lum-input-box.invoice-line-input:nth-child(1) ")));
		description = descriptionLocator.findElement(By.name("description"));
		description.sendKeys(desc);
	}
	public void enterLineAmountWithoutVAT(String lnWithoutvat) {
		withoutVATLocator= wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".display-flex.column.lum-input-box.invoice-line-input:nth-child(2) ")));
		lineAmountwithoutVAT = withoutVATLocator.findElement(By.name("totalWithoutVat"));
		lineAmountwithoutVAT.sendKeys(lnWithoutvat);
	}
	public void enterLineVAT(String lineVA) {
		lineVATLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".display-flex.column.lum-input-box.invoice-line-input:nth-child(3) ")));
		lineVAT = lineVATLocator.findElement(By.name("vatPercentage"));
		lineVAT.sendKeys(lineVA);
	}
	public void enterLineAmount(String lineAmt) {
		lineAmountLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".display-flex.column.lum-input-box.invoice-line-input:nth-child(4) ")));
		lineAmount = lineAmountLocator.findElement(By.name("amount"));
		lineAmount.sendKeys(lineAmt);
	}
	public void clickOnAddLineConfirmButton() {
		confirmButtonLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.display-flex.center-y.center-x.m-t-1.flex")));
		addAlineConfirmButton = confirmButtonLocator.findElement(By.xpath("//button[contains(text(),'Confirm')]"));
		addAlineConfirmButton.click();
	}
	public void enterDiscountApplied(String discApp) {
		discountApplied = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("discountPercentage")));
		discountApplied.sendKeys(discApp);
	}
	public void enterTotalDiscount(String totalDis) {
		totalDiscount = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("discountAmount")));
		totalDiscount.sendKeys(totalDis);
	}
	public void enterTotalWithoutVAT(String totWithoutVAT) {
		totalWithoutVAT = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("totalWithoutVat")));
		totalWithoutVAT.sendKeys(totWithoutVAT);
	}
	public void enterVAT(String vA) {
		VAT = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("vatPercentage")));
		VAT.sendKeys(vA);
	}
	public void enterTotal(String tot) {
		Total = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("amount")));
		Total.sendKeys(tot);
	}
	public void clickOnsavePurchaseInvoiceButtton() {
		savePurchaseInvoiceButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button[type = 'submit']")));
		savePurchaseInvoiceButton.click();
	}
	
	public void clickOnDeleteInvoice() {
		invoiceLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div.p-x-1.display-flex.column > ul > li")));
		deleteInvoice = invoiceLocator.findElement(By.cssSelector("#app-container-body > div > div > div > div > div > div.p-x-1.display-flex.column > ul > li > svg"));
		deleteInvoice.click();
		
	}
	public void clickOnPersonOneExpenseOne() {
		personOneExpenseOne = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > ul > li:nth-child(1) > div:nth-child(2) > ul > li:nth-child(1) > a")));
		personOneExpenseOne.click();
		
	}
	public void clickOnEditAnExpense() {
		editAnExpense = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > svg")));
		editAnExpense.click();
	}
	public void uploadAreceipt(String receiptPath) {
		uploadAreceipt = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div:nth-child(3) > div.flex.d-none.d-md-inline-block > div:nth-child(1) > section > div > input[type=file]")));
		uploadAreceipt.sendKeys(receiptPath);
		
	}
	public void scanAreceipt(String receiptPath) {
		scanAreceipt = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div:nth-child(3) > div.flex.d-none.d-md-inline-block > div:nth-child(2) > section > div > input[type=file]")));
		scanAreceipt.sendKeys(receiptPath);
	}
	public void clickOnDeleteReceipt() {
		deleteAttachmentLocator = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div > div:nth-child(3) > div.preview-image.d-none.d-md-inline-block > div")));
		deleteAttachment = deleteAttachmentLocator.findElement(By.cssSelector("#app-container-body > div > div > div > div > div > div:nth-child(3) > div.preview-image.d-none.d-md-inline-block > div > svg"));
		deleteAttachment.click();
	}

}
