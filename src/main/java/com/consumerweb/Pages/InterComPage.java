package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class InterComPage {
	WebDriver driver;
	WebDriverWait wait;
	WebElement interCom,searchOurArticlestextBox,sendButton,searchHolder;
	Select drpRegCountry;

	    
	public InterComPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	public void clickonInterCom() {
		interCom = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.intercom-lightweight-app > div")));
		interCom.click();
	}
	
	public void startAconversation(String message) {
		 //     div.intercom-1qgdrjz.ea4em4b0
		searchHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.intercom-1qgdrjz.ea4em4b0")));
		searchOurArticlestextBox = searchHolder.findElement(By.xpath("//input[@placeholder='Search our articles']"));  
		searchOurArticlestextBox.sendKeys(message);	
	}
	public void clickOnSend() {
		sendButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.intercom-1s2hu9p.e1r6wzsz0")));
		sendButton.click();
	}

	
	
}
