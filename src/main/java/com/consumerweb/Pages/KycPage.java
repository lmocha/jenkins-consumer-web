package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class KycPage {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement cont;
	Select drpSelectAdoc;

	    
	public KycPage(WebDriver driver){

	        this.driver = driver;
	        //This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 10);
	        
	    }     
	public void clickOnContinue() {
		cont = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(2) > div > button")));
		cont.click();
		
	}
	public void selectPassport() {
		drpSelectAdoc = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(3) > div > form > div.display-flex.column.lum-input-box.m-t-2 > div > select"))));
		drpSelectAdoc.selectByVisibleText("Passport");
	}

}
