package com.consumerweb.Pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TruNarrativePage {	
	WebDriver driver;
	WebDriverWait wait;
	WebElement userName,nextBtn,login,passWord,referrals,searchTextBox,assignBtn,assignToMe,assignReferralHolder,multipleSessionsContinue,assigneeReferral1,div2,iframe,holderDiv,holderDiv1,
	holderDiv1_2,holderDiv2,holderDiv3,holderDiv4,teamDrp,seachSelectAteam,memberDrp,memberSearch,startButton,acceptButton,markAsAcceptButton;
	Select drpTeam,drpMember;
	Boolean referralsButtonIsDisplayed;
	String test,assignReferralTitle;

	    
	public TruNarrativePage(WebDriver driver){

	        this.driver = driver;
	        //This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 10);
	        
	    }     
	public void enterUsername(String uName) {
		userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("wt1$WebPatterns_wt8$block$wtUsername$wtUserNameInput")));
		userName.sendKeys(uName);
		
	}
	public void clickOnNextButton() {
		nextBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("wt1$WebPatterns_wt8$block$wtAction$wtLoginButton2")));
		nextBtn.click();		
	}
	
	public void enterPassword(String pass) {
		passWord = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("wt1$WebPatterns_wt8$block$wtPassword$wtPasswordInput")));
		passWord.sendKeys(pass);
		
	}
	public void clickOnLoginButton() {
		login = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("wt1$WebPatterns_wt8$block$wtAction$wtLoginButton")));
		login.click();
	}
	public void clickOnReferrals() {
		List<WebElement> referralList = driver.findElements(By.linkText("Referrals"));
		//referralsButtonIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Referrals"))).isDisplayed();
		//referrals = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Referrals")));
		if(referralList.isEmpty()) {
			multipleSessionsContinue = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#wt15_WebPatterns_wt27_block_wtAction_wtAction_wt4")));
			multipleSessionsContinue.click();
			referrals = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Referrals")));
			referrals.click();
		}else {
			referrals = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Referrals")));
			referrals.click();
		}	
		
		
	}
	public void searchByRefName(String ref) {
		searchTextBox = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("TruNarrative_Theme_wt2$block$wtMainContent$TruWebPatterns_wtFilter_Left$block$WebPatterns_wt12$block$wtSearch_wrapper$wt14$wt11$wtControl$wtControl$WebPatterns_wt10$block$wtActionsLeft$wtSearchInput")));
		searchTextBox.sendKeys(ref,Keys.ENTER);
		
	}
	public void clickOnAssignButton() throws InterruptedException {
		assignBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("TruNarrative_Theme_wt2_block_wtMainContent_wtTable_ctl03_wtButtonAssign")));
		assignBtn.click();
	}
	public void clickOnAssignToMe() {
		assignToMe = wait.until(ExpectedConditions.presenceOfElementLocated(By.id("TruNarrative_Theme_wt7_block_WebPatterns_wt7_block_wtMainContent_wtMainContent_TruWebPatterns_wt58_block_wtContent_wt19")));
		assignToMe.click();
	}
	public void selectAteam(String team) throws Exception {
		assignReferralTitle = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div.os-internal-ui-dialog.os-internal-ui-widget.os-internal-ui-widget-content.os-internal-ui-corner-all.os-internal-Popup.os-internal-ui-draggable > div.os-internal-ui-dialog-titlebar.os-internal-ui-widget-header.os-internal-ui-corner-all.os-internal-ui-helper-clearfix"))).getText();
		System.out.println("*******************************"+assignReferralTitle);
		driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
		holderDiv = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("TruNarrative_Theme_wt7$block$WebPatterns_wt7$block$wtMainContent$wtMainContent$TruWebPatterns_wt58$block$wtContent$wtSelectform")));
		holderDiv1 = holderDiv.findElement(By.cssSelector("#TruNarrative_Theme_wt7_block_WebPatterns_wt7_block_wtMainContent_wtMainContent_TruWebPatterns_wt58_block_wtContent_wtSelectform > div:nth-child(1)"));
		teamDrp = holderDiv.findElement(By.id("TruNarrative_Theme_wt7_block_WebPatterns_wt7_block_wtMainContent_wtMainContent_TruWebPatterns_wt58_block_wtContent_TruWebPatterns_wt62_block_wtControl"));
		teamDrp.click();
		seachSelectAteam = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > span > span > span.select2-search.select2-search--dropdown > input")));
		seachSelectAteam.sendKeys(team,Keys.RETURN);
		
		
		
	}
	public void clickOnStart() {
		startButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Start")));
		startButton.click();
	}
	public void clickOnAccept() {
		acceptButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Accept")));
		acceptButton.click();
	}
	public void clickOnMarkAsAccept() {
		driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
		markAsAcceptButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Mark as accept")));
		markAsAcceptButton.click();
	}
	public void selectAmember() {
		//holderDiv1_2 = holderDiv.findElement(By.cssSelector("#TruNarrative_Theme_wt7_block_WebPatterns_wt7_block_wtMainContent_wtMainContent_TruWebPatterns_wt58_block_wtContent_wtSelectform > div:nth-child(2)"));
		//memberDrp = holderDiv1_2.findElement(By.id("TruNarrative_Theme_wt7_block_WebPatterns_wt7_block_wtMainContent_wtMainContent_TruWebPatterns_wt58_block_wtContent_TruWebPatterns_wt69_block_wtControl"));
		//memberDrp.click();
		//TruNarrative_Theme_wt7$block$WebPatterns_wt7$block$wtMainContent$wtMainContent$TruWebPatterns_wt58$block$wtContent$TruWebPatterns_wt69$block$wtControl$wtcb_User
		//memberSearch = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > span > span > span.select2-search.select2-search--dropdown > input")));
		//memberSearch.sendKeys(member);
		//drpMember = new Select(memberDrp);
		//drpMember.selectByVisibleText("Selpha Atemba");
       
		
	}
	
	

}
