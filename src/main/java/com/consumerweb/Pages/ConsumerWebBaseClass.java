package com.consumerweb.Pages;


import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import javax.imageio.ImageIO;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



public class ConsumerWebBaseClass {
	
	
	public Properties OR = new Properties();
	//builds a new report using the html template 
    public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    //helps to generate the logs in test report.
    public static ExtentTest test;
    public static WebDriver driver;
	public static WebDriverWait wait;
	public static String sourceAcc,destAcc;
	public static Boolean notificationsButtonIsDisplayed,userButtonIsDisplayed,welcomeUserText;
	WebElement userName,passWord,confirmPassword,securityCode,endUserAgreement,AccountTerms,cardHolderTerms,registerBtn,goToTheBottomButton,iHaveReadTnCsButton,confirmButton,acceptCookies;
	Select drpSelectAdoc;
	LoginPage loginPage;
	
//**************DEV******************************************
	
//===============TOQIO=======================================
//	public static String username = "sel-consumer-dev";
//    public static String password = "IncandescentRabbit6253";
	
	
//**************TEST******************************************
	
//==============TOQIO=====================
	public static String username = "selpha123";
	public static String password = "Olivo2021";
//	
//==============TOQIO-FX=====================
//	public static String username = "fx-consumer-test";
//	public static String password = "Olivo2019";
	
//==============Syncom======================
//	public static String username = "syncom.consumer";
//    public static String password = "Olivo2021";
	
//**************LIVE******************************************

//=====================TravelFx==================
//	public static String username = "TestTravelFXConsumer";
//    public static String password = "Olivo2019";
	
	//============TOQIO=============
//	public static String username = "sel-consumer-live";
//	public static String password = "Olivo2019";
	
	public  String driverLocation= System.getProperty("user.dir") + "/drivers/chromedriver";
	public static Logger log=Logger.getLogger(ConsumerWebBaseClass.class.getName());
    
	
	
    
    @BeforeSuite
    public void setUp() {
    	// initialize the HtmlReporter
    	htmlReporter = new ExtentHtmlReporter(System.getProperty("user.dir") + "/test-output/testReport.html");
    	//initialize ExtentReports and attach the HtmlReporter
    	extent = new ExtentReports();
    	extent.attachReporter(htmlReporter);
    	 //To add system or environment info by using the setSystemInfo method //*[@id="root"]/div[2]/div/div/form/button       #root > div.display-flex.flex > div > div > form > button     
    	extent.setSystemInfo("Host Name", "Mac");
    	extent.setSystemInfo("Environment", "TEST");
    	extent.setSystemInfo("User Name", "Selpha");
    	extent.setSystemInfo("Browser", "Chrome");
    	//configuration items to change the look and feel
        //add content, manage tests etc
    	htmlReporter.config().setChartVisibilityOnOpen(true);
    	htmlReporter.config().setDocumentTitle("CONSUMER Web Automation Test Report for TEST environment");
    	htmlReporter.config().setReportName("Test Report");
    	htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
    	htmlReporter.config().setTheme(Theme.STANDARD);
    	htmlReporter.config().setTimeStampFormat("EEEE, MMMM dd, yyyy, hh:mm a '('zzz')'");
    	
    }
   
    @AfterMethod
    public void getResult(ITestResult result) throws IOException {
        if(result.getStatus() == ITestResult.FAILURE) {
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName()+" Test case FAILED due to below issues:", ExtentColor.RED));
//            String feature = getClass().getName();
            String feature = result.getName().toUpperCase();
            String screenShotPath = capture(driver, feature);
            test.fail(result.getThrowable().getMessage());
            test.fail("Snapshot below: " + test.addScreenCaptureFromPath(screenShotPath));

        }
        else if(result.getStatus() == ITestResult.SUCCESS) {
            test.log(Status.PASS, MarkupHelper.createLabel(result.getName()+" PASSED ", ExtentColor.GREEN));
        }
        else {
            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName()+" SKIPPED ", ExtentColor.ORANGE));
            test.skip(result.getThrowable());
        }
    }
     
    @AfterTest
    public void tearDown() {
    	//to write or update test information to reporter
        extent.flush();
    }
    
  public void init() throws Exception {
      loadData();
      String log4jConfPath = System.getProperty("user.dir") + "/resources/log4j.properties";
      PropertyConfigurator.configure(log4jConfPath);
      System.out.println(OR.getProperty("browser"));
      selectBrowser(OR.getProperty("browser"));
      getUrl(OR.getProperty("url"));
      wait = new WebDriverWait(driver, 10);
//      Thread.sleep(1500);
//			screen.click(closeThis);
//		Thread.sleep(1500);
  }
  
  public static String capture(WebDriver driver,String screenShotName) throws IOException{
      TakesScreenshot ts = (TakesScreenshot)driver;
      File source = ts.getScreenshotAs(OutputType.FILE);
      String dest = System.getProperty("user.dir") +"/ErrorScreenshots/"+screenShotName+".png";
      File destination = new File(dest);
      FileUtils.copyFile(source, destination);        
                   
      return dest;
  }
  
  public void loadData() throws IOException {
   File file = new File(
   System.getProperty("user.dir") + "/resources/configurations.properties");
   FileInputStream f = new FileInputStream(file);
   OR.load(f);
   
  }
  
  public void selectBrowser(String browser) {
	  System.out.println(System.getProperty("os.name"));
	  if (System.getProperty("os.name").contains("Window")) {
		  if (browser.equalsIgnoreCase("chrome")) {
			  System.out.println(System.getProperty("user.dir"));
			  //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
			  ChromeOptions options = new ChromeOptions();
				options.addArguments("--allow-file-access-from-files",
//						"--headless",
			            "--use-fake-ui-for-media-stream","--window-size=1920,1200",
			            "--allow-file-access",
			            "--use-file-for-fake-audio-capture=D:\\PATH\\TO\\WAV\\xxx.wav",
			            "--use-fake-device-for-media-stream");
				driver = new ChromeDriver(options);
			
				//driver = new ChromeDriver();
//        ChromeOptions chromeOptions = new ChromeOptions();
//        chromeOptions.addArguments("--headless");
//        driver = new ChromeDriver(chromeOptions);
//		driver.manage().window().maximize();
        // driver = new EventFiringWebDriver(dr);
        // eventListener = new WebEventListener();
        // driver.register(eventListener);
			  driver=new ChromeDriver();
			  System.out.println("The driver is......."+driver.toString());
    } else if (browser.equalsIgnoreCase("firefox")) {
        System.out.println(System.getProperty("user.dir"));
        System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "/drivers/geckodriver");
        driver = new FirefoxDriver();
        // driver = new EventFiringWebDriver(dr);

        // driver.register(eventListener);
        // setDriver(driver);
    }
		  } else if (System.getProperty("os.name").contains("Mac")) {
			  if (browser.equalsIgnoreCase("chrome")) {
				  System.out.println(System.getProperty("user.dir"));
				  //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("--allow-file-access-from-files",
//							"--headless",
				            "--use-fake-ui-for-media-stream","--window-size=1920,1200",
				            "--allow-file-access",
				            "--use-file-for-fake-audio-capture=D:\\PATH\\TO\\WAV\\xxx.wav",
				            "--use-fake-device-for-media-stream");
					driver = new ChromeDriver(options);
				  System.out.println("The driver is...."+driver.toString());
				  //     '--headless'
        // driver = new EventFiringWebDriver(dr);
        // eventListener = new WebEventListener();
        // driver.register(eventListener); 
				  } else if (browser.equalsIgnoreCase("firefox")) {
					  System.out.println(System.getProperty("user.dir"));
					  System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "/drivers/geckodriver");
					  driver = new FirefoxDriver();
        // driver = new EventFiringWebDriver(dr);

        // driver.register(eventListener);
        // setDriver(driver);
                 } else if (browser.equalsIgnoreCase("safari")) {
		            System.out.println(System.getProperty("user.dir"));
		            System.setProperty("webdriver.gecko.driver",System.getProperty("user.dir") + "/drivers/geckodriver");
		            driver = new SafariDriver();
}
			  }
}
  public String getUrl(String url) {
	  log.info("navigating to :-" + url);
      getDriver();
      driver.get(url);
      driver.manage().window().maximize();
      return url;
      //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
}
  
  
//to connect to webdriver
 public static WebDriver getDriver(){
    if(driver==null){
       //System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
       driver=new ChromeDriver();
    }
    return driver;

}

    
  
    public void loginToGmailLetsCreateYourLUMAccount() throws Exception{
    	ChromeOptions options = new ChromeOptions();
    	//fake compliance settings
	    options.addArguments("use-fake-ui-for-media-stream", "use-fake-device-for-media-stream" );
    	driver =  new ChromeDriver(options);
		//maximize the browser
		driver.manage().window().maximize();
    	String url = "https://accounts.google.com/signin";
    	driver.get(url);
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
    	WebElement email_phone = driver.findElement(By.xpath("//input[@id='identifierId']"));
    	email_phone.sendKeys("selpharr@gmail.com");
    	driver.findElement(By.id("identifierNext")).click();
    	WebElement password = driver.findElement(By.xpath("//input[@name='password']"));
    	WebDriverWait wait = new WebDriverWait(driver, 20);
    	wait.until(ExpectedConditions.elementToBeClickable(password));
    	password.sendKeys("Olivo2020");
    	driver.findElement(By.id("passwordNext")).click();
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
    	String x="//*[@id=\"gb\"]/div[2]/div[3]/div[2]/iframe";
    	driver.switchTo().defaultContent(); // you are now outside both frames
    	//WebDriverWait wait1 = new WebDriverWait(driver, 5);
    	driver.switchTo().frame(driver.findElement(By.xpath(x)));
    	//Account xpath
    	String account="//*[contains(text(), 'Gmail')]";
    	//driver.switchTo().frame(x);
    	driver.findElement(By.xpath(account)).click(); 
    	System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
    	// Mailer name for which i want to check do i have an email in my inbox 
    	String MyMailer = "support";
    	List<WebElement> a = driver.findElements(By.xpath("//*[@class='yW']/span"));
        System.out.println(a.size());
        for (int i = 0; i < a.size(); i++) {
            System.out.println(a.get(i).getText());
            if (a.get(i).getText().equals(MyMailer)) //to click on a specific mail.
                {                                           
                a.get(i).click();
                System.out.println(driver.getTitle());
                System.out.println(driver.getCurrentUrl());
                break;
            }
            else {
            	System.out.println("Email not found");
            }
        }
        List <WebElement> alllinks = driver.findElements(By.tagName("a"));
        System.out.println("Number of links found: "+ alllinks.size());
        for(int i=0;i<alllinks.size();i++) {
        	List <WebElement> alllinks1 = driver.findElements(By.tagName("a"));
        	String link=alllinks1.get(i).getText();
        	String link1=link.trim();
        	String link2="Let'S Create Your LUM Account!";
        	if (alllinks1.get(i).getText().trim().equals(link2)) //to click on a specific mail.
            {                                           
        		alllinks1.get(i).click();
        		//System.out.println("link found	"+link);
        		System.out.println(driver.getTitle());
        		System.out.println(driver.getCurrentUrl());
        		break;
            }
        else {
        		System.out.println("Link not found	"+link1);
        }
    }
        
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
        ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs1.get(2));
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        //setUp credentials
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("username"))).sendKeys("test-selpha");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password"))).sendKeys("Olivo2019");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("confirmPassword"))).sendKeys("Olivo2019");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.name("code"))).sendKeys("1234");
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(1) > div > div > svg"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(2) > div > div > svg"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(3) > div > div > svg"))).click();
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > button"))).click();
        
        Thread.sleep(30000);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); 
        ArrayList<String> tabs2 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs2.get(2));
        System.out.println("This should be KYC page");
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
        //click on continue button
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Continue')]"))).click();
        //select passport
        drpSelectAdoc = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(3) > div > form > div.display-flex.column.lum-input-box.m-t-2 > div > select"))));
		drpSelectAdoc.selectByVisibleText("Passport");
		//Click on continue
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(3) > div > form > button"))).click();
		//Add code here ***************
		System.out.println("************************ Going into click here to capture ******************");
		//Click here to capture  #lum-document-capture > div > div > div > div > div > div.styles__ContainerStyled-adgmle-0.koPGBP > div.styles__DetectionModalStyled-adgmle-3.gcXJZx > div:nth-child(1) > div
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#lum-document-capture > div > div > div > div > div > div.styles__ContainerStyled-adgmle-0.koPGBP > div.styles__DetectionModalStyled-adgmle-3.gcXJZx > div:nth-child(1) > div > div"))).click();
		//Click continue Button  
		wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#lum-document-capture > div > div > div > div > div > div > div.styles__DetectionModalStyled-adgmle-3.dqGpTz > div.vd-detection-modal-content.vd-confirmation > div > div.vd-buttons-container > button.vd-button.vd-continue"))).click();
		Thread.sleep(500);
		
    }
    public static void seleniumSetUp() {
    	//System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir") + "/drivers/chromedriver");
    	driver = new ChromeDriver();
	    //maximize the browser
    	driver.manage().window().maximize();
    	wait = new WebDriverWait(driver, 10);
    	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    	driver.get("https://lum-consumer-test.lumapp.io/login");	
	}

   
    public void manageTimeOuts() {
    driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
    }
    
    
    public void switchToNewWindow() {
    	String winHandleBefore = driver.getWindowHandle();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
        
    }
   public boolean isElementPresent(By selector) {
	   manageTimeOuts();
	   return driver.findElements(selector).size()>0;
   }
   
   public String typeOfTransaction(By selector,By selector2) {
	   sourceAcc = wait.until(ExpectedConditions.presenceOfElementLocated(selector)).getText();
	   destAcc = wait.until(ExpectedConditions.presenceOfElementLocated(selector2)).getText();
	   if(sourceAcc.contains("EUR")&& destAcc.contains("EUR")) {
		   return "EUR to EUR";
	   }else if(sourceAcc.contains("GBP")&& destAcc.contains("GBP")) {
		   return "GBP to GBP";
	   }else return sourceAcc+" to "+destAcc;
	   
   }
   public boolean isTheSameCurrency(String type) {
	   return type.contains("EUR to EUR")|| type.contains("GBP to GBP");
   }

   
	public void login() throws InterruptedException {
		System.out.println("Started successfully loggging in a user");
		loginPage=new LoginPage(driver);
		loginPage.enterUserName(username);
		loginPage.enterPassword(password);
		loginPage.clickOnLogin();
		//Cookie banner is shown
		if(isElementPresent(By.cssSelector("div.lum-cookie-banner"))) {
			acceptCookies = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[text()='Accept']")));
			acceptCookies.click();
		}
		if(isElementPresent(By.cssSelector("div.ReactModal__Content.ReactModal__Content--after-open.confirm-modal.lum-termsandconditions-modal"))) {
			goToTheBottomButton= wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Go to the bottom')]")));
			goToTheBottomButton.click();
			Thread.sleep(5000);
			iHaveReadTnCsButton= wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.display-flex.center-y.clickable ")));
			iHaveReadTnCsButton.click();
			confirmButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Confirm')]")));
			confirmButton.click();
		}
		System.out.println("Finished successfully loggging in a user");
	}
	
	
	
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++//

	/* Asset Label is present*/
	public Boolean assertTextPresent(By selector, String txt) {
		String label;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		label=myElement.getAttribute("innerText");
		return label.equals(txt);
		
	}
	
	public void clickOnFirstElementOfUL(By selector) {
		String firstItemXpath = "(//*[@class='tx-list-separator'])[1]"; 
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		myElement.findElement(By.xpath(firstItemXpath)).click();
	}
	
    public String showElementText(By selector) {
    	String label;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		label=myElement.getAttribute("innerText");
    	return label;
    	
    }


	
	public Boolean assertElementIsDisplayed(By selector) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		return myElement.isDisplayed();
		
	}
	
	public Boolean assertElementIsNotEmpty(By selector) {
		String label;
		WebDriverWait wait = new WebDriverWait(driver, 10);
		String myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector)).getText();
		return !myElement.isEmpty();
		
	}
  
	public Boolean assertElementIsNotDisplayed(By selector) {
		return 	!isElementPresent(selector);
	}
	public void listItems(By selector,By selector2) {
		String placeHolderCSS = "div.lum-placeholder.display-flex.column.center-x.center-y ";
		if(isElementPresent(By.cssSelector(placeHolderCSS))) {
			test.log(Status.INFO, "there is no list found");
		}else {
			 List<WebElement> listOfItems =  wait.until(ExpectedConditions.presenceOfElementLocated(selector)).findElements(selector2);
			 test.log(Status.INFO, "This page has ..."+listOfItems.size()+"...items");
	        for (WebElement webElement : listOfItems) {
		            String transactions = webElement.getText();
		            test.log(Status.INFO, transactions);
		        }
		}

	}
	//  //*[@class='reset-list-style lum-section-list']/li/div/ul/li

	public void listULItems(By selector) {
		String placeHolderCSS = "div.lum-placeholder.display-flex.column.center-x.center-y ";
		if(isElementPresent(By.cssSelector(placeHolderCSS))) {
			test.log(Status.INFO, "there is no list found");
		}else {
			 List<WebElement> listOfItems =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(selector));
			 test.log(Status.INFO, "This page has ..."+listOfItems.size()+"...items");
	        for (WebElement webElement : listOfItems) {
		            String transactions = webElement.getText();
		            test.log(Status.INFO, transactions);
		        }
		}

	}
	public boolean listItemsContainsText(By selector,By selector1,String text) {
		 List<WebElement> listOfItems =  wait.until(ExpectedConditions.presenceOfElementLocated(selector)).findElements(selector1);
		 String transactions = null;
		 test.log(Status.INFO, "This page has ..."+listOfItems.size()+"...items");
       for (WebElement webElement : listOfItems) {
	            transactions = webElement.getText();
	            test.log(Status.INFO, transactions);
	        }
       return transactions.contains(text);
	}
	
    public void sendKeysToATextBox(By selector,String text) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		myElement.clear();
		myElement.sendKeys(text);
    	
    }
    public void sendKeysToATextBoxAndClickEnter(By selector,String text) {
  		WebDriverWait wait = new WebDriverWait(driver, 10);
  		WebElement myElement = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
  		myElement.sendKeys(text,Keys.RETURN);
      	
      }
	public void selectFirstItemOnDropdown(By selector) {
		Select dropDown = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(selector)));
		dropDown.selectByIndex(0);
	}
    public void clickOnAnElement( By selector) {
    	WebDriverWait wait = new WebDriverWait(driver, 30);
		WebElement ele = wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		ele.click();
    }
	public void loginWithDifferentRole(String uname,String pass) {
		String usernameTextBx="email";
		String passwordTxtBx="password";
		String loginBtnXPath="//button[text()='Login']";
		wait = new WebDriverWait(driver, 15);
		sendKeysToATextBox(By.name(usernameTextBx),uname);
		sendKeysToATextBox(By.name(passwordTxtBx),pass);
		clickOnAnElement(By.xpath(loginBtnXPath));
	}
	
	/******************************************************************/
	
	
	
	/**
	 * This function accepts a url e.g. https://test.toq.io/login. You will be taken to login page
	 * @param url
	 */
	public void goToURL(String url) {
		
		    driver.get(url);
	}
	
	
	/**
	 * This function accepts a url e.g. https://test.toq.io/login. Login page will be opened in a new tab
	 * @param url
	 */
	public void goToURLInNewTab(String url) {
			
		driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL +"t");
		ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1)); //switches to new tab
		driver.get(url);
		//driver.switchTo().window(tabs.get(0)); // switch back to main screen        
		// driver.get("https://www.news.google.com");
	}
		
	
	/**
	 * This function clicks on the element whose generic path is passed. E.g will click on a login button if the path provided is for login button
	 * @param selector
	 */
	public void clickElement(By selector) {
		
		wait = new WebDriverWait(driver, 20);
		WebElement ele =wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		ele.click();
		
	}
	/*
	 * Generic function that clicks on an element after it becomes visible in the page
	 */
	public void waitForElementToBeVisible(By Selector) {
		
		WebDriverWait wait = new WebDriverWait(driver, 60); 
		WebElement element = wait.until(ExpectedConditions.elementToBeClickable(Selector));
		element.click();
		
	}
	
	public WebElement waitForElement(By Selector) {
		
		WebDriverWait wait = new WebDriverWait(driver, 60); 
		WebElement element = wait.until(ExpectedConditions.presenceOfElementLocated(Selector));
		return element;
		
	}
	
	/*
	 * Generic function that sends data to a Text box
	 * You pass the path of the selector and the data to be inputed
	 */
	public void sendText(By selector, String txt) {
//		********   we replace this lines with the above method******************
//		WebDriverWait wait = new WebDriverWait(driver, 10);
//		WebElement element= wait.until(ExpectedConditions.presenceOfElementLocated(selector));
		waitForElement(selector).clear();
		waitForElement(selector).sendKeys(txt);
		
			
	}
	/*
	 * Generic function uploads a file/image etc
	 */
	public void uploadFile(By Selector, String filePath) {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		WebElement fileUploadLink = wait.until(ExpectedConditions.presenceOfElementLocated(Selector));
		fileUploadLink.sendKeys(filePath);
		
	}

	/*
	 * This generic function clicks on a random list element
	 * You provide the path to the unOrderedList
	 */
	public String clickOnARandomULListElement(By Selector) {
		try {
			
				WebDriverWait wait = new WebDriverWait(driver, 10);
				List<WebElement> list = driver.findElements(Selector);
				System.out.println(list);
				int sizeUL = list.size();
				System.out.println(sizeUL);
			    // get random number
			    Random random = new Random();
			    int randomEle = random.nextInt(sizeUL);
			    System.out.println(randomEle);
			    // Select the list item
			    list.get(randomEle).click();
			  // return list.get(randomEle).getAttribute("innerText");
			    return "Payment " + randomEle;
			
		}
		
		catch(Exception e) {
			
			return "No entries";
			
		}
		
	}
	
   /*
    *  This function will get Page title
    */
    public String pageTitle() {
    	
    	return driver.getTitle();
    }
    /*
     * This function will get page URL
     */
    public String pageURL() {
    	
        return driver.getCurrentUrl();
    }
    /*
     * This generic function will check all textboxes on a page
     */
    public void checkAllTxtBxs(By Selector) {
    	
    	List <WebElement> allChkbxs = driver.findElements(Selector);
    	System.out.println("Number of links found: "+ allChkbxs.size());
    	
    	if (allChkbxs.size()>0) {
    		
    		for(int i=0;i<allChkbxs.size();i++) {
	    		 
	    		 allChkbxs.get(i).click();
    		}
		   
    	}
	    else {
	    	
	        	System.out.println("Check boxes not found");
	        		
	    }	
    }
	/*
	 * 	Enter current date
	 */
	public void enterCurrentDate(By Selector){    
		
		  wait = new WebDriverWait(driver, 20);
		  DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		  LocalDate localDate = LocalDate.now();
		  wait.until(ExpectedConditions.presenceOfElementLocated(Selector));
		  driver.findElement(Selector).sendKeys(dtf.format(localDate));
		  
	}
	/*
	 * This function asserts if text of the element that is contained in the selector is the same as the sting in the variable pagetitle
	 * It returns true or false
	 */
	public boolean assertCorrectText(By Selector, String text) {
		
		wait = new WebDriverWait(driver, 20);
		WebElement txt= wait.until(ExpectedConditions.visibilityOfElementLocated(Selector));
		return txt.getText().toString().contains(text);
	}
	
	
	/*
	 * This function selects a random li element
	 * You pass the selector to get all li elements in the ul and also the visible text of the element you want to click
	 */
	public void randomlySelectListElementByVisibleText(By Selector,String visibleText) {
			
			waitForElement(Selector);
			List<WebElement> list = driver.findElements(Selector);
			System.out.println(list);
			
			for(WebElement option : list)
			   {
			       System.out.println(option.getText()); 
			       
			       if (option.getText().contains(visibleText)) {
			           option.click();
			           break;
			       }
			   }
	}
	/*
	 * Click on an select Element using  visible text
	 */
	public void clickOnSelectUsingVisibleText(By selector,String text) {
		Select country = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(selector)));
		country.selectByVisibleText("text");
	}
	/******************************************************************/
    

    	
}
