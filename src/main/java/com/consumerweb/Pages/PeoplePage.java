package com.consumerweb.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

public class PeoplePage extends ConsumerWebBaseClass {
	WebDriver driver;
	WebDriverWait wait;
	    
	public PeoplePage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 10);   
	    }
	
	public void clickOnPeople() {
		String peopleLinkText = "People";
		clickOnAnElement(By.linkText(peopleLinkText));;
		
	}
	public void showThePeopleList() {
		String titleHolderCSS = "#app-container-body > div > div > div > div > div > div.lum-title-container.column-lt-m";
		String totalEmployeesTitleCSS = "#app-container-body > div > div > div > div > div > div.lum-title-container.column-lt-m > div:nth-child(2) > div.display-flex.total-people-title";
		String listCSS = "#box-container > ul";
		String listXpath = "//*[@id=\"box-container\"]/ul/li";		
		assertTextPresent(By.cssSelector(titleHolderCSS), "People");
		assertTextPresent(By.cssSelector(totalEmployeesTitleCSS),"Total people");
		listItems(By.cssSelector(listCSS),By.xpath(listXpath));      
	}
	public void showInvitedPeople() {
		String listCss = "#box-container > ul > li > div:nth-child(2) > ul";
		String  listXpath = "//*[@id=\"box-container\"]/ul/li/div[2]/ul/li";
		listItems(By.cssSelector(listCss),By.xpath(listXpath));  
		
	}
	public void assertAccountOwnerDescIsDisplayed() {
		String accountOwnerTextCSS = "#box-container > div:nth-child(2) > div.display-flex.lum-fs-14.lum-color-black-70";
		assertTextPresent(By.cssSelector(accountOwnerTextCSS), "By creating an ACCOUNT OWNER beneficiary you will grant this user access to all the functionality, including:\n"
		 		+ "Bank current account\n"
		 		+ "Ordering payments\n"
		 		+ "Card management\n"
		 		+ "Managing people with access");
		}
	public void assertCardHolderDescIsDisplayed() {
		String cardHolderTextCSS = "#box-container > div:nth-child(2) > div.display-flex.lum-fs-14.lum-color-black-70";
		assertTextPresent(By.cssSelector(cardHolderTextCSS),"By creating a CARD HOLDER you will grant this user access to a reduced version of the functionality, including:\n"
		 		+ "See their own movements\n"
		 		+ "Edit their cards\n"
		 		+ "Edit personal details\n"
		 		+ "Managing people with access");
	}
	public void clickOnInvitedTab() {
		String invitedLinkText = "Invited";
		clickOnAnElement(By.linkText(invitedLinkText));;
	}
	public void clickOnAddAnewUserButton() {
		String addNewUserButtonXpath = "//button[contains(text(),'Add a new user')]";
		clickOnAnElement(By.xpath(addNewUserButtonXpath));;
				
	}
	public void selectAccountOwnerRole() {
		Select userRole = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div.display-flex.column.lum-input-box.user-role > div > select"))));
		userRole.selectByVisibleText("Account owner");
	}
	public void selectCardHolderRole() {
		Select userRole = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div.display-flex.column.lum-input-box.user-role > div > select"))));
		userRole.selectByVisibleText("Card holder");
	}
	public void enterName() {
		String nameName = "firstName";
		sendKeysToATextBox(By.name(nameName),"Kyle");
	}
	public void enterSurname() {
		String surNameName = "lastName";
		sendKeysToATextBox(By.name(surNameName),"Bruhm");
	}
	public void enterEmail() {
		String emailName = "email";
		sendKeysToATextBox(By.name(emailName),"selpha@toq.io");
	}
	public void enterDateOfBirth() {
		String dOBName = "birthDate";
		sendKeysToATextBox(By.name(dOBName),"01/01/1970");
	}
	public void enterPhonePrefix() {
		Select phonePrefix = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(3) > div.lum-input-phone.display-flex.user-input > div.display-flex.column.lum-input-box.dropdown-phone > div > select"))));
		phonePrefix.selectByVisibleText("Kenya(254)");
	}
	public void enterPhoneNumber() {
		String phoneNumberName = "phoneNumber";
		sendKeysToATextBox(By.name(phoneNumberName),"0706410315");
	}
	public void clickOnSaveButton() {
		String saveButtonXpath = "//button[contains(text(),'Save')]";
		clickOnAnElement(By.xpath(saveButtonXpath));;
	}
	public void clickOnConfirmButton() {
		String confirmButtonXpath = "//button[contains(text(),'Confirm')]";
		clickOnAnElement(By.xpath(confirmButtonXpath));
	}
	public void clickOnCancelButton() {
		String cancelButtonXpath = "//*[text()='Cancel']";
		clickOnAnElement(By.xpath(cancelButtonXpath));
	}

}
