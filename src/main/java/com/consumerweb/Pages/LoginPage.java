package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

public class LoginPage extends ConsumerWebBaseClass{
	
	private WebDriver driver;
	//WebDriver driver;
	private WebDriverWait wait;
	
	    
	public LoginPage(WebDriver driver){
		this.driver = driver;
        //  This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 10); 
	    }        

	//Enter User name
	public void enterUserName(String uName) {
		String usernameName = "email";
		sendText(By.name(usernameName), uName);
	}
	
	
	public void enterPassword(String pass) {
		String passwordName = "password";
		sendText(By.name(passwordName), pass);
	}
	
	public void clickOnLogin() {
		String loginButtonXpath = "//*[text()='Login']";
		clickElement(By.xpath(loginButtonXpath));	
	}
	public void assertPasswordErrorMessage() throws InterruptedException {
		String passErrorClassName = "input-error";
		Thread.sleep(5);
		assertCorrectText(By.className(passErrorClassName),"Field is required");
	}
	public void verifyLoginPageIsDisplayed() throws InterruptedException {
		String usernameName = "email";
		String passwordName = "password";
		String loginButtonXpath = "//*[text()='Login']";
		assertElementIsDisplayed(By.name(usernameName));
		assertElementIsDisplayed(By.name(passwordName));
		assertElementIsDisplayed(By.xpath(loginButtonXpath));

	}

	
}
