package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import com.aventstack.extentreports.Status;

public class PersonalSettingsPage extends ConsumerWebBaseClass{

	
	WebDriver driver;
	WebDriverWait wait;
	String path= System.getProperty("user.dir") + "/files/flower.jpg";
	Select userRole,languageSelector,languageSelector1;

	    
	public PersonalSettingsPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	
	public void clickOnPersonalSettings() throws InterruptedException {
		String personalSettingsCSS = "div.lum-avatar.display-flex.center-x.center-y";
		String userSettingsXpath = "//*[text()='User settings']";
		clickOnAnElement(By.cssSelector(personalSettingsCSS));
		Thread.sleep(2000);
		clickOnAnElement(By.xpath(userSettingsXpath));	
	}
	public void clickOnPersonalDetails() {
		String personalDetailsLinkText = "Personal details";
		clickOnAnElement(By.linkText(personalDetailsLinkText));
	}
	public void clickOnNotificationSetting() {
		String notificatopnsSettingsLinkText = "Notification settings";
		clickOnAnElement(By.linkText(notificatopnsSettingsLinkText));
	}
	public void clickOnChangeSecurityCode() {
		String changeSecurityCodeLinkText = "Change security code";
		clickOnAnElement(By.linkText(changeSecurityCodeLinkText));
	}
	public void clickOnChangePassword() {
		String changePasswordLinkText = "Change password";
		clickOnAnElement(By.linkText(changePasswordLinkText));
	}
	public void clickOnLanguage() {
		String languageLinkText = "Language";
		clickOnAnElement(By.linkText(languageLinkText));
	}
	public void uploadProfilePic() throws InterruptedException {
		System.out.println(path);
		String profilePicCSS = "#app-container-body > div > div > div > div:nth-child(2) > div > div.display-flex.column-lt-m.center-y.m-b-2 > div.clickable.display-flex.avatar-container > section > div > input[type=file]";
		sendKeysToATextBox(By.cssSelector(profilePicCSS),path);
		Thread.sleep(2000);
	}
	public void clickOnTransactionsButton() throws InterruptedException {
		String transactionsButtonXpath = "Transactions";
		clickOnAnElement(By.linkText(transactionsButtonXpath));
		Thread.sleep(2000);
	}
	public void clickOnEditButton() {
		String editButtonCSS = "#box-container > form > div.display-flex.center-x.m-t-4 > button";
		clickOnAnElement(By.cssSelector(editButtonCSS));
	}
	public void enterSurname() {
		String surnameName = "lastName";
		sendKeysToATextBox(By.name(surnameName),"Atembarr123");
	}
	public void clickOnSaveButton() throws InterruptedException {
		String saveButtonXpath = "//*[text()='Save']";
		clickOnAnElement(By.xpath(saveButtonXpath));
		Thread.sleep(2000);
	}
	public void closeVerificationWindow() {
		String verificationWindowCSS = "svg.lum-icon-component-fill.close_icon.clickable";
		clickOnAnElement(By.cssSelector(verificationWindowCSS));
	}
	public void clickOnBackButton() {
		String backButtonCSS = "#app-container-body > div > div > div > div.lum-back-btn.display-flex.center-y.clickable.m-b-2 > svg";
		clickOnAnElement(By.cssSelector(backButtonCSS));
	}
	public void toggleEmailNotifications() {
		String emailNotificationsToggleCSS = "#box-container > form > div.display-flex.center-y.item.m-b-2 > div:nth-child(2)";
		clickOnAnElement(By.cssSelector(emailNotificationsToggleCSS));
		clickOnAnElement(By.cssSelector(emailNotificationsToggleCSS));
	}
	public void enterOldCode(String oldCode) {
		String oldCodeName = "oldCode";
		sendKeysToATextBox(By.name(oldCodeName),oldCode);
	}
	public void enterNewCode(String newcode) {
		String newCodeName = "newCode";
		sendKeysToATextBox(By.name(newCodeName),newcode);
	}
	public void repeatNewCode(String repeatcode) {
		String repeatCodeName = "repeatNewCode";
		sendKeysToATextBox(By.name(repeatCodeName),repeatcode);
	}
	public void enterCurrentPassword(String currPass) {
		String currentPassWordName = "currentPassword";
		sendKeysToATextBox(By.name(currentPassWordName),currPass);
	}
	public void enterNewPassword(String newpass) {
		String newPassWordName = "newPassword";
		sendKeysToATextBox(By.name(newPassWordName),newpass);

	}
	public void repeatNewPassword(String repeatpass) {
		String repeatPasswordName = "repeatNewPassword";
		sendKeysToATextBox(By.name(repeatPasswordName),repeatpass);
	}
	public void changePassword() throws InterruptedException {
		for(int x=0;x<=1;x++) {
			if(x == 0) {
				clickOnPersonalSettings();
				clickOnChangePassword();
				enterCurrentPassword("Olivo2021");
				enterNewPassword("Olivo2022");
				repeatNewPassword("Olivo2022");
				clickOnSaveButton();
				Thread.sleep(2000);
			}else if(x == 1) {
				enterCurrentPassword("Olivo2022");
				enterNewPassword("Olivo2021");
				repeatNewPassword("Olivo2021");
				clickOnSaveButton();
				Thread.sleep(4000);
			}
		}
	}
	public void changeSecurityCode() throws InterruptedException {
		Thread.sleep(4000);
		for(int x=0;x<=1;x++) {
			if(x == 0) {
				 clickOnPersonalSettings();
				 clickOnChangeSecurityCode();
				 enterOldCode("1234");
				 enterNewCode("1111");
		  		 repeatNewCode("1111");
				 clickOnSaveButton();
				 Thread.sleep(2000);
			}else if(x ==1) {
				 enterOldCode("1111");
				 enterNewCode("1234");
		  		 repeatNewCode("1234");
				 clickOnSaveButton();
				 Thread.sleep(4000);
			}
		
	}
	}
	public void selectSpanish() {
		languageSelector = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div.display-flex.column.lum-input-box > div > select"))));
		languageSelector.selectByVisibleText("Español");
	}
	public void selectEnglish() {
		languageSelector1 = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div.display-flex.column.lum-input-box > div > select"))));
		languageSelector1.selectByVisibleText("English");
	}
	public void selectItalian() {
		languageSelector = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div.display-flex.column.lum-input-box > div > select"))));
		languageSelector.selectByVisibleText("Italian");
	}
	public void selectGerman() {
		languageSelector = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div.display-flex.column.lum-input-box > div > select"))));
		languageSelector.selectByVisibleText("German");
	}
	public void saveLanguageSelection() throws InterruptedException {
		String spanishSaveXpath = "//*[text()='Guardar']";
		clickOnAnElement(By.xpath(spanishSaveXpath));
		Thread.sleep(1000);
	}
	public void assertLanguageIsChangedToSpanish() {
		String expensesLinkText = "Gastos";
		String cardsLinkText = "Tarjetas";
		String peopleLinkText = "Personas";
		String paymentsLinkText = "Pagos";
		assertTextPresent(By.linkText(expensesLinkText), "Gastos");
		assertTextPresent(By.linkText(cardsLinkText), "Tarjetas");
		assertTextPresent(By.linkText(peopleLinkText), "Personas");
		assertTextPresent(By.linkText(paymentsLinkText), "Pagos");
	}
	
	public void displayUserTransactions() {
		String txPlaceHolderCSS = "div.lum-placeholder.display-flex.column.center-x.center-y ";
		String listCSS = "#box-container > ul";
		String listXpath = "//*[@id=\"box-container\"]/ul/li";
		 if(isElementPresent(By.cssSelector(txPlaceHolderCSS))) {
			 test.log(Status.INFO, "This user has no transactions");
		 }else {
			 listItems(By.cssSelector(listCSS),By.xpath(listXpath));
		 }
	}
	
	public void listCardsThatAuserHas() {
		String listCSS = "#box-container > div > ul";
		String listXpath = "//*[@id=\"box-container\"]/div/ul/li";
		 if(isElementPresent(By.cssSelector("h5.display-flex m-b-1.m-r-1.lum-fs-16"))) {
			 listItems(By.cssSelector(listCSS),By.xpath(listXpath));
		 }else {
			 test.log(Status.INFO, "This user has no cards");
		 }
	}
}
