package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class UserRegistrationPage {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement continueBtn,registerHere,fName,surName,dOB,emailField,mobileNo,continueButton,userAddress,userCity,userPostalCode,privacyPolicy,userContinueButton;
	Select drpCountry,drpPhonePrefix,drpUserCountry;

	    
	public UserRegistrationPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 5);   
	    } 
	
	public void clickOnNoAccountYetRegisterHere() {
		registerHere = wait.until(ExpectedConditions.presenceOfElementLocated(By.className("register")));
		registerHere.click();
	}
	public void selectAcountry() {
		drpCountry = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/form/div/div/select"))));
		drpCountry.selectByVisibleText("United Kingdom");
		
	}
	public void clickOnContinue() {
		continueBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[2]/div/div[1]/form/button")));
		continueBtn.click();
		
	}
	public void enterFirstName(String fn) {
		fName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("firstName")));
		fName.sendKeys(fn);
	}
    public void enterSurName(String ln) {
    	surName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("lastName")));
    	surName.sendKeys(ln);
    }
    public void enterDateOfBirth(String dob) {
    	dOB = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("birthDate")));
    	dOB.sendKeys(dob);
    }
    public void enterEmail(String email) {
    	emailField = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
    	emailField.sendKeys(email);
    }
    public void enterPhonePrefix() {
    	drpPhonePrefix = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[3]/div/div[1]/form/div[6]/div[1]/div/select"))));
    	drpPhonePrefix.selectByVisibleText("Kenya(254)");
    }
    public void enterPhoneNumber(String phoneNo) {
    	mobileNo = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("phoneNumber")));
    	mobileNo.sendKeys("0706410315");
    }
    public void clickOnContinueButton() {
    	continueButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[3]/div/div[1]/form/button[2]")));
    	continueButton.click();
    }
    public void selectAcountry1() {
    	drpUserCountry = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[4]/div/div[1]/form/div[1]/div/select"))));
    	drpUserCountry.selectByVisibleText("United Kingdom");
    }
    public void enterAddress(String address) {
    	userAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("personalAddress")));
    	userAddress.sendKeys(address);
    }
    public void enterCity(String cty) {
    	userCity = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("personalCity")));
    	userCity.sendKeys(cty);
    }
    public void enterPostalCode(String postal) {
    	userPostalCode = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("personalPostalCode")));
    	userPostalCode.sendKeys(postal);
    }
    public void clickOnPrivacyPolicy() {
    	privacyPolicy = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#root > div.display-flex.flex > div > div.container > div > div > div:nth-child(4) > div > div.flex > form > div:nth-child(6) > div > svg")));
    	privacyPolicy.click();
    }
    public void clickOnTheFinalContinueButton() {
    	userContinueButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"root\"]/div[2]/div/div[2]/div/div/div[4]/div/div[1]/form/button[2]")));
    	userContinueButton.click();
    }

}
