 package com.consumerweb.Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

public class AccountsPage extends ConsumerWebBaseClass{
	WebDriver driver;
	WebDriverWait wait;

	    
	public AccountsPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 5);   
	    }
	

	public void goToAccountsPage() {
		String accPage="(//span[contains(text(),'Accounts')])[1]";
		clickOnAnElement(By.xpath(accPage));
		
	}

	public void clickOnPaymentButton() {
		String paymentButtonXpath =  "//*[text()='New payment']";
		clickOnAnElement(By.xpath(paymentButtonXpath));
	}
	public void checkAccountsPageisDisplayed() {
		String accountsTitle = "div.lum-title.account-title";
		String totalBalanceIn = "div.box-title.m-a-0";
		String balanceIsDisplayed = "span.lum-truncate.lum-fs-40";
		String paymentButtonIsDisplayed = "Payment";
		String listCSS = "div.lum-account-list.flex";
		String listCSS1  = "a.lum-account-item.reset-link";
		if(isElementPresent(By.cssSelector(totalBalanceIn))) {
		assertTextPresent(By.cssSelector(accountsTitle),"Accounts");
		assertTextPresent(By.cssSelector(totalBalanceIn),"Total balance in");
		assertElementIsDisplayed(By.cssSelector(balanceIsDisplayed));
		assertElementIsDisplayed(By.linkText(paymentButtonIsDisplayed));
		listItems(By.cssSelector(listCSS),By.cssSelector(listCSS1));
		}else {
			test.log(Status.INFO, "This company has got one account only");
		}

	}
	public void assertCancelledAccountsAreDisplayed() {
		String listCSS = "div.lum-account-list.flex";
		String listCSS1  = "a.lum-account-item.reset-link";
		listItemsContainsText(By.cssSelector(listCSS),By.cssSelector(listCSS1),"Cancelled");
	}
	public void showTotalAccountBalance() {
		String totalBalanceInCSS = "div.box-title.m-a-0";
		String totalACCBalanceCSS = "span.lum-truncate.lum-fs-40"; 
		if(isElementPresent(By.cssSelector(totalBalanceInCSS))) {
			showElementText(By.cssSelector(totalACCBalanceCSS));
		}else {
			test.log(Status.INFO, "This company has got one account only");
		}
		
	}
	public void checkThatAddAnaccountButtonIsPresent() {
		String addAccBtnLinkText = "Add an account";
		if(isElementPresent(By.cssSelector(addAccBtnLinkText))) {
			assertElementIsDisplayed(By.linkText(addAccBtnLinkText));
		}else {
			test.log(Status.INFO, "This company has got one account only");
		}
	}
	
	
	public void checkThatDirectorWithOneAccountCanAddAccount(){
		String moreButtonLinkText = "More";
		String addAnAccountXpath = "//*[text()='Add an account']";
		if(isElementPresent(By.linkText(moreButtonLinkText))) {
			clickOnAnElement(By.linkText(moreButtonLinkText));
			clickOnAnElement(By.xpath(addAnAccountXpath));
		}else {
			test.log(Status.INFO, "This company has more than one account");
		}
		
	}
	public void clickOnAddAnAccountButton() throws InterruptedException {
		String addAnAccountLinkText = "Add an account";
		String moreButtonLinkText = "More";
		String addAnAccountXpath = "//*[text()='Add an account']";
		Thread.sleep(3000);
		if(isElementPresent(By.linkText("Add an account"))) {
		clickOnAnElement(By.linkText(addAnAccountLinkText));
		}else {
			clickOnAnElement(By.linkText(moreButtonLinkText));
			clickOnAnElement(By.xpath(addAnAccountXpath));
		}	
	}
	public void checkEUR3PartnerProducts() {
		List<String>pp = null;
		List<WebElement> partnerProducts =  driver.findElements(By.className("clickable display-flex center-y lum-radiobutton mt-2 lum-w500 lum-fs-14"));
		for (WebElement webElement : partnerProducts) {
			String partnerProduct = webElement.getText();
			pp.add(partnerProduct);
	     System.out.println("************************************8");
	     System.out.println(pp);
	     Assert.assertTrue(pp.contains("European Union"));
		 }
	}
	
	public void enterAccountAlias(String alia) {
		String accountAliasTextBoxName = "alias";
		sendKeysToATextBox(By.name(accountAliasTextBoxName),alia);
	}
	public void enterAccountAliasThatIsMoreThan25Characters() {
		String accountAliasTextBoxName = "alias";
		sendKeysToATextBox(By.name(accountAliasTextBoxName),"QA TEAM TESTING ACCOUNT IN TEST");
	}
	public void assertAliasErrorIsDisplayed() {
		String aliasErrorCSS = "span.input-error";
		assertTextPresent(By.cssSelector(aliasErrorCSS), "Alias must be at least 4 characters and no more than 25");
	}
	public void selectGBPAccountInUK() throws InterruptedException {
		//"div.radion-container"
		String gBPAccountInUKXpath = "//*[text()='GBP Account in the UK']";
		clickOnAnElement(By.xpath(gBPAccountInUKXpath));
		Thread.sleep(3000);
		
	}
	
	public void clickOnSaveAccountButton() throws InterruptedException {
		String saveAccountButtonXpath = "//*[text()='Save']";
		clickOnAnElement(By.xpath(saveAccountButtonXpath));
		Thread.sleep(10000);
	}
	
	public void clickOnAccountDetails() {
		String accountDetailsXpath = "//*[text()='Account details']";
		clickOnAnElement(By.xpath(accountDetailsXpath));
	}
	//These selectors to be reviewed
	//   ".dropdown-item.display-flex.center-y.clickable:nth-child(2) "
	public void checkIfAccountDetailsAreWellDisplayed() {
		String receivePaymentsTextCSS = "h5.section-title.lum-fs-20.lum-w500";
		String accountAliasHolderCSS = ".lum-info.display-flex.column.flex:nth-child(1)   ";
		String accountAliasCSS = "div.info.lum-fs-16";
		String bankNameHolderCSS = ".lum-info.display-flex.column.flex:nth-child(2)   ";
		String bankNameCSS = "div.info.lum-fs-16";
		String bankAddressHolderCSS = ".lum-info.display-flex.column.flex:nth-child(3)   ";
		String bankAddressCSS = "div.info.lum-fs-16";
		String iBANCSSHolderCSS = ".lum-info.display-flex.column.flex:nth-child(4)   ";
		String iBANCSS = "div.info.lum-fs-16";
		String bicSwiftHolderCSS = ".lum-info.display-flex.column.flex:nth-child(5)   ";
		String bicSwiftCSS = "div.info.lum-fs-16";
		String beneficiaryNameHolderCSS = ".lum-info.display-flex.column.flex:nth-child(6)   ";
		String beneficiaryNameCSS = "div.info.lum-fs-16";
		String holderAddressHolderCSS = ".lum-info.display-flex.column.flex:nth-child(7)   ";
		String holderAddressCSS = "div.info.lum-fs-16";
		String gBPbeneficiaryNameHolderCSS = ".lum-info.display-flex.column.flex:nth-child(8)   ";
		String gBPbeneficiaryNameCSS = "div.info.lum-fs-16";
		String gBPholderAddressHolderCSS = ".lum-info.display-flex.column.flex:nth-child(9)   ";
		String gBPholderAddressCSS = "div.info.lum-fs-16";
		String sortCodeHolderCSS = ".lum-info.display-flex.column.flex:nth-child(6)   ";
		String sortCodeCSS = "div.info.lum-fs-16";
		String accountNumberHolderCSS = ".lum-info.display-flex.column.flex:nth-child(7)   ";
		String accountNumberCSS = "div.info.lum-fs-16";
		if (showElementText(By.cssSelector(receivePaymentsTextCSS)).contains("EUR")) {
			assertElementIsNotEmpty(By.cssSelector(accountAliasHolderCSS).cssSelector(accountAliasCSS));
			assertElementIsNotEmpty(By.cssSelector(bankNameHolderCSS).cssSelector(bankNameCSS));
			assertElementIsNotEmpty(By.cssSelector(bankAddressHolderCSS).cssSelector(bankAddressCSS));
			assertElementIsNotEmpty(By.cssSelector(iBANCSSHolderCSS).cssSelector(iBANCSS));
			assertElementIsNotEmpty(By.cssSelector(bicSwiftHolderCSS).cssSelector(bicSwiftCSS));
			assertElementIsNotEmpty(By.cssSelector(beneficiaryNameHolderCSS).cssSelector(beneficiaryNameCSS));
			assertElementIsNotEmpty(By.cssSelector(holderAddressHolderCSS).cssSelector(holderAddressCSS));
		}else {
			assertElementIsNotEmpty(By.cssSelector(accountAliasHolderCSS).cssSelector(accountAliasCSS));
			assertElementIsNotEmpty(By.cssSelector(bankNameHolderCSS).cssSelector(bankNameCSS));
			assertElementIsNotEmpty(By.cssSelector(bankAddressHolderCSS).cssSelector(bankAddressCSS));
			assertElementIsNotEmpty(By.cssSelector(iBANCSSHolderCSS).cssSelector(iBANCSS));
			assertElementIsNotEmpty(By.cssSelector(bicSwiftHolderCSS).cssSelector(bicSwiftCSS));
			assertElementIsNotEmpty(By.cssSelector(sortCodeHolderCSS).cssSelector(sortCodeCSS));
			assertElementIsNotEmpty(By.cssSelector(accountNumberHolderCSS).cssSelector(accountNumberCSS));
			assertElementIsNotEmpty(By.cssSelector(gBPbeneficiaryNameHolderCSS).cssSelector(gBPbeneficiaryNameCSS));
			assertElementIsNotEmpty(By.cssSelector(gBPholderAddressHolderCSS).cssSelector(gBPholderAddressCSS));
		}
		
	}
	
	public void checkIfCopyAccountDetailsIsAvailable() {
		String copyAllaccountDetailsXpath  = "//*[text()='Copy all account details']";
		assertElementIsNotEmpty(By.xpath(copyAllaccountDetailsXpath));
	}
	
	public void clickOnListIcon() throws InterruptedException {
		String listIconCSS = "svg.lum-icon-component-fill.clickable.lum-icon-color-black-70";
		clickOnAnElement(By.cssSelector(listIconCSS));
		Thread.sleep(2000);
	}
	public void clickOnBackButton() throws InterruptedException {
		String backButtonXpath = "//*[text()='Back']";
		clickOnAnElement(By.xpath(backButtonXpath));
		Thread.sleep(4000);
	}
	public void clickOnDots() {
		String moreButtonName = "icn_dots";
		clickOnAnElement(By.name(moreButtonName));
	}
	public void clickOnCancelAccount() throws InterruptedException {
		String cancelAccountOptionXpath = "//*[text()='Cancel account']";
		clickOnAnElement(By.xpath(cancelAccountOptionXpath));
		Thread.sleep(2000);	
	}
	public void clickOnDownLoadBankStatement() throws InterruptedException {
		String cancelAccountOptionXpath = "//*[text()='Download statement']";
		clickOnAnElement(By.xpath(cancelAccountOptionXpath));
		Thread.sleep(4000);	
	}
	public void clickOnCancelAndRemoveFromView() throws InterruptedException {
		String cancelAndRemoveFromViewXpath = "//div[contains(text(),'Cancel and remove from view')]";
		clickOnAnElement(By.xpath(cancelAndRemoveFromViewXpath));
		Thread.sleep(3000);	
	}
	public void clickOnConfirmCancellationButton() throws InterruptedException {
		String confirmCancellationButtonXpath = "//*[text()='Confirm cancellation']";
		clickOnAnElement(By.xpath(confirmCancellationButtonXpath));
		Thread.sleep(9000);		
	}
	public void checkNewAccountMovementsPageIsDisplayed() {
		String NoMovementsplaceholderXpath =  "//*[text()='There are no movements']";
		assertTextPresent(By.xpath(NoMovementsplaceholderXpath), "There are no movements");
		

	}

	public void clickOnAddAccountButtonForExistingAccounts() throws InterruptedException {
		String firstAccountCSS = ".lum-account-item.reset-link:nth-child(1)";
		String dotsName = "icn_dots";
		String addAnAccountXpath = "//*[text()='Add an account']";
		if(isElementPresent(By.cssSelector("a.lum-account-item.reset-link"))) {
			clickOnAnElement(By.cssSelector(firstAccountCSS));
			clickOnAnElement(By.name(dotsName));
			Thread.sleep(2000);
			clickOnAnElement(By.xpath(addAnAccountXpath));
			Thread.sleep(1000);
		}else {
			clickOnAnElement(By.name(dotsName));
			Thread.sleep(2000);
			clickOnAnElement(By.xpath(addAnAccountXpath));
			Thread.sleep(1000);
			
		}
	}
	
	public void checkAccountCreationPageIsDisplayed() {
		String accountAliasTextBoxName = "alias";
		String accountTypeCSS1 = "div.lum-radiogroup ";           
		String accountTypeCSS2 = "div.clickable.display-flex.center-y.lum-radiobutton.mt-2.lum-w500.lum-fs-14";
		assertElementIsDisplayed(By.name(accountAliasTextBoxName));
		listItems(By.cssSelector(accountTypeCSS1),By.cssSelector(accountTypeCSS2));
	}
	
	public void clickOnTransactionsSubDashBoard() throws InterruptedException {
		String transactionsSubdashboardLinkText = "Transactions";
		clickOnAnElement(By.linkText(transactionsSubdashboardLinkText));
	}
	
	public void checkThatFilterIsDisplayedForTransactions() {
		String filterxpath = "//div[contains(text(),'Filter')]";
		assertElementIsDisplayed(By.xpath(filterxpath));
		
	}
	public void getListOfItemsDisplayed() {
		listItems(By.cssSelector("ul.reset-list-style.lum-section-list"),By.cssSelector("li.item-list-separator"));
	}
	public void checkSearchByMerchantNotesAddressOrConceptWorks() throws InterruptedException {
		String filterxpath = "//div[contains(text(),'Filter')]";
		String searchByMerchantTxBxName = "text";
		String buttonXpath = "//*[text()='Confirm']";
		clickOnAnElement(By.xpath(filterxpath));
		sendKeysToATextBox(By.name(searchByMerchantTxBxName),"TOQIO");
		clickOnAnElement(By.xpath(buttonXpath));
		Thread.sleep(2000);
		getListOfItemsDisplayed();
		Thread.sleep(2000);
		
	}
	public void checkSearchByAmountWorks() throws InterruptedException {
		String filterxpath = "//div[contains(text(),'Filter')]";
		String amountFromName = "amountFrom";
		String amountToName  = "amountTo";
		String buttonXpath = "//*[text()='Confirm']";
		clickOnAnElement(By.xpath(filterxpath));
		checkClearFiltersWorks(); 
		Thread.sleep(2000);
		clickOnAnElement(By.xpath(filterxpath));
		Thread.sleep(1000);
		sendKeysToATextBox(By.name(amountFromName),"0");
		sendKeysToATextBox(By.name(amountToName),"450000");
		clickOnAnElement(By.xpath(buttonXpath));
		Thread.sleep(2000);
		getListOfItemsDisplayed();
	
		
	}
	public void checkSearchByDateWorks() throws InterruptedException {
		String buttonXpath = "//*[text()='Confirm']";
		String filterxpath = "//div[contains(text(),'Filter')]";
		String dateFromName = "dateFrom";
		String dateToName  = "dateTo";
		clickOnAnElement(By.xpath(filterxpath));
		checkClearFiltersWorks();
		Thread.sleep(2000);
		clickOnAnElement(By.xpath(filterxpath));
		Thread.sleep(1000);
		sendKeysToATextBox(By.name(dateFromName),"01/10/2021");
		sendKeysToATextBox(By.name(dateToName),"20/10/2021");
		clickOnAnElement(By.xpath(buttonXpath));
		Thread.sleep(2000);
		getListOfItemsDisplayed();
		clickOnAnElement(By.xpath(filterxpath));
		checkClearFiltersWorks();
		Thread.sleep(2000);
		assertElementIsNotDisplayed(By.name(dateFromName));
		
	}
	public void checkClearFiltersWorks() {
		String clearFiltersXpath = "//*[text()='Clear filters']";
		clickOnAnElement(By.xpath(clearFiltersXpath));
		
	}
	
	public void clickOnAccountsMenuOption() throws InterruptedException {
		String accountsLinkText = "Accounts";
		clickOnAnElement(By.linkText(accountsLinkText));
		Thread.sleep(2000);
	}
	public void clickOn3DotsOfExistingAccounts() {
		String threedotsCSS = "svg.lum-icon-component-fill.info-icon";
		if(isElementPresent(By.cssSelector("div.account-grid"))) {
			clickOnAnElement(By.cssSelector(threedotsCSS));
		}else {
			test.log(Status.INFO, "No accounts found");
		}
	}
	public void checkThe3DotsAreAvailable() {
		String threedotsCSS = "svg.lum-icon-component-fill.info-icon";
		if(isElementPresent(By.cssSelector("div.account-grid"))) {
			assertElementIsDisplayed(By.cssSelector(threedotsCSS));
		}
		
	}
	public void checkTheAccountDetailsPopupIsDisplayed() {
		String accountsDetailsPopUpXpath = "//*[text()='Account details']";
		assertElementIsDisplayed(By.xpath(accountsDetailsPopUpXpath));	
	}
	
	public void clickOnAccountDetailsPopUp() throws InterruptedException{
		String accountsDetailsPopUpXpath = "//*[text()='Account details']";
		clickOnAnElement(By.xpath(accountsDetailsPopUpXpath));
		Thread.sleep(3000);
	}
	public void checkDeleteAccountCopiesAreUpdated() {
		String ulCSS = "ul.p-l-2";
		String cardsCSS = "li:nth-child(1)";
		String outBoundCSS = "li:nth-child(2)";
		String inBoundCSS = "li:nth-child(3)";
		assertTextPresent(By.cssSelector(ulCSS).cssSelector(cardsCSS),"Any cards linked to this account will be cancelled");
		assertTextPresent(By.cssSelector(ulCSS).cssSelector(outBoundCSS),"Outbound payments from this account will no longer be possible");		
		assertTextPresent(By.cssSelector(ulCSS).cssSelector(inBoundCSS),"Inbound payments to this account will no longer be possible");
	}
	
	public void checkCountryCodeIsRemovedFromAccountType() {
		String accountCSS1 = ".lum-info.display-flex.column.flex.info-element:nth-child(4)";
		String accountTypeTextCSS = "div.info.lum-fs-16";
		Assert.assertFalse(assertTextPresent(By.cssSelector(accountCSS1).cssSelector(accountTypeTextCSS),"Account in"));
	}
	public boolean thereIsMoreThanOneAccount() {
		String firstAccountCSS = "a.lum-account-item.reset-link";
		return isElementPresent(By.cssSelector(firstAccountCSS));
		
	}
	public void checkUserCanCopyAccountDetails() throws InterruptedException {
		String firstAccountCSS = "a.lum-account-item.reset-link";
		String accountDetailsXpath = "//*[text()='Account details']";
		String copyAllaccountDetailsXpath  = "//*[text()='Copy all account details']";
		if(thereIsMoreThanOneAccount()) {
			clickOnAnElement(By.cssSelector(firstAccountCSS));
			clickOnAnElement(By.xpath(accountDetailsXpath));
		}else {
			clickOnAnElement(By.xpath(accountDetailsXpath));
		}
		clickOnAnElement(By.xpath(copyAllaccountDetailsXpath));

}
	public void checkUserCanDownloadBankStatement() throws InterruptedException {
		String firstAccountCSS = "a.lum-account-item.reset-link";
		if(thereIsMoreThanOneAccount()) {
			clickOnAnElement(By.cssSelector(firstAccountCSS));
		}else {
			
		}
		clickOnDots();
		clickOnDownLoadBankStatement();
		
	}
	public void enterDownloadBankStatementFromDAte() {
		String bankStatementFromDateName = "startDate";
		sendKeysToATextBox(By.name(bankStatementFromDateName),"01/10/2021");
		
	}
	public void enterDownloadBankStatementToDAte() {
		String bankStatementToDateName = "endDate";
		sendKeysToATextBox(By.name(bankStatementToDateName),"25/10/2021");
		
	}
	public void clickOnDownLoadButton() {
		String bankStatementDownloadXpath = "//*[text()='Download']";
		clickOnAnElement(By.xpath(bankStatementDownloadXpath));
	}
	public void clickOnCompanySettings() {
		String companySettingsLinkText = "Company settings";
		clickOnAnElement(By.linkText(companySettingsLinkText));
	}
	public void clickOnCompanyProfile() {
		String companyProfileLinkText = "Company profile";
		clickOnAnElement(By.linkText(companyProfileLinkText));
	}
	public String getCompanyCountry() {
		String companyCountryHolderCSS = ".flex-wrap.order-box:nth-child(2)";
		String companyCountry = "div.info.lum-truncate.lum-fs-16";
		clickOnCompanySettings();
		clickOnCompanyProfile();
		return showElementText(By.cssSelector(companyCountryHolderCSS).cssSelector(companyCountry));
	}
	public void clickOnAccountDetailsButton() {
		String firstAccountCSS = "a.lum-account-item.reset-link";
		String accountDetailsXpath = "//*[text()='Account details']";
		if(thereIsMoreThanOneAccount()) {
			clickOnAnElement(By.cssSelector(firstAccountCSS));
		}else {
			
		}
		clickOnAnElement(By.xpath(accountDetailsXpath));
	}
	public void clickOnFirstAccount() {
		String firstAccountCSS = ".lum-account-item.reset-link:nth-child(1)";
		clickOnAnElement(By.cssSelector(firstAccountCSS));
	
	}
	public void compareAccountNameAndPayFromName() {
		Assert.assertEquals(getAccountName(), getPayfrom());

	}
	public String getAccountName() {
		String accountNameClassName = "BasicText__StyledText-sc-19x2x6q-0 lmvplQ PageHeader__StyledTitle-sc-167y5gr-4 kEIDC toqio-page-header-title toqio-basic-text";
		clickOnFirstAccount();
		return showElementText(By.className(accountNameClassName));
	}
	public String getPayfrom() {
		String payFromCSS = "div.lum-truncate.lum-w500";
		clickOnPaymentButton();
		return showElementText(By.cssSelector(payFromCSS));
	}
	public void checkThatBankStatementIsReplaced() {
		String firstAccountCSS = "a.lum-account-item.reset-link";
		String cancelAccountOptionXpath = "//*[text()='Download statement']";
		if(thereIsMoreThanOneAccount()) {
			clickOnAnElement(By.cssSelector(firstAccountCSS));
		}else {
			
		}
		clickOnDots();
		assertTextPresent(By.xpath(cancelAccountOptionXpath),"Download bank statement");
		
	}
	
	public boolean accountBalanceIsZero() {
		String accountBalanceCSS = "div.lum-truncate.lum-color-black-50.lum-fs-14";
		return showElementText(By.cssSelector(accountBalanceCSS)).contains("0.00");
		
	}
	public String returnCurrencyOfTheAccount() {
		String currencyCSS = "div.m-r-1.lum-w500";
		return showElementText(By.cssSelector(currencyCSS));
	}
	//isElementPresent(By.cssSelector(modifyCSS))
	//clickOnAnElement(By.cssSelector(accountsUlCSS).cssSelector(firstAccountInList));
	public void choosePayFrom() throws InterruptedException {
		String modifyCSS = "div.lum-color-primary.lum-semi-bold.clickable";
		String divHolderCSS = "body > div:nth-child(15) > div > div > div > div > ul > li:nth-child(1)";
		String mainAccountXpath = "//*[text()='main account']";
		String accountsUlCSS = "ul.reset-list-style.lum-fs-16"; //reset-list-style lum-fs-16
		String firstAccountInList = "li:nth-child(1)";
		String searchInputCSS = "input.search-input";
		if(accountBalanceIsZero()) {
			if(isElementPresent(By.cssSelector(modifyCSS))) {
				clickOnAnElement(By.cssSelector(modifyCSS));
				Thread.sleep(2000);
				//sendKeysToATextBox(By.cssSelector(searchInputCSS),"Eur"); 
				clickOnAnElement(By.cssSelector(divHolderCSS));

			}else {
				test.log(Status.INFO, "This company has only one account");
			}
			
		}
	}
	// body > div:nth-child(14) > div > div > div > div > ul > li:nth-child(1) > div:nth-child(2) > ul > li
	public void chooseAnExistingBeneficiary() {
		String beneficiaryListHolderCSS = "body > div:nth-child(14) > div > div > div > div > ul > li:nth-child(1) > div:nth-child(2) > ul > li";
		String firstbeneficiaryCSS = "li:nth-child(1)";
		String noBeneficiaryFoundCSS = "div.lum-placeholder.display-flex.column.center-x.center-y ";
		String chooseAnExistingBeneficiaryXpath = "//*[text()='Choose an existing beneficiary']";
		clickOnAnElement(By.xpath(chooseAnExistingBeneficiaryXpath));
		if(isElementPresent(By.cssSelector(noBeneficiaryFoundCSS))) {
			test.log(Status.INFO, "There are no beneficiaries");
		}else {

			clickOnAnElement(By.cssSelector(beneficiaryListHolderCSS));
		}
	}
	
	public void enterYouSendAmount() {
		String youSendName = "amount";
		sendKeysToATextBox(By.name(youSendName),"200");
	}
	//select[contains(@class,‘lum-input lum-select lum-truncate ‘)]/option[text()=‘€ - EUR’]“;
	public void selectInterledgerPaymentScheme() throws InterruptedException {
		String paymentSchemeCSS = "#box-container > form > div:nth-child(3) > div.m-l-1.m-r-2.max-w-100 > div:nth-child(2) > div > select";
		String interledgerXpath = "//*[@id=\"box-container\"]/form/div[3]/div[2]/div[2]/div/select/option[2]";
        clickOnAnElement(By.cssSelector(paymentSchemeCSS));
		Thread.sleep(1500);
		clickOnAnElement(By.xpath(interledgerXpath));
		
	}
	public void enterConcept() {
		String ConceptName = "concept";
		sendKeysToATextBox(By.name(ConceptName),"TESTING");
	}
	public void clickOnImmediatePayment() {
		String immediatePaymentCSS = "div.clickable.display-flex.center-y.lum-radiobutton ";
		clickOnAnElement(By.cssSelector(immediatePaymentCSS));
	}
	public void clickOnConfirmButton() {
		String buttonXpath = "//*[text()='Confirm']";
		clickOnAnElement(By.xpath(buttonXpath));
	}
	public void enterNotifyByEmail() {
		String notifyByEmailName = "emails";
		sendKeysToATextBox(By.name(notifyByEmailName),"selpha@toq.io");
	}
	public void enterSecurityCode() throws InterruptedException {
		String securityCodeName = "code";
		sendKeysToATextBoxAndClickEnter(By.name(securityCodeName),"1234");
		Thread.sleep(3000);
	}
	public void clickOnTotalBalanceCurrencySelector() {
		String totalBalanceCurrencySelectorCSS = "a.Link__StyledLink-sc-oiyps4-0.hwhGee toqio-link-component.undefined.toqio-link";
		if(isElementPresent(By.cssSelector(totalBalanceCurrencySelectorCSS))) {
			clickOnAnElement(By.cssSelector(totalBalanceCurrencySelectorCSS));
		}else {
			test.log(Status.INFO, "This company has only one currency");
		}
		
	}
	public void clickOnTheFirstCurrency() throws InterruptedException {
		String firstCurrencyCSS = "p.BasicText__StyledText-sc-19x2x6q-0.eDeFUE Link__StyledText-sc-oiyps4-1.dTAxCA toqio-a-text.toqio-basic-text";
		clickOnAnElement(By.cssSelector(firstCurrencyCSS));
		Thread.sleep(3000);
	}
	
	public void loginAsAcardHolder() {
//		setUp();
//		login("accountant","Olivo2021");
//		chooseAccount();	
	}

	public void checkAddAccountIsNotDisplayed() {
		String addAnAccountXpath = "//*[text()='Add an account']";
		assertElementIsNotDisplayed(By.xpath(addAnAccountXpath));
	}
	
	


	
	

}
