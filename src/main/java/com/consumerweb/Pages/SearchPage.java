package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchPage  extends ConsumerWebBaseClass{
	WebDriver driver;
	WebDriverWait wait;
	WebElement search,searchButton,exportButton,searchByMerchant,amountFrom,amountTo,dateFrom,dateTo,category,transactionOwner,movementType,categoryHolder,suggestedCurrenciesHolder1,
	currency,files,paymentStatus,firstCat,firstTransactionOwner,outBoundMovements,confirmOutBound,euros,withoutFiles,confirmAllButton,pendingPaymentStatus,outBoundMovements1,pendingPaymentStatus1,
	confirmPendingstatus,confirm,firstTransactionOwnerHolder,suggestedCurrenciesHolder,categoryHolder1,firstCat1,firstTransactionOwnerHolder1,firstTransactionOwner1,euros1,withoutFiles1;
	Select userRole,phonePrefix,frequency,countryOfRegistration;

	    
	public SearchPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	
	public void clickOnSearch() {
		search = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//a[@href='/search']/div)[1]")));
		search.click();
		
	}
	public void clickOnSearchButton() {
		searchButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Search')]")));
		searchButton.click();
		}
	public void clickOnExportButton() {
		exportButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Export')]")));
		exportButton.click();
	}
	public void searchByMerchant(String merchant) {
		searchByMerchant = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("text")));
		searchByMerchant.sendKeys(merchant);
	}
	public void enterAmountRangeFrom(String amtFrom) {
		amountFrom = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("amountFrom")));
		amountFrom.sendKeys(amtFrom);
	}
	public void enterAmountRangeTo(String amtTo) {
		amountTo = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("amountTo")));
		amountTo.sendKeys(amtTo);
	}
	public void enterDate1(String date1) {
		dateFrom = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dateFrom")));
		dateFrom.sendKeys(date1);
	}
	public void enterDate2(String date2) {
		dateTo = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("dateTo")));
		dateTo.sendKeys(date2);
	}
	public void clickOnCategory() {
		category = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div:nth-child(5)")));
		category.click();
	}
	//     /html/body/div[7]/div/div/div/div/form/ul/li[1]  /html/body/div[8]/div/div/div/div/form/ul/li[1]/div[2]/ul/li[1]     body > div:nth-child(16) > div > div > div > div > form > ul > li:nth-child(1)
	//   /html/body/div[7]/div/div/div/div/form/ul/li[1]
	public void clickOnFirstCategory() {
		categoryHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.reset-list-style.lum-fs-16")));
		firstCat = categoryHolder.findElement(By.cssSelector("li:nth-child(1)"));
		firstCat.click();
	}

	public void clickOnTransactionOwner() {
		transactionOwner = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div:nth-child(6)")));
		transactionOwner.click();
	}
	//     body > div:nth-child(18) > div > div > div > div > form > ul > li:nth-child(1) > div:nth-child(2) > ul
	public void clickOnTheFirstTransactionOwner() {
		firstTransactionOwnerHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.reset-list-style.lum-section-list")));
		firstTransactionOwner = firstTransactionOwnerHolder.findElement(By.cssSelector("li:nth-child(1)"));
		firstTransactionOwner.click();
	}
	public void clickOnMovementType() {
		String movementType = "//*[text()='Movement type']";
		clickElement(By.xpath(movementType));
	}
	public void clickOutboundMovements() {
		String outBoundMovementXpath = "//*[text()='Outbound movements']";
		clickElement(By.xpath(outBoundMovementXpath));
	}

	public void clickOnCurrency() {
		String currencyXpath =  "//*[text()='Currency']";
		clickElement(By.xpath(currencyXpath));
	}
	
	public void selectEuros() {
		suggestedCurrenciesHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("ul.reset-list-style.lum-fs-16")));
		euros = suggestedCurrenciesHolder.findElement(By.cssSelector(".item-list-separator:nth-child(1) "));
		euros.click();
	}

	public void clickOnFiles() {
		String filesXpath =  "//*[text()='Files']";
		clickElement(By.xpath(filesXpath));
	}
	//     clickable display-flex center-y lum-radiobutton mt-2
	public void selectWithoutFiles() {
		String withoutFilesXpath =  "//*[text()='Without files']";
		clickElement(By.xpath(withoutFilesXpath));
	}
	public void confirmAll() {
		confirmAllButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div:nth-child(12) > div > div > div > div > form > div.display-flex.center-y.center-x.flex.m-t-2 > button")));
		confirmAllButton.click();
	}
	public void clickOnPaymentStatus() {
		String paymentStatusXpath = "//*[text()='Payment status']";
		clickElement(By.xpath(paymentStatusXpath));
	}
	public void clickOnPendingPaymentStatus() {
		String paymentStatusXpath =  "//*[text()='Pending']";
		clickElement(By.xpath(paymentStatusXpath));
	}
	public void confirmPendingStatus() {
		confirmPendingstatus =wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div:nth-child(13) > div > div > div > div > form > div.display-flex.center-y.center-x.flex.m-t-2 > button")));
		confirmPendingstatus.click();
	}
	public void clickOnConfirm() {
		String confirmXpath = "//*[text()='Confirm']";
		clickElement(By.xpath(confirmXpath));
	}

}
