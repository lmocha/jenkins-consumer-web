package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class NotificationsPage {
	WebDriver driver;
	WebDriverWait wait;
	WebElement notifications;
	Select userRole,phonePrefix,frequency,countryOfRegistration;

	    
	public NotificationsPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	
	public void clickOnNotifications() {
		notifications = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("(//a[@href='/notifications']/div)[1]")));
		notifications.click();
		
	}

}
