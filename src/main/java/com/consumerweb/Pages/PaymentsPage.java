package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

public class PaymentsPage extends ConsumerWebBaseClass{
	WebDriver driver;
	WebDriverWait wait;
	AccountsPage accountsPage;

	    
	public PaymentsPage(WebDriver driver){
	        this.driver = driver;
	        accountsPage = new AccountsPage(driver);
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	public void clickOnAButton(String text) throws InterruptedException {
		String textOnButtonXpath = "//*[text()='"+text+"']";
		clickElement(By.xpath(textOnButtonXpath));
		Thread.sleep(2000);
	}
	public void clickOnPayments() {
		String paymentLinkText = "Payments";
		clickOnAnElement(By.linkText(paymentLinkText));
	}
	public void assertAvailableBalanceIsDisplayed() {
		String availableBalanceCSS = "div.lum-truncate.lum-color-black-50.lum-fs-14";
		assertElementIsNotEmpty(By.cssSelector(availableBalanceCSS));
	}
	public void assertPaymentsPageIsDisplayed() {
		String paymentSummaryXpath = "//*[text()='Payment summary']";
		String paymentsXpath = "//*[text()='Payments']";
		String beneficiariesXpath = "//*[text()='Beneficiaries']";
		String newPaymentXpath = "//*[text()='New payment']";
		String newVeneficiaryXpath = "//*[text()='New beneficiary']";
		assertTextPresent(By.xpath(paymentSummaryXpath), "Payment summary");
		assertTextPresent(By.xpath(paymentsXpath ), "Payments");
		assertTextPresent(By.xpath(beneficiariesXpath), "Beneficiaries");
		assertTextPresent(By.xpath(newPaymentXpath), "New payment");
		assertTextPresent(By.xpath(newVeneficiaryXpath), "New beneficiary");
	}

	public void modifyPayFrom(String curr) throws InterruptedException {
		String modifyCSS = "div.lum-color-primary.lum-semi-bold.clickable";
		String accountsListXpath ="//ul[contains(@class,'reset-list-style lum-fs-16')]/li";
		if(isElementPresent(By.cssSelector(modifyCSS))) {
			clickOnAnElement(By.cssSelector(modifyCSS));
			Thread.sleep(2000);
			randomlySelectListElementByVisibleText(By.xpath(accountsListXpath),curr);
		}
	}
	public void modifyPayToForOwnAccounts(String curr) {
		String accountsListXpath ="//ul[contains(@class,'reset-list-style lum-fs-16')]/li";
	   randomlySelectListElementByVisibleText(By.xpath(accountsListXpath),curr);
	}
	public void assertPayFromIsDisplayed() {
		String payFromXpath = "//*[text()='Pay from']";
		assertElementIsDisplayed(By.xpath(payFromXpath));
	}
	public void assertPayToIsDisplayed() {
		String payToXpath = "//*[text()='Pay to']";
		assertElementIsDisplayed(By.xpath(payToXpath));
	}
	public void assertAmountAndDetailsIsDisplayed() {
		String amountAndDetailsXpath = "//*[text()='Amount and details']";
		assertElementIsDisplayed(By.xpath(amountAndDetailsXpath));
	}
	public void clickOnViewAllForRecentPayments() {
		String recentPaymentsViewAllCSS = "#box-container > a";
		clickOnAnElement(By.cssSelector(recentPaymentsViewAllCSS));
	}
	public void clickOnRecurrent() {
		String recurrentTabLibkText = "Recurrent";
		clickOnAnElement(By.linkText(recurrentTabLibkText));
	}

	public void clickOnBeneficiaries() throws InterruptedException {
		String beneficiaryTabLinkText = "Beneficiaries";
		clickOnAnElement(By.linkText(beneficiaryTabLinkText));
		Thread.sleep(3000);
	}
	public void viewBeneficiaryList() {
		String noBeneficiaryPlaceHolderCSS = "div.lum-placeholder.display-flex.column.center-x.center-y.p-y-2";
		String ULXpath =  "//*[@class='reset-list-style lum-section-list']";
		 if(isElementPresent(By.cssSelector(noBeneficiaryPlaceHolderCSS))) {
			 test.log(Status.INFO, "We have no beneficiaries");
		 }else {
			 listULItems(By.xpath(ULXpath));
		 }

            
	}
	public void clickOnTheFirstBeneficiary() {
		String firstBeneficiaryCSS =  "//*[@id=\"box-container\"]/ul/li[1]/div[2]/ul/li[1]";
		clickOnAnElement(By.cssSelector(firstBeneficiaryCSS));
	}
	public void clickOnNewPaymentIcon() {
		String paymentIconXpath = "//*[text()='New payment']";
		clickOnAnElement(By.xpath(paymentIconXpath));
	}
	public void clickOnNewBeneficiaryIcon() {
		String newBeneficiaryIconXpath = "//*[text()='New beneficiary']";
		clickOnAnElement(By.xpath(newBeneficiaryIconXpath));
	}
	public void clickOnChooseAnExistingBeneficiary() {
		String chooseAnExistingBeneficiaryXpath = "//*[text()='Choose an existing beneficiary']";
		clickOnAnElement(By.xpath(chooseAnExistingBeneficiaryXpath));
	}
	public void clickOnTransferBetweenMyOwnAccounts() {
		String chooseTransferBetweenMyOwnAccountsXpath = "//*[text()='Transfer between my accounts']";
		clickOnAnElement(By.xpath(chooseTransferBetweenMyOwnAccountsXpath));
	}
	public void selectAbeneficiary(String ben) {
		String selectBeneficiaryCSS = "input.search-input";
		sendKeysToATextBoxAndClickEnter(By.cssSelector(selectBeneficiaryCSS),ben);
	}
	public void selectVacationAccount() {
		String vacationXpath = "//*[text()='Vacation']";
		clickOnAnElement(By.xpath(vacationXpath));
	}
	public void clickOnModify() {
		String modifyXpath = "//*[text()='Modify']";
		if(isElementPresent(By.xpath(modifyXpath))) {
			clickOnAnElement(By.xpath(modifyXpath));	
		}else {
			test.log(Status.INFO, "This company has only one account");
		}
	}
    public void enterPayFrom() {
    	if(accountHasNoBalance()) {
    		clickOnModify();
    		selectTheFirstBeneficiary();
    	}else {
    		test.log(Status.INFO, "The account has balance");
    	}
    	
    }

    public void enterPayTo() {
    	clickOnChooseAnExistingBeneficiary();
    	selectTheFirstBeneficiary();
    	
    }
    public boolean fXIsOn() throws InterruptedException {
    	String searchPerCurrencyXpath =  "//*[text()='Search per currency']";
    	clickOnPayments();
    	clickOnBeneficiaries();
    	clickOnFilter();
    	return isElementPresent(By.xpath(searchPerCurrencyXpath));
    }
   public void makeApayment() throws InterruptedException {
	   clickOnPayments();
	   clickOnNewPaymentIcon();
	   enterPayFrom();
	   enterPayTo();
	   if(isSameCurrencyPayment()) {
			 accountsPage.enterYouSendAmount();
			 accountsPage.selectInterledgerPaymentScheme();
			 accountsPage.enterConcept();
			 accountsPage.clickOnImmediatePayment();
			 accountsPage.clickOnConfirmButton();
			 accountsPage.enterNotifyByEmail();
			 accountsPage.clickOnConfirmButton();
			 accountsPage.enterSecurityCode();
	   }else
	   {
		   accountsPage.enterYouSendAmount();
		   enterReference();
		   enterConcept();
		   accountsPage.clickOnConfirmButton();
		   accountsPage.enterNotifyByEmail();
		   accountsPage.clickOnConfirmButton();
		   accountsPage.enterSecurityCode();
	   }
	   
   }
   public void assertMessageIsDisplayedForCB() {
	   String errorMsgCSS = "div.lum-box.lum-warning-box.m-b-2";
	   assertElementIsDisplayed(By.cssSelector(errorMsgCSS));
	   
   }
	public void enterYouSendAmount() {
		String youSendName = "amount";
		sendKeysToATextBox(By.name(youSendName),"100");
	}
	public void assertPaymentStatementIsAttachedToApayment() {
		String paymentstatementXpath =  "//*[text()='Download statement']";
		assertElementIsDisplayed(By.xpath(paymentstatementXpath));
	}
	public void clickOnRandomBeneficiary() {
		String randomBeneficiaryXpath =  "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		clickOnARandomULListElement(By.xpath(randomBeneficiaryXpath));
	}
	public void selectGivenCurrencyBeneficiary(String currency) {
		String randomBeneficiaryXpath =  "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		randomlySelectListElementByVisibleText(By.xpath(randomBeneficiaryXpath),currency);
	}
	public void enterReference() {
		String referenceName = "reference";
		sendKeysToATextBox(By.name(referenceName),"test reference");
	}
	public void assertBeneficiaryDetailsAreWellDisplayed() {
		String beneficiaryDetailsXpath = "//*[text()='Beneficiary details']";
		String nameCSS ="div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(1)";
		String textHolderCSS = "div.info.lum-truncate.lum-fs-16";
		String typeCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(2)";
		String currencyCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(3)";
		String accountNumberCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(4)";
		String sortcodeCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(5)";
		String bankCountryCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(6)";
		String addressCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(7)";
		String cityCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(8)";
		String postalCodeCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(9)";
		String residenceCountryCSS = "div.lum-info.display-flex.column.flex.lum-truncate.payee-info:nth-child(10)";
		assertElementIsNotEmpty(By.xpath(beneficiaryDetailsXpath));
		assertElementIsNotEmpty(By.cssSelector(nameCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(typeCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(currencyCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(accountNumberCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(sortcodeCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(bankCountryCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(addressCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(cityCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(postalCodeCSS).cssSelector(textHolderCSS));
		assertElementIsNotEmpty(By.cssSelector(residenceCountryCSS).cssSelector(textHolderCSS));
				
	}
	public void SelectRandomBeneficiary() {
		String randomBeneficiaryXpath = "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		randomlySelectListElementByVisibleText(By.xpath(randomBeneficiaryXpath),"GBP");
	}
	public void enterAmount(String amt) {
		String amountName = "amount";
		sendKeysToATextBox(By.name(amountName),amt);
	}
	public void enterConcept() {
		String descriptionName = "concept";
		sendKeysToATextBox(By.name(descriptionName),"Test concept");
	}
	public void clickOnPaymentConfirmButton() {
		String confirmPaymentXpath = "//button[contains(text(),'Confirm')]";
		clickOnAnElement(By.xpath(confirmPaymentXpath));	
	}
	public void confirmTheSecondTime() {
		String confirmTheSecondTimeCSS = "#box-container > div > button.btn.btn-big.btn-green.m-x-1";
		clickOnAnElement(By.cssSelector(confirmTheSecondTimeCSS));
	}
	public void enterSecurityCode(String co) {
		String securityCodeName = "code";
		sendKeysToATextBox(By.name(securityCodeName),co);
	} 
	public void confirm() {
		String sendButtonCSS = "body > div:nth-child(14) > div > div > div > div.flex > form > div.display-flex.center-y.center-x.flex > button";
		clickOnAnElement(By.cssSelector(sendButtonCSS));
	}
	public void closeEnterSecurityCode() {
		String closeSecurityPopUpCSS = "body > div:nth-child(13) > div > div > div > svg";
		clickOnAnElement(By.cssSelector(closeSecurityPopUpCSS));
	}
	public void enterScheduledPaymentDate(String dat) {
		String scheduledPaymentDateName = "transferConfig.startDate";
		sendKeysToATextBox(By.name(scheduledPaymentDateName),dat);
	}
	public void enterRecurringFrequency() {
		Select frequency = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div:nth-child(4) > div > div:nth-child(4) > div > select"))));
		frequency.selectByVisibleText("Monthly");
	}
	public void enterFirstTransferDate(String ftd) {
		String firstDateTransferName  = "transferConfig.startDate";
		sendKeysToATextBox(By.name(firstDateTransferName),ftd);
	}
	public void clickOnRepeatUntilCancelled() {
		String repeatCSS = "#box-container > form > div:nth-child(4) > div > div:nth-child(7) > svg";
		clickOnAnElement(By.cssSelector(repeatCSS));
	}
	public void clickOnCompanyCombobox() {
		String companyComboBoxXpath = "//*[text()='Company']";
		clickOnAnElement(By.xpath(companyComboBoxXpath));
	}
	public void clickOnIndividualCombobox() {
		String individualComboBoxCSS = "#box-container > div > div:nth-child(2) > svg";
		clickOnAnElement(By.cssSelector(individualComboBoxCSS));
	}
	public void enterCompanyName() {
		String companyNameName = "name";
		sendKeysToATextBox(By.name(companyNameName),"TEST BENEFICIARY");	
	}

	public void enterCountryOfRegistration() {
		Select countryOfRegistration = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.payee-inputs-container > div:nth-child(2) > div > select"))));
		countryOfRegistration.selectByVisibleText("United Kingdom");
	}
	public void enterCity() {
		String cityName = "addressCity";
		sendKeysToATextBox(By.name(cityName),"LLANGURIG");
	}
	public void enterAddress() {
		String addressName = "addressStreet";
		sendKeysToATextBox(By.name(addressName),"99 Constitution St");
	}
	public void enterPostalCode(){
		String postalCodeName = "addressPostalCode";
		sendKeysToATextBox(By.name(postalCodeName),"SY18 8YW");
	}
	public void enterSortCode(String sortC) {
		String sortCodeName = "ukSortCode";
		sendKeysToATextBox(By.name(sortCodeName),sortC);
	}
	public void enterAccountNumber(String accNo) {
		String accNumberName  = "ukAccountNumber";
		sendKeysToATextBox(By.name(accNumberName),accNo);
	}
	
	public void enterIBAN() throws InterruptedException {
		String iBANname = "iban";
		sendKeysToATextBox(By.name(iBANname),"GB11PAYR00997500000891");
		Thread.sleep(5000);
	}
	public void enterAccountNumber() throws InterruptedException {
		String accNoname = "ukAccountNumber";
		sendKeysToATextBox(By.name(accNoname),"49643097");
		Thread.sleep(5000);
	}
	public void enterSortCode() throws InterruptedException {
		String sortCodename = "ukSortCode";
		sendKeysToATextBox(By.name(sortCodename),"123456");
		Thread.sleep(5000);
	}
	public void enterBICSWIFT(String bicSwift) {
		String bICsWIFTname = "bicSwift";
		sendKeysToATextBox(By.name(bICsWIFTname),bicSwift);
	}
	public void clickOnBeneficiarySaveButton() throws InterruptedException {
		String beneficiarySaveButtonXpath = "//*[text()='Save']";
		clickOnAnElement(By.xpath(beneficiarySaveButtonXpath));
		Thread.sleep(10000);
	}
	public void enterFirstNameWithSpecialChars() {
		String firstNamename = "firstName";
		sendKeysToATextBox(By.name(firstNamename),"名稱");
	}
	public void enterLastNameWithSpecialChars() {
		String lastNamename = "lastName";
		sendKeysToATextBox(By.name(lastNamename),"姓");
	}
	public void assertErrorOnName() {
		String namesHolderCSS = " div.display-flex.column-lt-m m-b-1.center-y:nth-child(1)";
		String nameErrorHolderCSS = "div.display-flex.column.lum-input-box.user-input:nth-child(1)";
		String nameErrorCSS = "span.input-error";
		assertTextPresent(By.cssSelector(namesHolderCSS).cssSelector(nameErrorHolderCSS).cssSelector(nameErrorCSS ), "Field must be Latin characters");
	}
	public void assertErrorOnSurName() {
		String namesHolderCSS = " div.display-flex.column-lt-m m-b-1.center-y:nth-child(2)";
		String nameErrorHolderCSS = "div.display-flex.column.lum-input-box.user-input:nth-child(1)";
		String nameErrorCSS = "span.input-error";
		assertTextPresent(By.cssSelector(namesHolderCSS).cssSelector(nameErrorHolderCSS).cssSelector(nameErrorCSS ), "Field must be Latin characters");
	}
	public void clickOnVerificationCloseButton() {
		String verificationCloseButtonCSS = "body > div.ReactModalPortal > div > div > div > svg > path";
		clickOnAnElement(By.cssSelector(verificationCloseButtonCSS));
	}
	// Filter
	public void clickOnFilter() {
		String filterXpath = "//*[text()='Filter']";
		clickOnAnElement(By.xpath(filterXpath));
	}
	public void orderBYBeneficiaryName() {
		Select orderByBeneficiaryName = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div:nth-child(18) > div > div > div > div > form > div:nth-child(3) > div > select > option:nth-child(1)"))));
		orderByBeneficiaryName.selectByVisibleText("Beneficiary name");
	}
	public void orderBycreationDate() {
		Select orderByCreationDate = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("select.lum-input.lum-select.lum-truncate  "))));
		orderByCreationDate.selectByVisibleText("Creation date");
	}
	public void searchBeneficiaryByName(String nam) {
		String searchByNamename = "search";
		sendKeysToATextBox(By.name(searchByNamename),nam);
	}
	public void clickOnClearFilters() {
		String clearFiltersXpath = "//button[contains(text(),'Clear filters')]";
		clickOnAnElement(By.xpath(clearFiltersXpath));
	}
	public void clickConfirm() {
		String confirmButtonXpath = "//button[contains(text(),'Confirm')]";
		clickOnAnElement(By.xpath(confirmButtonXpath));
	}
	public void clickOnViewAllForFailedPayments() {
		String failedViewAllXpath = "(//*[@id=\"box-container\"]/a)[2]";
		clickOnAnElement(By.xpath(failedViewAllXpath));
	}
	public void clickOnFistElementInUL() {
		String ULXpath = "(//*[@class='payments-list reset-list-style'])[1]";
		clickOnFirstElementOfUL(By.xpath(ULXpath));
	}
	public void assertPaymentSchemeIsDisplayed() {
		String paymentSchemeXpath = "//*[text()='Payment scheme']";
		assertElementIsDisplayed(By.xpath(paymentSchemeXpath));
	}
	public void assertRecentPaymentsAreDisplayed() {
		String recentPaymentsXpath = "//*[text()='Recent payments']";
		String recentViewAllXpath = "(//*[@id=\"box-container\"]/a)[1]";
		String viewAllListXpath= "//*[@class='reset-list-style lum-section-list']";
		String recentPaymentsListXpath =  "(//*[@class='payments-list reset-list-style'])[1]";
		if(isElementPresent(By.xpath(recentPaymentsXpath))) {
			if(isElementPresent(By.xpath(recentViewAllXpath))) {
				clickOnViewAllForFailedPayments();
				listULItems(By.xpath(viewAllListXpath));
			}else {
				listULItems(By.xpath(recentPaymentsListXpath));
			}

		}else {
			test.log(Status.INFO, "we have no Recent payments");
		}
		
	}
	public void assertFailedPaymentsAreDisplayed() {
		String failedPaymentsXpath = "//*[text()='Failed payments']";
		String failedViewAllXpath = "(//*[@id=\"box-container\"]/a)[2]";
		String viewAllListXpath= "//*[@class='reset-list-style lum-section-list']";
		String failedPaymentsListXpath =  "(//*[@class='payments-list reset-list-style'])[2]"; 
		if(isElementPresent(By.xpath(failedPaymentsXpath))) {
			if(isElementPresent(By.xpath(failedViewAllXpath))) {
				clickOnViewAllForFailedPayments();
				listULItems(By.xpath(viewAllListXpath));
			}else {
				listULItems(By.xpath(failedPaymentsListXpath));
			}

		}else {
			test.log(Status.INFO, "we have no failed payments");
		}
		
	}
	//  reset-list-style lum-section-list
	public void assertScheduledPaymentsAreDisplayed() {
		String scheduledPaymentsXpath = "//*[text()='Scheduled payments']";
		String scheduledViewAllXpath = "(//*[@id=\"box-container\"]/a)[3]";
		String viewAllListXpath= "//*[@class='reset-list-style lum-section-list']";
		String scheduledPaymentsListXpath =  "(//*[@class='payments-list reset-list-style'])[3]";
		if(isElementPresent(By.xpath(scheduledPaymentsXpath))) {
			if(isElementPresent(By.xpath(scheduledViewAllXpath))) {
				clickOnViewAllForFailedPayments();
				listULItems(By.xpath(viewAllListXpath));
			}else {
				listULItems(By.xpath(scheduledPaymentsListXpath));
			}

		}else {
			test.log(Status.INFO, "we have no Scheduled payments");
		}
		
	}
	public boolean accountHasNoBalance() {
		String accountBalanceCSS = "div.lum-truncate.lum-color-black-50.lum-fs-14";
		return showElementText(By.cssSelector(accountBalanceCSS)).contains("0.00");	
	}
	//   "(//*[@class='same'])[3]");
	public boolean isSameCurrencyPayment() {
		String payFromCurrencyXpath = "(//*[@class='m-r-1 lum-w500'])[1]";
		String payToCurrencyXpath = "(//*[@class='m-r-1 lum-w500'])[2]";
		String payFromCurrency = showElementText(By.xpath(payFromCurrencyXpath));
		String payToCurrency = showElementText(By.xpath(payToCurrencyXpath));
		return payFromCurrency.equals(payToCurrency);
	}
	public boolean isClearBank() {
		String referenceName = "reference";
		return !isElementPresent(By.name(referenceName));
	}
	public void selectTheFirstBeneficiary() {
		String firstBenXpath = "(//*[@class='reset-list-style lum-fs-16'])[1]";
		if(isElementPresent(By.xpath(firstBenXpath))) {
			clickOnAnElement(By.xpath(firstBenXpath));
		}else {
			test.log(Status.INFO, "No beneficiaries found");
		}
		
	}
	public int getNumberOfAvailableCurrencies() {
		Select drpCurrency = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("select.lum-input.lum-select.lum-truncate  "))));
		return drpCurrency.getOptions().size();


	}
	public void selectEURCurrency() {
		String currencyXpath = "//span[contains(text(),'Currency')]";
		if(isElementPresent(By.xpath(currencyXpath))) {
			Select drpCurrency = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("select.lum-input.lum-select.lum-truncate  "))));
			drpCurrency.selectByVisibleText("€ - EUR");
		}

	}
	public void selectGBPCurrency() throws InterruptedException {
		String currencyXpath = "//span[contains(text(),'Currency')]";
		if(isElementPresent(By.xpath(currencyXpath))) {
			Select drpCurrency = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("select.lum-input.lum-select.lum-truncate  "))));
			drpCurrency.selectByVisibleText("£ - GBP");
			Thread.sleep(3000);
		}

	}
	public void deleteBeneficiary() throws InterruptedException {
		String deleteBeneficiaryXpath = "//*[text()='Delete']";
		clickOnAnElement(By.xpath(deleteBeneficiaryXpath));
		Thread.sleep(3000);
	}
	public void acceptDeleteBeneficiary() throws InterruptedException {
		String acceptDeleteBeneficiaryXpath = "//*[text()='Accept']";
		clickOnAnElement(By.xpath(acceptDeleteBeneficiaryXpath));
		Thread.sleep(2000);
	}
	public void clickOnAPayment() throws InterruptedException {
		String paymentsUlXpath = "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		clickOnARandomULListElement(By.xpath(paymentsUlXpath));
		Thread.sleep(2000);
	}
	// 
	public void enterSpainResidenceCountry() {   // select.lum-input.lum-select.lum-truncate  
		String residenceCountryXpath =  "(//*[@class='lum-input lum-select lum-truncate  '])[2]";
		Select individualFxResidenceCountry = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(residenceCountryXpath))));
		individualFxResidenceCountry.selectByVisibleText("Spain");
	}
	public void enterUKResidenceCountry() throws InterruptedException {   // select.lum-input.lum-select.lum-truncate  
		String residenceCountryXpath =  "(//*[@class='lum-input lum-select lum-truncate  '])[2]";
		Select individualFxResidenceCountry = new Select(wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(residenceCountryXpath))));
		individualFxResidenceCountry.selectByVisibleText("United Kingdom");
		Thread.sleep(2000);
	}


}
