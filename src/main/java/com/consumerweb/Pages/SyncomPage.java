package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.aventstack.extentreports.Status;

public class SyncomPage extends ConsumerWebBaseClass{
	AccountsPage accountsPage;
	PaymentsPage paymentsPage;
	WebDriver driver;
	WebDriverWait wait;
	
	public SyncomPage(WebDriver driver){
        this.driver = driver;
        //  This initElements method will create all WebElements
        PageFactory.initElements(driver, this);
        wait = new WebDriverWait(driver, 5);  
        accountsPage =  new AccountsPage(driver);
        paymentsPage = new PaymentsPage(driver);
    }
	public void clickOnAddAnAccountButton() throws InterruptedException {
		accountsPage.clickOnAccountsMenuOption();
		accountsPage.clickOnAddAnAccountButton();
	}
	public void saveAndDeleteAccount() throws InterruptedException {
		accountsPage.clickOnSaveAccountButton();
		accountsPage.clickOnDots();
		accountsPage.clickOnCancelAccount();
		accountsPage.clickOnCancelAndRemoveFromView();
		accountsPage.clickOnConfirmCancellationButton();
	}

	
	
	public void createAndDeleteAllAccountTypesinCB() throws InterruptedException {
		String gBPaccount = "//*[text()='GBP account']";
		String eURaccount = "//*[text()='EUR account']";
		String cHFaccount= "//*[text()='CHF account']";
		String cZKaccount = "//*[text()='CZK account']";
		String dKKaccount = "//*[text()='DKK account']";
		String hUFaccount = "//*[text()='HUF account']";
		String nOKaccount = "//*[text()='NOK account']";
		String pLNaccount = "//*[text()='PLN account']"; 
		String rONaccount = "//*[text()='RON account']";
		String sEKaccount = "//*[text()='SEK account']";
		String uSDaccount = "//*[text()='USD account']";
		for(int x=0;x<=10;x++) {
			if (x==0) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(gBPaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("GBP TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "GBP TESTING ACCOUNT created and deleted successfully");
			}
			if(x == 1) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(eURaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("EUR TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "EUR TESTING ACCOUNT created and deleted successfully");
			}
			if(x == 2) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(cHFaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("CHF TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "CHF TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==3) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(cZKaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("CZK TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "CZK TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==4) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(dKKaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("DKK TESTING ACCOUNT");
				saveAndDeleteAccount();
				Thread.sleep(2000);
				test.log(Status.INFO, "DKK TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==5) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(hUFaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("HUF TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "HUF TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==6) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(nOKaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("NOK TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "NOK TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==7) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(pLNaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("PLN TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "PLN TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==8) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(rONaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("RON TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "RON TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==9) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(sEKaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("SEK TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "SEK TESTING ACCOUNT created and deleted successfully");
			}
			if(x ==10) {
				clickOnAddAnAccountButton();
				accountsPage.clickOnAnElement(By.xpath(uSDaccount));
				Thread.sleep(1000);
				accountsPage.enterAccountAlias("USD TESTING ACCOUNT");
				saveAndDeleteAccount();
				test.log(Status.INFO, "USD TESTING ACCOUNT created and deleted successfully");
			}
			
			else {
				
			}
		}	
	}
	public void selectPayFrom() {
		paymentsPage.enterPayFrom();
	}
	public void selectTransferBetweenMyOwnAccounts() {
		String transferBtnOwnAccounts = "//*[text()='Transfer between my accounts']";
		clickOnAnElement(By.xpath(transferBtnOwnAccounts));	
	}
	public void selectPayToForOwnAccounts() {
		String accountsUL = "ul.reset-list-style.lum-fs-16";
		clickOnARandomULListElement(By.cssSelector(accountsUL));
		
	}
	public void enterYouSendAmount(String amount) throws InterruptedException {
		String youSendName = "sourceCurrencyAmount";
		sendText(By.name(youSendName), amount);
		Thread.sleep(3000);
	}
	public void clickOnConfirm() throws InterruptedException {
		String confirmXpath = "//button[contains(text(),'Confirm')]";
		clickElement(By.xpath(confirmXpath));
		Thread.sleep(3000);
	}
	public void enterPaymentDetails() throws InterruptedException {
		   if(paymentsPage.isSameCurrencyPayment()) {
			     enterYouSendAmount("100");
				 accountsPage.selectInterledgerPaymentScheme();
				 accountsPage.enterConcept();
				 accountsPage.clickOnImmediatePayment();
				 clickOnConfirm();
				 accountsPage.enterNotifyByEmail();
				 clickOnConfirm();
				 accountsPage.enterSecurityCode();
				 assertPaymentSucceeded();
		   }else
		   {
			   enterYouSendAmount("200");
			   paymentsPage.enterReference();
			   paymentsPage.enterConcept();
			   clickOnConfirm();
			   accountsPage.enterNotifyByEmail();
			   clickOnConfirm();
			   accountsPage.enterSecurityCode();
			   assertPaymentSucceeded();
		   }
	}
	public void chooseAnExistingBeneficiary() {
		String chooseAnExistingBeneficiaryXpath = "//*[text()='Choose an existing beneficiary']";
		clickElement(By.xpath(chooseAnExistingBeneficiaryXpath));
	}
	public void selectPayFrom(String text) {
		String ulXpath = "//ul[contains(@class,'reset-list-style lum-fs-16')]/li";
		randomlySelectListElementByVisibleText(By.xpath(ulXpath),text);
		
	}
	public void selectArandomBeneficiaryBycurrency(String curr) {
		String beneficiaryULxpath = "//ul[contains(@class,'reset-list-style lum-section-list')]/li/div/ul/li";
		randomlySelectListElementByVisibleText(By.xpath(beneficiaryULxpath),curr);
	}
	public void selectUKfasterPayments() throws InterruptedException {
		String paymentSchemeCSS = "select.lum-input.lum-select.lum-truncate  ";
		String interledgerXpath = "//*[@id=\"box-container\"]/form/div[3]/div[2]/div[2]/div/select/option[2]";
        clickOnAnElement(By.cssSelector(paymentSchemeCSS));
		Thread.sleep(1500);
		clickOnAnElement(By.xpath(interledgerXpath));
	}
	public void clickOnAccountSettings() throws InterruptedException {
		String accountSettingsXpath = "(//a[@href='/account-settings']/div)[1]";
		clickElement(By.xpath(accountSettingsXpath));
	}
	public void clickOnDefaultCurrency() {
		String defaultCurrencyXpath = "Default currency";
		clickElement(By.linkText(defaultCurrencyXpath));
	}
	public void selectCurrency() throws InterruptedException {
		String selectCurrXpathGBP = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='GBP']";
		String selectCurrXpathNOK = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='NOK']";
		String selectCurrXpathRON = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='RON']";
		String selectCurrXpathCHF = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='CHF']";
		String selectCurrXpathSEK = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='SEK']";
		String selectCurrXpathCZK = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='CZK']";
		String selectCurrXpathDKK = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='DKK']";
		String selectCurrXpathEUR = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='EUR']";
		String selectCurrXpathHUF = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='HUF']";
		String selectCurrXpathPLN = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='PLN']";
		String selectCurrXpathUSD = "//select[contains(@class, 'lum-input lum-select lum-truncate')]/option[text()='USD']";
        for(int x=0;x<=10;x++ ) {
        	if (x==0) {
        		if(isElementPresent(By.xpath(selectCurrXpathGBP))) {
            		clickElement(By.xpath(selectCurrXpathGBP));
            		clickOnSaveButton();
            		Thread.sleep(5000);
            		checkRightCurrencyIsDisplayedInExpenses("£");
            		Thread.sleep(2000);
            		test.log(Status.INFO, "Default currency set to GBP successfully");
        		}else {
        			test.log(Status.INFO, "No GBP currency");
        		}

        	}
        	if (x==1) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		
            		if(isElementPresent(By.xpath(selectCurrXpathNOK))) {
            			clickElement(By.xpath(selectCurrXpathNOK));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("kr");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to NOK successfully");
        		}else {
            		test.log(Status.INFO, "No NOK currency");
        		}

        	}
        	if (x==2) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathRON))) {
            			clickElement(By.xpath(selectCurrXpathRON));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("lei");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to RON successfully");
        		}else {
        			test.log(Status.INFO, "No RON currency");
        		}

        	}
        	if(x==3) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathCHF))) {
            			clickElement(By.xpath(selectCurrXpathCHF));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("CHF");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to CHF successfully");
        		}else {
        			test.log(Status.INFO, "No CHF currency");
        		}

        	}
        	if(x==4) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathSEK))) {
            			clickElement(By.xpath(selectCurrXpathSEK));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("kr");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to SEK successfully");
        		}else {
        			test.log(Status.INFO, "No SEK currency");
        		}

        	}
        	if(x==5) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathCZK))) {
            			clickElement(By.xpath(selectCurrXpathCZK));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("Kč");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to CZK successfully");
        		}else {
        			test.log(Status.INFO, "No CZK currency");
        		}

        	}
        	if(x==6) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathDKK))) {
            			clickElement(By.xpath(selectCurrXpathDKK));
            			clickOnSaveButton();
            			Thread.sleep(7000);
            			checkRightCurrencyIsDisplayedInExpenses("kr");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to DKK successfully");
        		}else {
        			test.log(Status.INFO, "No DKK currency");
        		}
        		
        	}
        	if(x==7) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathEUR))) {
            			clickElement(By.xpath(selectCurrXpathEUR));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("€");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to EUR successfully");
        		}else {
        			test.log(Status.INFO, "No EUR currency");
        		}

        	}
        	if(x==8) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathHUF))) {
            			clickElement(By.xpath(selectCurrXpathHUF));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("Ft");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to HUF successfully");
        		}else {
        			test.log(Status.INFO, "No HUF currency");
        		}

        	}
        	if(x==9) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
               		if(isElementPresent(By.xpath(selectCurrXpathPLN))) {
               			clickElement(By.xpath(selectCurrXpathPLN));
               			clickOnSaveButton();
               			Thread.sleep(5000);
               			checkRightCurrencyIsDisplayedInExpenses("zł");
               			Thread.sleep(2000);
               			test.log(Status.INFO, "Default currency set to PLN successfully");
        		}else {
        			test.log(Status.INFO, "No PLN currency");
        		}

        	}
        	if(x==10) {
            		clickOnAccountSettings();
            		clickOnDefaultCurrency();
            		if(isElementPresent(By.xpath(selectCurrXpathUSD))) {
            			clickElement(By.xpath(selectCurrXpathUSD));
            			clickOnSaveButton();
            			Thread.sleep(5000);
            			checkRightCurrencyIsDisplayedInExpenses("$");
            			Thread.sleep(2000);
            			test.log(Status.INFO, "Default currency set to USD successfully");
        		}else {
        			test.log(Status.INFO, "No USD currency");
        		}
        			

        	}
        	
        }
	
	}
	public void clickOnSaveButton() {
		String saveButtonXpath = "//button[contains(text(),'Save')]";
		clickElement(By.xpath(saveButtonXpath));
	}
	public void checkRightCurrencyIsDisplayedInExpenses(String currencySymbol) {
		String expensesLinkText = "Expenses";
		String expensesCurrencyXpath = "//span[@class='lum-truncate lum-fs-40']";
		clickElement(By.linkText(expensesLinkText));
		assertCorrectText(By.xpath(expensesCurrencyXpath), currencySymbol);
	}
	public void  assertPaymentSucceeded() {
		String paymentstatusXpath = "//*[@id=\"box-container\"]/div[2]/div[2]/div[2]";
		Assert.assertEquals(showElementText(By.xpath(paymentstatusXpath)),"Withheld");
	}
}
