package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CredentialsSetUp {
	
	WebDriver driver;
	WebDriverWait wait;
	WebElement userName,passWord,confirmPassword,securityCode,endUserAgreement,AccountTerms,cardHolderTerms,registerBtn;

	    
	public CredentialsSetUp(WebDriver driver){

	        this.driver = driver;
	        //This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 10);
	        
	    }        

	//Enter User name
	public void enterUserName(String uName) {
		userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("username")));
		userName.clear();
		userName.sendKeys(uName);
	}
	public void enterPassword(String pass) {
		passWord = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
		passWord.clear();
		passWord.sendKeys(pass);
		
	}
	public void confirmPassword(String confPass) {
		confirmPassword = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("confirmPassword")));
		confirmPassword.clear();
		confirmPassword.sendKeys(confPass);
		
	}
	public void enterSecurityCode(String secCode) {
		securityCode = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("code")));
		securityCode.clear();
		securityCode.sendKeys(secCode);
		
	}
	public void acceptEndUserAgreement() {
		endUserAgreement = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(1) > div > div > svg")));
		endUserAgreement.click();
	}
	public void acceptAccountTerms() {
		AccountTerms = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(2) > div > div > svg")));
		AccountTerms.click();
	}
	public void acceptCardHolderTerms() {
		cardHolderTerms = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(7) > div:nth-child(3) > div > div > svg")));
		cardHolderTerms.click();
	}
	public void clickOnRegister() {
		registerBtn = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Register")));
		registerBtn.click();
	}

}
