package com.consumerweb.Pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AdminPage {
	WebDriver driver;
	WebDriverWait wait;
	WebElement userName,password,loginButton,clients,searchByClient,clickOnSearchButton,foundClient,reject,delete,rejectReason,holderDiv,acceptButton,acceptDeleteButton,deleteClientHolderDiv;
	Select drpTeam,drpMember;
	Boolean referralsButtonIsDisplayed;
	String test,assignReferralTitle;

	    
	public AdminPage(WebDriver driver){

	        this.driver = driver;
	        //This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 10);
	        
	    }     
	public void enterUsername(String uName) {
		userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("email")));
		userName.sendKeys(uName);
		
	}
	public void enterPassword(String pass) {
		password = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("password")));
		password.sendKeys(pass);
	}
	public void clickLoginButton() {
		loginButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Login')]")));
		loginButton.click();
	}
	public void clickOnClients() {
		clients = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Clients")));
		clients.click();
	}
	public void searchByClientName(String clientNm) {
		searchByClient = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("text")));
		searchByClient.sendKeys(clientNm);
	}
	public void clickOnSearchButton() {
		clickOnSearchButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Search')]")));
		clickOnSearchButton.click();
	}
	public void clickOnClient() {
		foundClient = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > table > tbody")));
		foundClient.click();
	}
	public void clickOnReject() {
		reject = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.center-x.m-t-4 > button")));
		reject.click();
	}
	public void clickOnAccept() {
		acceptButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Accept')]")));
		acceptButton.click();
	}
	//
	public void clickOnDelete() {
		delete = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.center-x.m-t-4 > button")));
		delete.click();
	}
	//body > div:nth-child(9) > div > div
	public void enterRejectReason(String message) {
		//driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
		holderDiv = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div:nth-child(9) > div > div > div")));
		rejectReason = holderDiv.findElement(By.name("message"));
		rejectReason .sendKeys(message);
	}
	public void acceptDeleteClient() {
		deleteClientHolderDiv =   wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("body > div:nth-child(8) > div > div")));
		acceptDeleteButton = deleteClientHolderDiv.findElement(By.id("modal-confirm-button"));
		acceptDeleteButton.click();
	}
}
