package com.consumerweb.Pages;

import java.util.ArrayList;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.Status;

public class TranslationsPage extends ConsumerWebBaseClass{
	WebDriver driver;
	WebDriverWait wait;
	AccountsPage accountsPage;

	    
	public TranslationsPage(WebDriver driver){
	        this.driver = driver;
	        //  This initElements method will create all WebElements
	        PageFactory.initElements(driver, this);
	        wait = new WebDriverWait(driver, 15);   
	    }
	public void verifyEnglishTranslations() {
		String noPricingModelsCSS = "div.lum-placeholder.display-flex.column.center-x.center-y";
		String englishPricingModelTitleCSS = "div.title.m-t-0";
	    if(isElementPresent(By.cssSelector(noPricingModelsCSS))) {
	    	test.log(Status.INFO,"There are no pricing models");
	    }else {
	    	assertCorrectText(By.cssSelector(englishPricingModelTitleCSS), "Pricing models");
	    }
	
	}
	public void clickOnPricing(String pricingText) throws InterruptedException {
		 Thread.sleep(5000);
		String pricingXpath = "//*[text()='"+pricingText+"']";
		clickElement(By.xpath(pricingXpath));
	}
	public void switchToTheNextWindow() throws InterruptedException {
        ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs.get(1));
        System.out.println(driver.getTitle());
        System.out.println(driver.getCurrentUrl());
	    Thread.sleep(5000);
	}
	public void closeBrowserAndSwitchToWindowOne() {
	    driver.close();
        ArrayList<String> tabs1 = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tabs1.get(0));
	    refreshBrowser();
	}
	public void clickOnpersonalSettings() throws InterruptedException {
		String personalSettingsCSS = "div.lum-avatar.display-flex.center-x.center-y";
		String userSettingsXpath = "//*[text()='User settings']";
		clickOnAnElement(By.cssSelector(personalSettingsCSS));
		Thread.sleep(2000);
		clickOnAnElement(By.xpath(userSettingsXpath));	
	}
	public void clickOnLanguage() {
		String LanguageXpath = "//*[text()='Language']";
		clickOnAnElement(By.xpath(LanguageXpath));
	}
	public void clickOnSave(String saveText) throws InterruptedException {
		String saveXpath = "//*[text()='"+saveText+"']";
		clickOnAnElement(By.xpath(saveXpath));
		Thread.sleep(6000);
	}
	public void selectLanguage(String languageText) {
		String spanishXpath = "//select[contains(@class, 'lum-input lum-select lum-truncate ')]/option[text()='"+languageText+"']";
		clickOnAnElement(By.xpath(spanishXpath));
	}
	public void verifySpanishTranslations() {
		String noPricingModelCSS = "div.lum-placeholder.display-flex.column.center-x.center-y";
		String spanishPricingModelHeaderCSS = "div.title.m-t-0";
		if(isElementPresent(By.cssSelector(noPricingModelCSS))) {
			test.log(Status.INFO,"There are no pricing models");

		}else {
			assertCorrectText(By.cssSelector(spanishPricingModelHeaderCSS), "Modelo de precios");
	    }

	}
	public void verifyGermanTranslations() {
		String noPricingModelCSS = "div.lum-placeholder.display-flex.column.center-x.center-y";
		String germanPricingModelHeaderCSS = "div.title.m-t-0";
		if(isElementPresent(By.cssSelector(noPricingModelCSS))) {
			test.log(Status.INFO,"There are no pricing models");

		}else {
			assertCorrectText(By.cssSelector(germanPricingModelHeaderCSS), "Preismodelle");
	    }
    }

	public void verifyTranslationsToEnglish() {
		String cardsLinkText = "Cards";
		String peopleLinkText ="People";
		String paymentsLinkText = "Payments";
		String accountsLinkText = "Accounts";
		assertCorrectText(By.linkText(cardsLinkText), "Cards");
		assertCorrectText(By.linkText(peopleLinkText), "People");
		assertCorrectText(By.linkText(paymentsLinkText), "Payments");
		assertCorrectText(By.linkText(accountsLinkText), "Accounts");
	}
	public void refreshBrowser() {
		driver.navigate().refresh();
		}

}
