package com.consumerweb.Testcases;


import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;

public class LoginPageTest extends ConsumerWebBaseClass {
	LoginPage loginPage;
	String passwordErrorMessage,welcomeUserText,url;
	
	
	

    @BeforeClass
    public void setup() throws Exception{
    	init();
		loginPage = new LoginPage(driver);
        
    }
    
    
	@Test(priority = 0)
	public void verifyUserWithValidUserNameEmptyPassCannotLogin() throws InterruptedException {
		test = extent.createTest("TOQ 85:Verify User With Valid User Name and an Empty Password Cannot Login");
		loginPage.enterUserName(username);
		loginPage.clickOnLogin();
		loginPage.assertPasswordErrorMessage();
	}
	
	@Test(priority = 1)
	public void verifyUnregisteredUserNotAbleToLogin() throws InterruptedException {
		test = extent.createTest("TOQ 84: Verify Unregistered User is Not Able To Login");
		loginPage.enterUserName("Howard");
		loginPage.enterPassword(password);
		loginPage.clickOnLogin();
	}
	
	@Test(priority = 2)
	public void verifyUserAllowedToLandOnACertainURL() throws InterruptedException {
		test = extent.createTest("TOQ 86: Verify user allowed to land on a certain URL");
		url= getUrl(OR.getProperty("url"));
		driver.get(url+"people-expenses/person");
		loginPage.verifyLoginPageIsDisplayed();

	}
	 @Test(priority = 3)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("TOQ 83 :Verify successful login");
		    login();
		}
	
	@AfterTest
	public void shutDown() {
		driver.quit();
	}
	
	
}
