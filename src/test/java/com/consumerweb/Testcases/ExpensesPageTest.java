package com.consumerweb.Testcases;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.ExpensesPage;
import com.consumerweb.Pages.LoginPage;

public class ExpensesPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	ExpensesPage expensesPage;
	LoginPageTest loginPageTest;
	String welcomeUserText,title,totalSpent,mainAccDetails,IBAN,SWIFTForSEPA,userName,amountSpent,categoryName,categoryAmount,expenseName,expenseAmount;
	Boolean notificationsButtonIsDisplayed,userButtonIsDisplayed,peopleTabIsDisplayed,categoryTabIsDisplayed,addNewExpenseButtonIsDisplayed,listOfBeneficiariesDisplayed,userNameIsDisplayed,transactionAmountIsDisplayed,categoryAmountIsDisplayed,expenseAmoutIsDisplayed,
	expenseDateIsDisplayed,expenseTimeIsDisplayed,ExpenseStatusIsDisplayed,TransactionOwnerIsDisplayed,VATIsDisplayed,totalWithoutVATIsDisplayed,paymentDetailsIsDisplayed,categoryIsDisplayed,InvoiceDetailIsDisplayed,transactionReceiptIsDisplayed,notesSectionIsDisplayed,
	commentsSectionIsDisplayed,sendButtonIsDisplayed,expenseNameIsDisplayed,categoryNameIsDisplayed;
	WebElement expenses;
	ConsumerWebBaseClass consumerWebBaseClass;
	String receiptPath = System.getProperty("user.dir") + "/uploads/euroinvoice.png";
	
	
	@BeforeClass
	    public void setup() throws Exception {
		    init();
			loginPage = new LoginPage(driver);
			expensesPage = new ExpensesPage(driver);
	    }
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifyExpensesPageIsDisplayed() throws InterruptedException {
		 test = extent.createTest("TOQ 598:Verify expenses Page is displayed");
		 Thread.sleep(3000);
		 if(isElementPresent(By.linkText("Expenses"))) {
		 expensesPage.clickonExpenses();
		 title = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div.lum-title-container.column-lt-m > div.lum-title.lum-expenses-title"))).getText();
		 Assert.assertEquals(title, "Expenses");
		 totalSpent = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span.lum-truncate.lum-fs-40"))).getText();
		 System.out.println(totalSpent);
		 peopleTabIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("People"))).isDisplayed();
		 Assert.assertTrue(peopleTabIsDisplayed);
		 categoryTabIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Category"))).isDisplayed();
		 Assert.assertTrue(categoryTabIsDisplayed);
		 addNewExpenseButtonIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div.lum-expenses-buttons > button"))).isDisplayed();
		 Assert.assertTrue(addNewExpenseButtonIsDisplayed);
         List<WebElement> expenses =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("item-list-separator")));
         test.log(Status.INFO, "The expenses page has :"+expenses.size()+" people ");
         System.out.println(expenses.size());
            for (WebElement webElement : expenses) {
	            String name = webElement.getText();
	            System.out.println("==========================================================================");
	            System.out.println(name);
	            test.log(Status.INFO, name);
            }
	        }else {
	         expensesPage.clickOnAnalysis();
			 title = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div.title-container.column-lt-m > div.lum-title.analysis-title"))).getText();
			 Assert.assertEquals(title, "Analysis");
			 totalSpent = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div > div.title-container.column-lt-m > div:nth-child(2) > span"))).getText();
			 System.out.println(totalSpent);
			 peopleTabIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("People"))).isDisplayed();
			 Assert.assertTrue(peopleTabIsDisplayed);
			 categoryTabIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("Category"))).isDisplayed();
			 Assert.assertTrue(categoryTabIsDisplayed);
	         List<WebElement> people =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("item-list-separator")));
	         test.log(Status.INFO, "The Analysis page has :"+people.size()+" people ");
	         System.out.println(people.size());
	            for (WebElement webElement : people) {
		            String name = webElement.getText();
		            System.out.println("==========================================================================");
		            System.out.println(name);
		            test.log(Status.INFO, name);
	            }
	        }
		 }
	 @Test(priority = 2)
	 public void verifyPersonExpenseDetailsAreDisplayed() {
		 test = extent.createTest("TOQ 599:Verify person expenses details are displayed");
		 expensesPage.clickOnFirstExpense();
		 userNameIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > div.expense-detail-header > div"))).isDisplayed();
		 Assert.assertTrue(userNameIsDisplayed);
		 transactionAmountIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > span"))).isDisplayed();
		 Assert.assertTrue(transactionAmountIsDisplayed);
		 userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > div.expense-detail-header > div"))).getText();
		 amountSpent = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id=\"app-container-body\"]/div/div/div/div[2]/div/div[1]/span"))).getText();
		 System.out.println("User: "+userName+"......spent......."+amountSpent);
		 
		 List<WebElement> expensesPerUser =  wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > ul"))).findElements(By.xpath("//*[@id=\"box-container\"]/ul/li"));
         System.out.println("User   "+userName+ "has ..."+expensesPerUser.size()+"...expenses");
         test.log(Status.INFO, "User  "+userName+ "has "+expensesPerUser.size()+" expenses");
            for (WebElement webElement : expensesPerUser) {
	            String expenseDate = webElement.getText();
	            System.out.println("==========================================================================");
	            System.out.println(expenseDate);
	            test.log(Status.INFO, expenseDate);
	        }
	 }
	 @Test(priority = 3)
	 public void verifyCategoryListIsDisplayed() {
		 test = extent.createTest("TOQ 600: Verify category list is displayed");
		 expensesPage.clickOnBackButtonOnExpenseDetailPage();
		 expensesPage.clickOnCategory();
		 List<WebElement> categoriesList =  wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.className("item-list-separator")));
		 test.log(Status.INFO, "Category tab has ..."+categoriesList.size()+"...categories");
         System.out.println("Category tab has ..."+categoriesList.size()+"...categories");
            for (WebElement webElement : categoriesList) {
	            String expenseDate = webElement.getText();
	            System.out.println("==========================================================================");
	            System.out.println(expenseDate);
	            test.log(Status.INFO, expenseDate);
	        }
	 }
	 @Test(priority = 4)
	 public void verifyACategoryDetailsAreDisplayed(){
		 test = extent.createTest("TOQ 601: Verify a category detail is displayed");
        expensesPage.clickOnFirstCategory();
        categoryName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > div.expense-detail-header > div"))).getText();
        System.out.println("The name of the first category is  "+categoryName);
        test.log(Status.INFO, "The name of the first category is  "+categoryName);
        categoryNameIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > div.expense-detail-header > div"))).isDisplayed();
        Assert.assertTrue(categoryNameIsDisplayed);
        categoryAmountIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > span"))).isDisplayed();
        Assert.assertTrue(categoryAmountIsDisplayed);
        categoryAmount = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container"))).findElement(By.cssSelector("#app-container-body > div > div > div > div.flex > div > div.display-flex.column-lt-m.center-y.m-b-2.lum-title-container > span")).getText();
        System.out.println("The total amount of expenses for this category is:  "+categoryAmount);
       // test.log(Status.INFO, "The total amount of expenses for this category is:  "+categoryAmount);
        List<WebElement> categoryOneExpenses =  wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > ul"))).findElements(By.cssSelector("li.item-list-separator"));
        System.out.println("This category has ..."+categoryOneExpenses.size()+"...expenses");
        test.log(Status.INFO, "This category has ..."+categoryOneExpenses.size()+"...expenses");
           for (WebElement webElement : categoryOneExpenses) {
	            String expenseDate = webElement.getText();
	            System.out.println("==========================================================================");
	            System.out.println(expenseDate);
	            test.log(Status.INFO,expenseDate );
	        }
	 }
	 @Test(priority = 5)
	 public void verifyExpenseDetailsISDisplayed() throws Exception{
		 test = extent.createTest("TOQ 602:Verify expense details is displayed");
		 expensesPage.clickOnFirstCategoryExpense();
		 expenseName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.center-y.column-lt-sm.center-x > div:nth-child(2)"))).findElement(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.center-y.column-lt-sm.center-x > div:nth-child(2) > div.display-flex.title-container.m-t-mobile > span.tx-main-info")).getText();
		 expenseNameIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.center-y.column-lt-sm.center-x > div:nth-child(2) > div.display-flex.title-container.m-t-mobile > span.tx-main-info"))).isDisplayed();
		 Assert.assertTrue(expenseNameIsDisplayed);
		 System.out.println("This expense's name is...:  "+expenseName);
		 test.log(Status.INFO,"This expense's name is...:  "+expenseName);
		 expenseAmount = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.column.amount-container.m-t-mobile"))).getText();
		 System.out.println("This expense total amount is:  "+expenseAmount);
		 test.log(Status.INFO,"This expense's amount is...:  "+expenseAmount);
		 Thread.sleep(600);
		 expenseAmoutIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.column.amount-container.m-t-mobile"))).isDisplayed();
		 Assert.assertTrue(expenseAmoutIsDisplayed);
		 expenseDateIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.tx-date.d-none.d-md-flex > span:nth-child(1)"))).isDisplayed();
		 Assert.assertTrue(expenseDateIsDisplayed);
		 expenseTimeIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.tx-date.d-none.d-md-flex > span.lum-fs-14.lum-color-black-50"))).isDisplayed();
		 Assert.assertTrue(expenseTimeIsDisplayed);
		 ExpenseStatusIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.center-y.column-lt-sm.center-x > div:nth-child(2) > div.display-flex.title-container.m-t-mobile > span.lum-fs-14.display-flex.center-y.d-none.d-md-flex"))).isDisplayed();
		 Assert.assertTrue(ExpenseStatusIsDisplayed);
		 TransactionOwnerIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.space-between-x.column-lt-sm > div.display-flex.column.amount-container.m-t-mobile"))).isDisplayed();
		 Assert.assertTrue(TransactionOwnerIsDisplayed);
		 VATIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.column-lt-m.info-container > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > span"))).isDisplayed();
		 Assert.assertTrue(VATIsDisplayed);
		 totalWithoutVATIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.column-lt-m.info-container > div:nth-child(1) > div.display-flex.center-y.item-container.clickable > div:nth-child(2) > span"))).isDisplayed();
		 Assert.assertTrue(totalWithoutVATIsDisplayed);
		 categoryIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.column-lt-m.info-container > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div.item-content"))).isDisplayed();
		 Assert.assertTrue(categoryIsDisplayed);
		 InvoiceDetailIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.display-flex.column-lt-m.info-container > div:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div.item-content"))).isDisplayed();
		 Assert.assertTrue(InvoiceDetailIsDisplayed);
		 //transactionReceiptIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.m-t-1.info-container > div.display-flex.center-y.item-container > div > div.file-container.lum-semi-bold.clickable.display-flex.center-y"))).isDisplayed();
		 //Assert.assertTrue(transactionReceiptIsDisplayed);
		 notesSectionIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div.m-t-1.info-container > div.display-flex.column.lum-input-box > input"))).isDisplayed();
		 Assert.assertTrue(notesSectionIsDisplayed);
		 commentsSectionIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.lum-tx-detail-comments.m-b-2"))).isDisplayed();
		 Assert.assertTrue(commentsSectionIsDisplayed);
		 sendButtonIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Send')]"))).isDisplayed();
		 Assert.assertTrue(sendButtonIsDisplayed);
		 Thread.sleep(10);
	 }
	@Test(priority = 6)
	 public void verifyUserCanAddAReceiptToAnExpense() throws Exception{
		test = extent.createTest("TOQ 603:Verify user can add a receipt to an expense");
		List<WebElement> receiptAvailable = driver.findElements(By.cssSelector("#box-container > div.m-t-1.info-container > div.display-flex.center-y.item-container > div > div.file-container.lum-semi-bold.clickable.display-flex.center-y"));
		if(receiptAvailable.size()>0) {
			 expensesPage.deleteAreceipt();
			 Thread.sleep(1000);
			 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
			 expensesPage.clickOnUploadAreceipt(receiptPath);
			 expensesPage.clickOnUpload();
		}else {
			expensesPage.clickOnUploadAreceipt(receiptPath);
			expensesPage.clickOnUpload();
		
		}
		//expensesPage.clickOnSave();	
		 
	 }
	
	@Test(priority = 7)
	 public void verifyUserCanDeleteAreceipt() throws Exception{
		 test = extent.createTest("TOQ 605: Verify user can delete an expense");
		 expensesPage.deleteAreceipt();
		 driver.manage().timeouts().implicitlyWait(10,TimeUnit.SECONDS);
		 //Thread.sleep(2000);
		 //expensesPage.clickOnSave();
	 }
	 
	
	 @Test(priority = 8)
	 public void verifyUserCanAddPurchaseInvoice() throws Exception{
		 test = extent.createTest("TOQ 606:Verify user can add a purchase invoice to an expense");
		 expensesPage.editInvoiceDetail();
		 Thread.sleep(8000);
		 if(isElementPresent(By.cssSelector("button.btn.btn-big.btn-green.lum-invoice-add"))) {
			 expensesPage.clickOnAddNewInvoiceButton();
			 expensesPage.enterSupplierName("Clean Distributors");
			 expensesPage.enterCompanyNumber("12345123");
			 expensesPage.enterPostalCode("12345");
			 expensesPage.enterInvoiceNumber("INV 0006");
			 expensesPage.enterInvoiceDate("11/12/2020");
			 expensesPage.enterDueDate("18/12/2020");
			 expensesPage.clickOnaddLine();
			 Thread.sleep(1000);
			 expensesPage.addDescription("Pro Detergent");
			 expensesPage.enterLineAmountWithoutVAT("24000");
			 expensesPage.enterLineAmount("42000");
			 expensesPage.clickOnAddLineConfirmButton();
			 expensesPage.enterDiscountApplied("0");
			 expensesPage.enterTotalWithoutVAT("24000");
			 expensesPage.enterTotal("42000");
			 expensesPage.clickOnsavePurchaseInvoiceButtton();
			 Thread.sleep(5000);
			 expensesPage.clickOnDeleteInvoice();
			 Thread.sleep(5000);
			 expensesPage.toggleTheIsThisAPurchaseInvoiceButton();
			 Thread.sleep(5000);
		 }else {
			 expensesPage.toggleTheIsThisAPurchaseInvoiceButton();
			 Thread.sleep(8000);
			 expensesPage.clickOnAddNewInvoiceButton();
			 expensesPage.enterSupplierName("Clean Distributors");
			 expensesPage.enterCompanyNumber("12345123");
			 expensesPage.enterPostalCode("12345");
			 expensesPage.enterInvoiceNumber("INV 0006");
			 expensesPage.enterInvoiceDate("11/12/2020");
			 expensesPage.enterDueDate("18/12/2020");
			 expensesPage.clickOnaddLine();
			 Thread.sleep(1000);
			 expensesPage.addDescription("Pro Detergent");
			 expensesPage.enterLineAmountWithoutVAT("24000");
			 expensesPage.enterLineAmount("42000");
			 expensesPage.clickOnAddLineConfirmButton();
			 expensesPage.enterDiscountApplied("0");
			 expensesPage.enterTotalWithoutVAT("24000");
			 expensesPage.enterTotal("42000");
			 expensesPage.clickOnsavePurchaseInvoiceButtton();
			 Thread.sleep(5000);
			 expensesPage.clickOnDeleteInvoice();
			 Thread.sleep(5000);
			 expensesPage.toggleTheIsThisAPurchaseInvoiceButton();
			 Thread.sleep(5000);
		 }

	 }
	 
	@Test(priority = 9)
	 public void verifyUserCanSelectNewCurrency() throws InterruptedException {
		test = extent.createTest("TOQ 607:Verify user can select new currency");
		 if(isElementPresent(By.linkText("Expenses"))) {
			 expensesPage.clickonExpenses();
			 Thread.sleep(2000);
			 expensesPage.clickOnAddNewExpense();
			 expensesPage.clickOnGBP();
			 expensesPage.selectCurrency("eur");
			 expensesPage.clickOnFirstCurrencyOption(); 
		 }else {
			 test.log(Status.INFO,"Expenses is disabled");
		 }

		 
	 }
	 
	 @Test(priority = 10)
	 public void verifyUserCanAddNewExpense() throws Exception{
		test = extent.createTest("TOQ 119:Verify user can add a new expense");
		if(isElementPresent(By.linkText("Expenses"))) {
			 expensesPage.clickonExpenses();
			 Thread.sleep(2000);
			 expensesPage.clickOnAddNewExpense();
			 expensesPage.enterNewExpenseAmount("12000");
			 expensesPage.enterNewExpenseMerchant("Great Merchant");
			 expensesPage.enterNewExpenseAddress("Reiseñor 28");
			 expensesPage.enterNewExpenseDate("04/12/2020");
			 expensesPage.enterNewExpenseTime("13:40");
			 Thread.sleep(2000);
			 expensesPage.clickOnExpenseSaveButton();
			 Thread.sleep(500);
		}else {
			 test.log(Status.INFO,"Expenses is disabled");
		 }

		 
		 
	 }
	@Test(priority = 11)
	public void verifyUserCanDeleteExpense() throws Exception{
		Thread.sleep(2000); 
		test = extent.createTest("TOQ 604:Verify user can delete expense");
		if(isElementPresent(By.linkText("Expenses"))) {
			if(isElementPresent(By.cssSelector("button.delete-button"))) {
				expensesPage.clickOnDeleteExpense();
				expensesPage.clickOnConfirmButton();
				Thread.sleep(2000); 
			}else {
				 test.log(Status.INFO,"The expense was never created");
				
			}

		}else {
			 test.log(Status.INFO,"Expenses is disabled");
		 }

	}
	
	
	 public void verifyUserCanAddNotes() throws Exception{
		 test = extent.createTest("TOQ 609:Verify user can add notes");
		 expensesPage.clickonExpenses();
		 expensesPage.clickOnFirstExpense();
		 expensesPage.clickOnPersonOneExpenseOne();
		 expensesPage.addNotes("...To be paid in full");
		 test.log(Status.INFO,"The notes...To be paid in full...were added");
		 expensesPage.clickOnSave(); 
		 
	 }
	
	 public void verifyUserCanAddComments() throws Exception{
		 Thread.sleep(2000); 
		 test = extent.createTest("TOQ 610: Verify user can add comments to an expense");
		 expensesPage.clickonExpenses();
		 expensesPage.clickOnFirstExpense();
		 expensesPage.clickOnPersonOneExpenseOne();
		 Thread.sleep(3000);
		 expensesPage.addComments("To be paid one week to the due date");
		 expensesPage.clickOnSend();
		 test.log(Status.INFO,"Comments");
		 
	 }
	 @Test(priority = 12)
	 public void verifyUserCanUploadAReceipt() throws Exception{
		 Thread.sleep(2000); 
		 test = extent.createTest("TOQ 603:Verify user can upload a receipt to an expense");
		 if(isElementPresent(By.linkText("Expenses"))) {
			 expensesPage.clickonExpenses();
			 expensesPage.clickOnFirstExpense();
			 expensesPage.clickOnPersonOneExpenseOne();
			 if(isElementPresent(By.cssSelector("svg.lum-icon-component-fill.edit-tx-icon.clickable"))) {
				 expensesPage.clickOnEditAnExpense();
				 if(isElementPresent(By.cssSelector("div.preview-image.d-none.d-md-inline-block"))) {
					 expensesPage.clickOnDeleteReceipt();
					 expensesPage.uploadAreceipt(receiptPath);
					 expensesPage.clickOnUpload();
					 Thread.sleep(6000);
					 expensesPage.clickOnDeleteReceipt();
					 Thread.sleep(6000);
					 test.log(Status.INFO,"Receipt uploaded and deleted successfully");
				 }else {
					 expensesPage.uploadAreceipt(receiptPath);
					 expensesPage.clickOnUpload();
					 Thread.sleep(6000);
					 expensesPage.clickOnDeleteReceipt();
					 Thread.sleep(6000);
					 test.log(Status.INFO,"Receipt uploaded and deleted successfully");
				 }
		
			 }else {
				 test.log(Status.INFO,"This is not your expense so you cannot edit it");
			 }
			 
			   }else {
				   test.log(Status.INFO,"Expenses is disabled");
			 }
		 
	 }
	 @Test(priority = 13)
	 public void verifyUserCanScanAreceipt() throws Exception{
		 test = extent.createTest("TOQ 608:Verify user scan a receipt to an expense");
		 if(isElementPresent(By.linkText("Expenses"))) {
			 expensesPage.clickonExpenses();
			 Thread.sleep(2000);
			 expensesPage.clickOnAddNewExpense();
			 Thread.sleep(10000);
			 expensesPage.scanAreceipt(receiptPath);
			 expensesPage.clickOnUpload();
			 Thread.sleep(10000);
			 expensesPage.clickOnDeleteReceipt();
			 Thread.sleep(6000);
			 expensesPage.clickOnSave();
			 test.log(Status.INFO,"Receipt scanned and deleted successfully");
		 }else {
			 test.log(Status.INFO,"Expenses is disabled");
		 }

		 
	 }
	 
	    @AfterTest
		public void shutDown() {
			driver.quit();
		}
	 
	
	
	

}
