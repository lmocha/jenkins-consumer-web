package com.consumerweb.Testcases;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.InterComPage;
import com.consumerweb.Pages.LoginPage;

public class InterComPageTest extends ConsumerWebBaseClass{
	InterComPage interComPage;
	LoginPage loginPage;
	String hiUserText,askUsAnythingText,startConversionText,findYourAnswerText;
	Boolean sendUsAmessageButtonIsDisplayed,searchOurArticlesIsDisplayed,weRunOnIntesrComIsDisplayed;
	WebElement searchOurArticlestextBox,divHolder;
	
	
	
	@BeforeClass
	    public void setup() throws Exception {
		    init();
			loginPage = new LoginPage(driver);
			interComPage = new InterComPage(driver);
	    }
	
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	    public void verifyInterComIsDisplayed() throws Exception {
	    	test = extent.createTest("Verify InterComPage is displayed correctly");
	    	interComPage.clickonInterCom();
	    	Thread.sleep(6000);
	    	driver.switchTo().frame("intercom-messenger-frame");
	    	askUsAnythingText = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("span.e1n5ubry4.intercom-lsrpuh"))).getText();
	    	Assert.assertEquals(askUsAnythingText, "Ask us anything, or share your feedback.");
	    	startConversionText = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.intercom-vmab6f.e1fyju1n2"))).getText();
	    	Assert.assertEquals(startConversionText, "Start a conversation");
	    	sendUsAmessageButtonIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("button.new-conversation-button.intercom-szufpy.esf9qb10"))).isDisplayed();
	    	Assert.assertTrue(sendUsAmessageButtonIsDisplayed);
	    	weRunOnIntesrComIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.linkText("We run on Intercom"))).isDisplayed();
	    	Assert.assertTrue(weRunOnIntesrComIsDisplayed);
	 }
	 @Test(priority = 2)
	 public void verifyUserCanSearchFromArticles() {
		 test = extent.createTest("Verify a user can start a conversation with someone from support");
		 if(isElementPresent(By.cssSelector("div.intercom-messenger-card-component"))) {
			 interComPage.startAconversation("accounts");
			 interComPage.clickOnSend();
			 divHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.intercom-18c1xc5.e6km4ip0")));
			 //divHolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("div.intercom-messenger-card-component"))).findElement(By.cssSelector("div.intercom-18c1xc5.e6km4ip0"));
	         List<WebElement> listOfResponces = divHolder.findElements(By.cssSelector("div.intercom-messenger-card-list-item.intercom-wlajoz e1b3yklj0"));
	         test.log(Status.INFO, "Our search has found :"+listOfResponces.size()+" responses ");
	         System.out.println(listOfResponces.size());
	            for (WebElement webElement : listOfResponces) {
		            String name = webElement.getText();
		            System.out.println("==========================================================================");
		            System.out.println(name);
		            test.log(Status.INFO, name);
		        }
		 }else {
			 test.log(Status.INFO, "user cannot search from articles");
		 }


	 }
	 
	 
	 
	@AfterTest
	public void shutDown() {
			driver.quit();
		}

}
