package com.consumerweb.Testcases;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.InterComPage;
import com.consumerweb.Pages.LoginPage;

public class PricingModelsPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	String withdarawalFees,balanceMaintenanceFees;
	
	
	
	@BeforeClass
	    public void setup() throws Exception {
		    init();
			loginPage = new LoginPage(driver);
	    }
	
	 @Test(priority = 0)
		public void verifyWithdrawalAndBalanceMaintenanceFeesAreDisplayed() throws InterruptedException {
		    test = extent.createTest("PRICING MODELS PAGE :Verify Withdrawal And Balance Maintenance Fees Are Displayed");
//		    String currentUrl = driver.getCurrentUrl();
		    //System.out.println(currentUrl);
		    driver .get("https://lum-consumer-test.lumapp.io/pricing");
		    withdarawalFees = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > table > tbody:nth-child(6) > tr:nth-child(10)"))).getText();
		    Assert.assertTrue(withdarawalFees.contains("withdrawal"));
		    test.log(Status.INFO, withdarawalFees);
		    balanceMaintenanceFees = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > table > tbody:nth-child(4) > tr:nth-child(4)"))).getText();
		    Assert.assertTrue(balanceMaintenanceFees.contains("Balance maintenance"));
		    test.log(Status.INFO, balanceMaintenanceFees);
			
		}
	 
		@AfterTest
		public void shutDown() {
				driver.quit();
			}

}
