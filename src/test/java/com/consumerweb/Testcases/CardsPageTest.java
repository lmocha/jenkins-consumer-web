package com.consumerweb.Testcases;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.CardsPage;
//import com.smeweb.pages.HomePage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;

public class CardsPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	CardsPage cardsPage;
	
	
	 @BeforeClass
	 public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 cardsPage = new CardsPage(driver);
	    }
	 
	 @Test(priority = 0)
	 public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("ACCOUNTS PAGE :Verify successful login");
		    login();
		}
	 
	 @Test(priority = 1)
	 public void verifyUserCanAccessCardsPage() throws Exception{
		 test = extent.createTest("TOQ-87:Verify user can access the accounts page");
		 cardsPage.clickOnCards();
		 }
//	
//		@Test(priority = 2)
//		public void verifyUserCanSeeCards() {
//		
//		test = extent.createTest("TOQ-186:Verify whether a user can see their own cards");
//		
//		cardsPage.assertMyCardslist();
//		
//		}
//		
//		@Test(priority = 3)
//		public void verifyfiltersIspresent() {
//		
//		test = extent.createTest("TOQ-444:Verify that filters are added to the card SME web");
//		//cardsPage.goTocardspage();
//		cardsPage.clickAllAcountsLink();
//		cardsPage.AssertallAccountsLable("Cards from all accounts");
//		}
//		
//		
//		
//		@Test(priority = 4)
//		public void verifyDirectorCanSeeCardsDetails() throws InterruptedException{
//		
//		test = extent.createTest("TOQ-188:Verify that a user DIRECTOR can see a detailed view when a card is clicked ");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		cardsPage.clickcardImage();
//		cardsPage.enterSecurityKey("1234");
//		cardsPage.confirmButton();
//		Thread.sleep(5000);
//		}
//	@Test(priority = 5)
//	 public void verifyUserCanIssueAVirtualCard() throws InterruptedException {
//		 test = extent.createTest("TOQ-190: Verify user can issue a virtual card");
//		 cardsPage.goTocardspage();
//		 cardsPage.clickOnIssueANewCardButton();
//		 cardsPage.clickNext("Next");
//		 cardsPage.enterCardName();
//		 cardsPage.clickNext("Next");
//		 cardsPage.clickNext("Next");
//		 cardsPage.confirmButton();
//	}
//	
//	
//	@Test(priority = 6)
//	
//	public void verifyUserCanIssuePhyiscalCard()  throws InterruptedException{
//		test = extent.createTest("TOQ-190 :Verify a Director can issue a Phyiscal card");
//		cardsPage.goTocardspage();
//		cardsPage.clickOnIssueANewCardButton();
//		cardsPage.clickNext("Next");
//		cardsPage.clickOnPhysicalComboBox();
//		cardsPage.enterDeliveryName("Lewis Test");
//		cardsPage.enterAddressLine("34443");
//		cardsPage.enterPostalCode("00100");
//		cardsPage.enterRegion("Nairobi");
//		cardsPage.enterCity("Nairobi");
//		cardsPage.enterCountry("United Kingdom");
//		cardsPage.clickNext("Next");
//		Thread.sleep(5000);
//		cardsPage.clickNext("Next");
//		cardsPage.confirmButton();
//	}
//	

//	@Test(priority = 7)
//	
//	public void verifyStatusIssued() throws InterruptedException{
//		test = extent.createTest("TOQ-462 :Verify card status changes to \"Issued\" after Issuing a new Physical card");
//		cardsPage.goTocardspage();
//		cardsPage.clickCheckBox("Issued");
//		cardsPage.FiltreAccounts("Krut");
//		//Thread.sleep(5000);
//		cardsPage.confirmButton();
//		cardsPage.selectparticulerCard("Krut");
//		//Thread.sleep(5000);
//		cardsPage.AssertalCardStatus("Issued");
//		
//	}
//	
//	@Test(priority = 8)
//	public void verifyAllInfoOnCardList() throws InterruptedException {
//		test = extent.createTest("TOQ-494: Verify whether all the information related to card is available in the card list");
//		cardsPage.goTocardspage();
//		cardsPage.clickCheckBox("Issued");
//		cardsPage.FiltreAccounts("Krut");
//		cardsPage.confirmButton();
//		cardsPage.checkcardInfo("Physical card", "Issued", "Krut");
//		Thread.sleep(5000);
//		
//	}
//	
//	@Test(priority = 9)
//	public void verifyStatusOnCardDetais() throws InterruptedException {
//		test = extent.createTest("TOQ-589: Verify that the card status in sme web is shown in any card details");
//		cardsPage.goTocardspage();
//		cardsPage.clickCheckBox("Issued");
//		cardsPage.FiltreAccounts("Krut");
//		cardsPage.confirmButton();
//		cardsPage.selectparticulerCard("Krut");
//		cardsPage.AssertalCardStatus("Issued");
//		cardsPage.assertCardInfoDisplayed();
//		Thread.sleep(5000);
//		
//	}
//	
//	
//	@Test(priority = 10)
//	public void verifyMoreInfoInCardDetails() throws InterruptedException {
//		test = extent.createTest("TOQ-634:  Verify whether more information is provided in card details");
//		cardsPage.goTocardspage();
//		cardsPage.clickCheckBox("Issued");
//		cardsPage.FiltreAccounts("Krut");
//		cardsPage.confirmButton();
//		cardsPage.selectparticulerCard("Krut");
//		cardsPage.AssertalCardStatus("Issued");
//		
//		Thread.sleep(5000);
//		
//	}
//	
//	@Test(priority = 11)
//	public void verifyUserCanveiwcardDetails() throws InterruptedException{
//		
//		test = extent.createTest("TOQ-192:Verify a user can get and produce card image with overprinted details");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		cardsPage.clickcardImage();
//		cardsPage.enterSecurityKey("1234");
//		cardsPage.confirmButton();
//		}
//	
//	@Test(priority = 12)
//	public void verifypinIsNotdisplayedOnVirtualCads() throws InterruptedException{
//		
//		test = extent.createTest("TOQ-194:Verify PIN is not being displayed on virtual cards ");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		cardsPage.clickcardImage();
//
//		}
//	
//	@Test(priority = 13)
//	public void verifyWhenAcardisCancelled() throws InterruptedException{
//		
//		test = extent.createTest("TOQ-389:Verify when a card is cancelled it should show cancelled ");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		cardsPage.selectCancle();
//		cardsPage.enterSecurityKey("1234");
//
//		}
//	
//	@Test(priority = 14)
//
//	public void verifySelectAccount()  throws InterruptedException{
//		test = extent.createTest("TOQ-407 :Verify we have an option to select an account in the card creation process");
//		cardsPage.goTocardspage();
//		cardsPage.clickOnIssueANewCardButton();
//		cardsPage.selectModify();
//		
//	}
//	
//	@Test(priority = 15)
//	public void verifyCorrectErroMsg() throws InterruptedException{
//		
//	test = extent.createTest("TOQ-432:Verify the system shows a correct and translated error message when less than 4 numbers are provided as security code");
//	cardsPage.goTocardspage();
//	cardsPage.selectCard();
//	cardsPage.clickcardImage();
//	cardsPage.enterSecurityKey("123");
//	cardsPage.clickOnDialogBox();
//	cardsPage.validateErroMsg("Code must have 4 numbers");
//	Thread.sleep(5000);
//	}
//	
//	@Test(priority = 16)
//	public void verifySelectCardHolder() {
//		test = extent.createTest("TOQ-651: Verify that user can select a card holder when creating a card for SME web");
//		cardsPage.goTocardspage();
//		cardsPage.clickOnIssueANewCardButton();
//		cardsPage.selectModifycardHolder();
//		cardsPage.assertCardHolderPoppage();
//
//		
//	}
//	@Test(priority = 17)
//	public void verifyCardDetailsOpenAfterNewCardCreation() throws InterruptedException {
//		test = extent.createTest("TOQ-680:Verify you're taken to card details when a card is created in SME web");
//		verifyUserCanIssuePhyiscalCard();
//		cardsPage.assertCardInfoDisplayed();
//		
//	}
//	
//	@Test(priority = 18)
//	public void verifyNewCardsCreatedUnderNewAccount() throws InterruptedException {
//		
//		test = extent.createTest("TOQ-697: Verify whether virtual and physical cards can be created using new accounts");
//		verifyUserCanIssueAVirtualCard();
//		shutdown();
//		verifyUserCanIssuePhyiscalCard();
//		cardsPage.assertCardInfoDisplayed();
//	}
//	
//	@Test(priority = 19)
//	public void verifyCardPinCanBeViewed() throws InterruptedException {
//		
//		test = extent.createTest("TOQ-788: Verify that the card pin is available only for 10 seconds");
//		cardsPage.goTocardspage();
//		cardsPage.clickCheckBox("Active");
//		cardsPage.FiltreAccounts("Twist");
//		cardsPage.clickCheckBox("Physical");
//		cardsPage.confirmButton();
//		cardsPage.selectparticulerCard("Twist");
//		cardsPage.clickShowPin();
//		cardsPage.enterSecurityKey("1234");
//		cardsPage.confirmButton();
//		
//		
//	}
//	
//	@Test(priority = 20)
//	public void verifyAccountAddedtoCardDetails()  throws InterruptedException{
//		test = extent.createTest("TOQ-431 : Verify that account information to card detail SME web was added");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		
//	}
//	
//	
//// to be complete when doing accounts
//	@Test(priority = 21)
//	public void verifyremovedCardsAreNotVisible()  throws InterruptedException{
//		test = extent.createTest("TOQ-742 : Verify cards from “Removed” accounts are not being shown");
//		cardsPage.goTocardspage();
//		cardsPage.clickAllAcountsLink();
//		
//	}
//	
//	public void verifyActivateCardBTNPresent()  throws InterruptedException{
//		test = extent.createTest("TOQ-712 : Verify whether activate link is replaced with a new activate card CTA");
//		cardsPage.goTocardspage();
//		verifyUserCanIssuePhyiscalCard();
//		//to add check after issuing new card.
//		
//	}
//	
//	
//
//	@Test(priority = 22)
//	public void verifyCardIndicatingBelongstoAccount()  throws InterruptedException{
//		test = extent.createTest("TOQ-693 : Verify there is a CTA in the card list indicating the account that a card belongs to");
//		cardsPage.goTocardspage();
//		cardsPage.clickAllAcountsLink();
//		cardsPage.asserAccountLabelPresent();
//		
//		
//		
//	}
//	
//	@Test(priority = 23)
//	public void verifyUserCannotActivateCardforOtherUsers()  throws InterruptedException{
//		test = extent.createTest("TOQ-751 :Verify the currently logged-in user cannot activate a card that doesn't belong to him/her");
//		cardsPage.goTocardspage();
//		cardsPage.selectCard();
//		
//		
//	}

//	
	
//Cardlimits tests
	
	
	@Test(priority = 3)
	public void verifyUserAddCardLimits() throws InterruptedException {
		 test = extent.createTest("TOQ-902:Verify there is an option to add a card limit when creating a card for SME web");
		// cardsPage.goTocardspage();
		 cardsPage.clickOnIssueANewCardButton();
		 cardsPage.clickNext("Next");
		 cardsPage.enterCardName();
		 cardsPage.clickNext("Next");
		 cardsPage.assertCardLimitsPage("Card limits");
		 Thread.sleep(5000);
		 cardsPage.clickNext("Next");
		 Thread.sleep(5000);
		 cardsPage.confirmButton();
	}
	@Test(priority = 4)
	 public void verifyAddCardLimits() throws InterruptedException {
		 test = extent.createTest("TOQ-902:Verify there is an option to add a card limit when creating a card for SME web");
		 cardsPage.clickOnCards();
		 cardsPage.clickOnIssueANewCardButton();
		 cardsPage.clickNext("Next");
		 cardsPage.enterCardName();
		 cardsPage.clickNext("Next");
		 cardsPage.assertCardLimitsPage("Card limits");
		 cardsPage.enterCardLimits("1000","1500","10000");
		 Thread.sleep(5000);
		 cardsPage.clickNext("Next");
		 Thread.sleep(5000);
		 //cardsPage.confirmButton();
	}
	@Test(priority = 5)
	 public void verifyCanSeeCardLimitsUnderDetails() throws InterruptedException {
		 test = extent.createTest("TOQ-928:Verify whether card limits are displayed in the card details (more details) page and card details are displayed in the more details page");
		 cardsPage.clickOnCards();
		 cardsPage.selectCard();
		 Thread.sleep(1000);
		 cardsPage.clickMoreDetails();
		 Thread.sleep(1000);
		 cardsPage.assertCardLabels("Card limits");
		 cardsPage.refreshBrowser();
	}
	
	@Test (priority = 6)
	 public void verifyCardAliasIsDisplaued() throws InterruptedException {
		 test = extent.createTest("TOQ-945:Verify whether card alias is displayed in \"my cards\" list for director and account owner");
		 cardsPage.clickOnCards();
		 cardsPage.selectCard();
		 Thread.sleep(5000);
		 cardsPage.clickMoreDetails();
		 Thread.sleep(5000);
		 cardsPage.assertCardLabels("Card limits");
		 cardsPage.refreshBrowser();
		
	}
	@Test(priority = 7)
	 public void verifyEditCardLimits() throws InterruptedException {
		 test = extent.createTest("TOQ-954:Verify user can Edit card limit for SME web");
		 cardsPage.clickOnCards();
		 cardsPage.selectCard();
//		 cardsPage.clickMoreBTN();
		 cardsPage.clickModifyLimits();
		 cardsPage.enterCardLimits("1000","1500","10000");
		 Thread.sleep(5000);
		 cardsPage.clickNext("Modify");
		 cardsPage.enterSecurityKey("1234");
		 cardsPage.confirmButton();
		 Thread.sleep(1000);
	}
	@Test(priority = 8)
	 public void verifyCardDetailsAreShwon() throws InterruptedException {
		 test = extent.createTest("TOQ-963:Verify card details are shown when clicking in \"more details\"");
		 cardsPage.clickOnCards();
		 cardsPage.selectCard();
		 Thread.sleep(5000);
		 cardsPage.clickMoreDetails();
		 cardsPage.assertCardLabels("Card details");
		 Thread.sleep(1000);
		 cardsPage.refreshBrowser();
		
	}
	@Test(priority = 9)
	 public void verifyLimitNotHigherThanBankLimit() throws InterruptedException {
		 test = extent.createTest("TOQ-995:Verify a User cannot introduce a limit higher than the banking provider allows");
		 cardsPage.clickOnCards();
		 cardsPage.clickOnIssueANewCardButton();
		 cardsPage.clickNext("Next");
		 cardsPage.enterCardName();
		 cardsPage.clickNext("Next");
		 cardsPage.assertCardLimitsPage("Card limits");
		 cardsPage.enterCardLimits("200000","200000","1000000");
		 cardsPage.assertLimitErrors("The amount exceeds the limit of €115,000.00");
		 Thread.sleep(5000);
		 cardsPage.clickNext("Next");
		 Thread.sleep(1000);
		 //cardsPage.confirmButton();
	}
	
	@AfterTest
	public void shutdown()
	{ 	
		driver.quit();
	}

}
