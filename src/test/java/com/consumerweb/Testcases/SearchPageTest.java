package com.consumerweb.Testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.SearchPage;

public class SearchPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	AccountsPage accountsPage;
	SearchPage searchPage;
	String noOfTransactions;
	Boolean notifica;
	WebElement contentHolder,resetButton;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 searchPage = new SearchPage(driver);
		 accountsPage = new AccountsPage(driver);   
	    }
	 
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifySearchPageIsDisplayedCorrectly() throws InterruptedException {
		 test = extent.createTest("Verify search page is displayed correctly");
		 searchPage.clickOnSearch();
	 }
	 
	 @Test(priority = 2)
	 public void verifyUserCanSearchByMerchantName() throws Exception {
		 test = extent.createTest("Verify user can search by Merchant name");
		 searchPage.searchByMerchant("A");
		 displayTransactions();

	 }
	 @Test(priority = 3)
	 public void verifyUserCanSearchByAmountRange() throws Exception {
		 test = extent.createTest("Verify user can search by Amount range");
		 searchPage.enterAmountRangeFrom("0");
		 searchPage.enterAmountRangeTo("4500000");
		 displayTransactions();
		 
	 }
	 @Test(priority = 4)
	 public void verifyUserCanSearchByDate() throws Exception {
		 test = extent.createTest("Verify user can search by date range");
		 searchPage.clickOnSearch();
		 searchPage.enterDate1("01/01/2020");
		 searchPage.enterDate2("20/12/2020");
		 displayTransactions();
	 }
	 @Test(priority = 5)
	 public void verifyUserCanSearchByCategory() throws Exception {
		 test = extent.createTest("Verify user can search by Category");
		 searchPage.clickOnSearch();
		 searchPage.clickOnCategory();
		 if(isElementPresent(By.cssSelector("li.item-list-separator"))) {
			 searchPage.clickOnFirstCategory();
			 searchPage.clickOnConfirm();
			 displayTransactions();
		 }else {
			 test.log(Status.INFO,"There are no categories");
		 }

	 }
	 @Test(priority = 6)
	 public void verifyUserCanSearchByTransactionOwner() throws Exception {
		 test = extent.createTest("Verify user can search by Transaction owner");
		 searchPage.clickOnSearch();
		 searchPage.clickOnTransactionOwner();
		 if(isElementPresent(By.cssSelector("ul.reset-list-style.lum-fs-16"))) {
			 searchPage.clickOnTheFirstTransactionOwner();
			 searchPage.clickOnConfirm();
			 displayTransactions();
		 }else {
			 test.log(Status.INFO,"There are no transaction owners");
		 }

	 }
	 @Test(priority = 7)
	 public void verifyUserCanSearchByMovementType() throws Exception {
		 test = extent.createTest("Verify user can search by movement type");
		 searchPage.clickOnSearch();
		 searchPage.clickOnMovementType();
		 if(isElementPresent(By.cssSelector("div.flex"))) {
			 searchPage.clickOutboundMovements();
			 searchPage.clickOnConfirm();
			 displayTransactions(); 
		 }else {
			 test.log(Status.INFO,"There are no movement types");
		 }

	 }
	@Test(priority = 8)
	 public void verifyUserCanSearchByCurrency() throws Exception {
		 test = extent.createTest("Verify user can search by Currency");
		 searchPage.clickOnSearch();
		 searchPage.clickOnCurrency();
		 if(isElementPresent(By.cssSelector("ul.reset-list-style.lum-section-list"))){
			 searchPage.selectEuros();
			 searchPage.clickOnConfirm();
			 displayTransactions(); 
		 }else {
			 
			 test.log(Status.INFO,"User cannot search by Currency");
		 }

	 }
	 @Test(priority = 9)
	 public void userCanSearchByFiles() throws Exception {
		 test = extent.createTest("Verify user can search by Files");
		 searchPage.clickOnSearch();
		 searchPage.clickOnFiles(); 
		 if(isElementPresent(By.cssSelector("div.display-flex.column.modal-content.no-title"))) {
			 searchPage.selectWithoutFiles();
			 searchPage.clickOnConfirm();
			 displayTransactions();
		 }else 
		 {
			 test.log(Status.INFO,"User cannot search by Files");
		 }

	 }
	@Test(priority = 10)
	 public void verifyUserCanSearchByPaymentStatus() throws Exception {
		 test = extent.createTest("Verify user can search by Payment status");
		 searchPage.clickOnSearch();
		 searchPage.clickOnPaymentStatus();
	 if(isElementPresent(By.cssSelector("div.display-flex.column.modal-content.no-title"))) {
			 searchPage.clickOnPendingPaymentStatus();
			 searchPage.clickOnConfirm();
			 displayTransactions();
		 }
		 else {
			 test.log(Status.INFO,"There are no payment statuses");
		 }

	 }
	 public void clickOnResetButton() throws InterruptedException {
		 resetButton = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Reset')]")));
		 resetButton.click();
		 Thread.sleep(3000);
	 }
	 
	 public void displayTransactions() throws Exception{
		 String ULlist = "//*[@class='reset-list-style lum-section-list']/li/div/ul/li";
		 Thread.sleep(1000);
		 if(isElementPresent(By.cssSelector("div.lum-placeholder.display-flex.column.center-x.center-y.flex"))) {
			 test.log(Status.INFO,"The search found no transactions");
			 clickOnResetButton();
		 }else {
			 listULItems(By.xpath(ULlist));

		        }
	            clickOnResetButton();
		 }
		 


	 
	 
	 //@AfterTest
	 public void shutDown() {
			driver.quit();
		}

}
