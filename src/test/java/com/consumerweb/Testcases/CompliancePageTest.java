package com.consumerweb.Testcases;


import org.testng.Assert;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.AdminPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.CredentialsSetUp;
import com.consumerweb.Pages.KycPage;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.TruNarrativePage;

public class CompliancePageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	AdminPage adminPage;
    ConsumerWebBaseClass consumerWerBaseClass;
    CredentialsSetUp credentialSetup;
    TruNarrativePage tn;
    KycPage kycPage;
    Boolean currencyTextBox;
    String selectYourCurrencyTitle,userName,userEmail;
    
    @BeforeClass
	public void setup() throws Exception{
    	init();
    	loginPage = new LoginPage(driver);
    	adminPage = new AdminPage(driver);
    	tn = new TruNarrativePage(driver);
    	kycPage = new KycPage(driver);
    	credentialSetup	= new CredentialsSetUp(driver);
	  }
	    
	//@Test(priority = 0)	
	public void loginToGmailAndCreateLUMAcc() throws Exception{
		test = extent.createTest("COMPLIANCE PAGE :Verify successful login");
		loginToGmailLetsCreateYourLUMAccount();
	    
	}

	
	
	//@Test(priority = 1)	
	public void loginToTNAndAccept() throws Exception{
		Thread.sleep(120000);
		driver.get("https://pp.trunarrative.cloud/TruNarrative_Theme/UserLogin.aspx");
		tn.enterUsername("selpha+test@toq.io");
		tn.clickOnNextButton();
		tn.enterPassword("S@temba321");
		tn.clickOnLoginButton();
		for(int x=0; x<4; x++) {
		tn.clickOnReferrals();
		tn.searchByRefName("Selpharr Atemba");
		List<WebElement> listOfReferrals = driver.findElements(By.cssSelector("#TruNarrative_Theme_wt2_block_wtMainContent_wtTable > tbody > tr"));
		if(listOfReferrals.size()>0) {
		tn.clickOnAssignButton();
		tn.selectAteam("Referral team");
		tn.clickOnAssignToMe();
		tn.clickOnStart();
		tn.clickOnAccept();
		tn.clickOnMarkAsAccept();
		}
		}
			
	}
	//@Test(priority = 2)	
	public void verifyAccountCreated() {
		System.out.println("Started verifying an account was created");
		test = extent.createTest("Verify account was created");
		driver.get("https://lum-consumer-test.lumapp.io/login");
		loginPage.enterUserName("test-selpha");
		loginPage.enterPassword("Olivo2019");
		loginPage.clickOnLogin();
		selectYourCurrencyTitle = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(2) > div.flex > div"))).getText();
		Assert.assertEquals(selectYourCurrencyTitle, "Select your currency");
		currencyTextBox = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(2) > div.flex > form > div > div > select"))).isDisplayed();
		Assert.assertTrue(currencyTextBox);
		
	}
	//@Test(priority = 3)
	public void loginToAdminAndDeleteCompany() throws Exception{
		driver.get("https://admin-portal-test.lumapp.io/login");
		adminPage.enterUsername("admin");
		adminPage.enterPassword("Olivo2019");
		adminPage.clickLoginButton();
		adminPage.clickOnClients();
		adminPage.searchByClientName("Selpharr");
		adminPage.clickOnSearchButton();
		adminPage.clickOnClient();
		Thread.sleep(2000);
		userName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div.lum-truncate.cust-second-col.m-l-1.m-r-2 > div:nth-child(1) > div.info.lum-truncate"))).getText();
		Assert.assertEquals(userName, "Selpharr W Atemba");
		userEmail = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > div:nth-child(2) > div.lum-truncate.cust-third-col.m-l-1.m-r-2 > div > div.info.lum-truncate"))).getText();
		Assert.assertEquals(userEmail, "selpharr@gmail.com");
		adminPage.clickOnReject();
		Thread.sleep(2000);
		adminPage.enterRejectReason("Testing");
		adminPage.clickOnAccept();
		adminPage.clickOnDelete();
		Thread.sleep(5000);
		adminPage.acceptDeleteClient();
	}

}
