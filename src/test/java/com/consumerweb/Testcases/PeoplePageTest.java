package com.consumerweb.Testcases;


import java.util.concurrent.TimeUnit;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PeoplePage;

public class PeoplePageTest extends ConsumerWebBaseClass  {
	LoginPage loginPage;
	PeoplePage peoplePage;
	LoginPageTest loginPageTest;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 peoplePage = new PeoplePage(driver);
	    }
	 
	 
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 
	 @Test(priority = 1)
	 public void verifyPeoplePageIsDisplayed() throws Exception{
		 test = extent.createTest("Verify People page is displayed correctly");
		 peoplePage.clickOnPeople();
		 peoplePage.showThePeopleList();
		 
	 }
	 @Test(priority = 2)
	 public void verifyInvitedTabIsDisplayed() {
		 test = extent.createTest("Verify Invited Tab is displayed correctly");
		 peoplePage.clickOnInvitedTab();
		 peoplePage.showInvitedPeople();

	 }
	 @Test(priority = 3)
	 public void verifyUserCanAddAnAccountOwner() {
		 test = extent.createTest("TOQ-820:Verify Account owner can add another Account owner/Cardholder");
		 peoplePage.clickOnAddAnewUserButton();
		 peoplePage.selectAccountOwnerRole();
		 peoplePage.assertAccountOwnerDescIsDisplayed();
		 peoplePage.enterName();
		 peoplePage.enterSurname();
		 peoplePage.enterEmail();
		 peoplePage.enterDateOfBirth();
		 peoplePage.enterPhonePrefix();
		 peoplePage.enterPhoneNumber();
		 peoplePage.clickOnSaveButton();
		 peoplePage.clickOnCancelButton();
		 //peoplePage.clickOnConfirmButton();
		 
	 }
	 
	 @Test(priority = 4)
	 public void verifyUserCanAddACardHolder() {
		 driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		 test = extent.createTest("Verify User can add a card Holder");
		 peoplePage.clickOnPeople();
		 peoplePage.clickOnAddAnewUserButton();
		 peoplePage.selectCardHolderRole();
		 peoplePage.assertCardHolderDescIsDisplayed();
		 peoplePage.enterName();
		 peoplePage.enterSurname();
		 peoplePage.enterEmail();
		 peoplePage.enterDateOfBirth();
		 peoplePage.enterPhonePrefix();
		 peoplePage.enterPhoneNumber();
		 peoplePage.clickOnSaveButton();
		 peoplePage.clickOnCancelButton();
		 //peoplePage.clickOnConfirmButton();
	 }
	 
	 @AfterTest
	 public void shutDown() {
			driver.quit();
		}
	 

	 


}
