package com.consumerweb.Testcases;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PaymentsPage;
import com.consumerweb.Pages.SyncomPage;

public class SyncomPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	SyncomPage syncomPage;
	PaymentsPage paymentsPage;
	AccountsPage accountsPage;
	
	
	 @BeforeClass
	 public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 syncomPage = new SyncomPage(driver);
		 paymentsPage = new PaymentsPage(driver);
		 accountsPage = new AccountsPage(driver);
	    }
	 
	 @Test(priority = 0)
	 public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("ACCOUNTS PAGE :Verify successful login");
		    login();
		}
	
	 
	 //@Test(priority = 1)
	 public void verifyWeCanCreateAllAccountTypesInClearBank() throws Exception{
		 test = extent.createTest("TOQ-376:AS a Account owner I want to create a new account from Accounts");
		 syncomPage.createAndDeleteAllAccountTypesinCB();
 
	 }
	 //@Test(priority = 2)
	 public void verifyCBUserCanMakePaymentBetweenOwnAccounts() throws Exception{
		 test = extent.createTest("verify clear bank User can make payment between own accounts");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 syncomPage.selectPayFrom();
		 syncomPage.selectTransferBetweenMyOwnAccounts();
		 syncomPage.selectPayToForOwnAccounts();
		 syncomPage.enterPaymentDetails();
 
	 }
	 //@Test(priority = 3)
	 public void verifyCBUserCanMakePaymentToAbeneficiary() throws Exception{
		 test = extent.createTest("verify clear bank user can make payment to a beneficiary");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.clickOnModify();
		 syncomPage.selectPayFrom("£");
		 paymentsPage.clickOnChooseAnExistingBeneficiary();
		 syncomPage.selectArandomBeneficiaryBycurrency("GBP");
		 paymentsPage.enterYouSendAmount();
		 syncomPage.selectUKfasterPayments();
		 paymentsPage.enterConcept();
		 accountsPage.clickOnImmediatePayment();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterNotifyByEmail();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterSecurityCode();
		 syncomPage.assertPaymentSucceeded();
 
	 }
	 //@Test(priority = 4)
	 public void verifyCBUserCanMakeFXPaymentBetweenOwnAccounts() throws Exception{
		 test = extent.createTest("verify clear bank user can make FX payment between own accounts");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.clickOnModify();
		 syncomPage.selectPayFrom("£");
		 syncomPage.selectTransferBetweenMyOwnAccounts();
		 syncomPage.selectPayFrom("€");
		 syncomPage.enterYouSendAmount("200");
		 paymentsPage.enterReference();
		 paymentsPage.enterConcept();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterNotifyByEmail();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterSecurityCode();
         
	 }
	 @Test(priority = 5)
	 public void verifyUserCanChangeDefaultCurrencyToAllAvailableCurrencies() throws InterruptedException {
		 test = extent.createTest("verify that a user can change default currency to all available currencies");
		 syncomPage.clickOnAccountSettings();
		 syncomPage.clickOnDefaultCurrency();
		 syncomPage.selectCurrency();

		
	 }




}
