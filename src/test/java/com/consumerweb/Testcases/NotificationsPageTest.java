package com.consumerweb.Testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.NotificationsPage;

public class NotificationsPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	AccountsPage accountsPage;
	NotificationsPage notificationsPage;
	String payments,notificationsTitle,notificationsPageContent;
	Boolean notifica;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 notificationsPage = new NotificationsPage(driver);
		 accountsPage = new AccountsPage(driver);
	    }
	 
	 
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifyNotificationsPageIsDisplayedCorrectly() throws InterruptedException {
		 test = extent.createTest("Verify notifications page is displayed correctly");
		 notificationsPage.clickOnNotifications();
		 notificationsTitle = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.lum-title.m-b-2"))).getText();
		 Assert.assertEquals(notificationsTitle, "Notifications");
		 notificationsPageContent = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div:nth-child(2)"))).getText();
		 test.log(Status.INFO, "The message on the notification page is :"+notificationsPageContent);
	 }
	 
	 @AfterTest
		public void shutDown() {
			driver.quit();
		}

}
