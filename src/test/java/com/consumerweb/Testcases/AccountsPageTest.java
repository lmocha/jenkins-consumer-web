package com.consumerweb.Testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PaymentsPage;
import com.consumerweb.Pages.SyncomPage;
import com.consumerweb.Pages.UserRegistrationPage;

public class AccountsPageTest extends ConsumerWebBaseClass{
	SoftAssert soft; 
	UserRegistrationPage userRegistrationPage;
	LoginPage loginPage;
	AccountsPage accountsPage;
	PaymentsPage paymentsPage;
	SyncomPage syncomPage;

	
	 @BeforeClass
	 public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 accountsPage = new AccountsPage(driver);
		 paymentsPage =new PaymentsPage(driver);
		 syncomPage = new SyncomPage(driver);
	    }
	 
	 @Test(priority = 0)
	 public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("ACCOUNTS PAGE :Verify successful login");
		    login();
		}
	 
	 @Test(priority = 1)
	 public void verifyUserCanAccessToTheAccountsPage() throws Exception{
		 test = extent.createTest("TOQ-87:Verify user can access the accounts page");
		 accountsPage.checkAccountsPageisDisplayed();
		 }
	  @Test(priority = 2)
	 public void verifyCancelledAccountsAreShownInAccountsSubDashboard() throws Exception{
		 test = extent.createTest("TOQ-537:Verify that cancelled accounts are shown in account subdashboard SME");
		 accountsPage.assertCancelledAccountsAreDisplayed();
		 }
	 @Test(priority = 3)
	 public void verifyUserCanseeAccountBalanceinTheAccountsPage() throws Exception{
		 test = extent.createTest("TOQ-88:Verify that user can see account balance in the accounts page");
		 accountsPage.showTotalAccountBalance();
	 }
	 
	 @Test(priority = 4)
	 public void verifyWeHaveAddAnAccountButton() {
		 test = extent.createTest("TOQ-525:Verify we have Add an account button SME");
		 accountsPage.checkThatAddAnaccountButtonIsPresent();
		 
	 }
	
	 public void verifyUserCanSeeTotalBalanceInDifferentCurrencies() throws InterruptedException {
		 test = extent.createTest("TOQ-394:verify User Can See Total Balance In Different Currencies"); 
		 accountsPage.clickOnTotalBalanceCurrencySelector();
		 accountsPage.clickOnTheFirstCurrency();
	 }
	 @Test(priority = 6)
	 public void verifyDirectorWithOneAccountCanCreateAnAccount() {
		 test = extent.createTest("TOQ-351: Verify as an account owner with ONE account I can be able to Create an Account from Accounts by clicking More and choosing Add an account");
		 accountsPage.checkThatDirectorWithOneAccountCanAddAccount();
		 
	 }
	 
	 @Test(priority = 7)
	 public void verifyUserCanAddAccountWhenAccountsPageIsInListDisplay() throws InterruptedException{
		 test = extent.createTest("TOQ-435:Verify whether you can add account, when accounts page is in list display format");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnListIcon();
		 accountsPage.clickOnAddAnAccountButton();
		 accountsPage.checkAccountCreationPageIsDisplayed();
		 
	 }
	 
		 public void verifyWhenIClickOnaPaymentFromAnAccPayFromWillBeTheAccount() throws InterruptedException {
		 test = extent.createTest("TOQ-404: FE:In [Account] I select my main account and click on new payment, then in the payment section the one selected is my test account");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnFirstAccount();
		 accountsPage.compareAccountNameAndPayFromName();
	 }
	 
	 @Test(priority = 9)
	 public void verifyFilterMovementsIsAddedToGlobalMovements() throws InterruptedException {
		 test = extent.createTest("TOQ-491:Verify that filter was added to global movements Consumer");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnTransactionsSubDashBoard();
		 accountsPage.checkThatFilterIsDisplayedForTransactions();
	 }
	 
	 @Test(priority = 10)
	 public void verifyUserCanAccessFilter() throws InterruptedException {
		 test = extent.createTest("TOQ-175:Verify user can access Filter");
		 accountsPage.checkSearchByMerchantNotesAddressOrConceptWorks();
		 accountsPage.checkSearchByAmountWorks();
		 
		 
	 }
	 @Test(priority = 11)
	 public void verifyUserCanClearFilters() throws InterruptedException {
		 test = extent.createTest("TOQ-176:Verify user can Clear filters");
		 accountsPage.checkSearchByDateWorks(); 
	 }
	 @Test(priority = 12)
	 public void verifyAccountAaccountInfoWasAddedToTile() throws InterruptedException {
		 test = extent.createTest("TOQ-360:FE: Verify that Info Icon to the a/c tile Consumer web is added");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkThe3DotsAreAvailable();
		 accountsPage.clickOn3DotsOfExistingAccounts();
		 accountsPage.checkTheAccountDetailsPopupIsDisplayed();
	 }
	 
	 
	 @Test(priority = 13)
	 public void verifyAccountDetailsOptionOnAnyAccountTakesUserToTheAccountDetailsPage() throws InterruptedException {
		 test = extent.createTest("TOQ-472:Verify account details info is showing when selecting three dot menu option 'Account details´ option of any account");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOn3DotsOfExistingAccounts();
		 accountsPage.clickOnAccountDetailsPopUp();
		 accountsPage.checkIfAccountDetailsAreWellDisplayed();
	 }
	 @Test(priority = 14)
	 public void verifyThatCopyAccountDetailsFuncionalityWasAddedToSMEWeb() throws InterruptedException {
		 test = extent.createTest("TOQ-762:Verify that copy account details funcionality was added SME web");
		 accountsPage.checkIfCopyAccountDetailsIsAvailable();
	 }
	
	 
	 @Test(priority = 15)
	 public void verifyaddAnAccountOptionNavigatesToNewAccountPage() throws Exception{
		 test = extent.createTest("TOQ-490:Verify add account option navigates to new account page");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnAddAccountButtonForExistingAccounts();
		 accountsPage.checkAccountCreationPageIsDisplayed();
		 }
	
	 public void verifyEUR3PartnerProductsAreShownSeparately() throws InterruptedException {
		 test = extent.createTest("TOQ-831:Verify EUR3 partner products are showed separately");
		 accountsPage.clickOnAddAnAccountButton();
		 accountsPage.checkEUR3PartnerProducts();
		 
	 }
	  @Test(priority = 16)
	 public void verifyThereISLimitToAccountAliasTo25Characters() throws Exception{
		 test = extent.createTest("TOQ-863:Verify there is limit to account alias to 25 characters");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnAddAnAccountButton();
		 accountsPage.enterAccountAliasThatIsMoreThan25Characters();
		 accountsPage.selectGBPAccountInUK();
		 accountsPage.clickOnSaveAccountButton();
		 accountsPage.assertAliasErrorIsDisplayed();
		 
	 }
	 @Test(priority = 17)
	 public void verifyWeHaveCreateAccountFormInConsumer() throws Exception{
		 test = extent.createTest("TOQ-376:AS a Account owner I want to create a new account from Accounts");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnAddAnAccountButton();
		 accountsPage.enterAccountAlias("TEST ACCOUNT");
		 accountsPage.selectGBPAccountInUK();
		 accountsPage.clickOnSaveAccountButton();
		  
		 
	 }
	 @Test(priority = 18)
	 public void verifySMERedirectsUserToTheCreatedAccountMovementsPage() {
		 test = extent.createTest("TOQ-490:Verify CONSUMER WEB Redirect USER to the created account movements page"); 
		 accountsPage.checkNewAccountMovementsPageIsDisplayed();
	 }
	 @Test(priority = 19)
	 public void VerifyNewlyCreatedAccountShowsTheCorrectAccountDetails() {
		  test = extent.createTest("TOQ-832:Verify the newly created Account is showing the correct account details");
		  accountsPage.clickOnAccountDetails();
		  accountsPage.checkIfAccountDetailsAreWellDisplayed();
		  
	  }
	@Test(priority = 20)
	 public void verifyCopyUpdates() throws InterruptedException {
			 test = extent.createTest("TOQ-566:Verify Copy updates are up to date");
			 accountsPage.clickOnBackButton();
			 accountsPage.clickOnDots();
			 accountsPage.clickOnCancelAccount();
			 accountsPage.checkDeleteAccountCopiesAreUpdated();
			 
		 }
	  @Test(priority = 21)
	 public void verifyCountryCodeWasRemoved() throws InterruptedException {
			 test = extent.createTest("TOQ-864:Verify country code under account type section has been removed from the Cancel account pop-up.");
			 accountsPage.checkCountryCodeIsRemovedFromAccountType();
			 
			 
		 }
	 @Test(priority = 22)
	 public void verifyUserCanDeleteAnAccount() throws InterruptedException {
		 test = extent.createTest("TOQ-528:Verify user can delete an account");
		 accountsPage.clickOnCancelAndRemoveFromView();
		 accountsPage.clickOnConfirmCancellationButton();
		 
	 }
	 @Test(priority = 23)
	 public void verifyUserCanCopyAccountDetails() throws InterruptedException {
		 test = extent.createTest("TOQ-167:Verify user can copy Account details");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkUserCanCopyAccountDetails();
		 	 
	 }	
	 @Test(priority = 24)
	 public void verifyBankStatementCopyIsReplaced() throws InterruptedException {
		 test = extent.createTest("TOQ-727:Verify the label Bank Statement is replaced with Download Bank Statement ");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkThatBankStatementIsReplaced();
		 	 
	 }
	 @Test(priority = 25)
	 public void verifyUserCanDownloadBankStatement() throws InterruptedException {
		 test = extent.createTest("TOQ-171:Verify user can download Bank statement");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkUserCanDownloadBankStatement();
		 accountsPage.enterDownloadBankStatementFromDAte();
		 accountsPage.enterDownloadBankStatementToDAte();
		 accountsPage.clickOnDownLoadButton();
	 }
	 @Test(priority = 26)
	 public void verifyUserCanSeeAccountDetails() throws InterruptedException {
		 test = extent.createTest("TOQ-89:Verify user can see Account details");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnAccountDetailsButton();
		 accountsPage.checkIfAccountDetailsAreWellDisplayed();
	 }
	 @Test(priority = 27)
	 public void verifyUserCanMakeAPayment() throws InterruptedException {
		 test = extent.createTest("TOQ-168:Verify user can make a New payment");
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.clickOnFirstAccount();
		 accountsPage.clickOnPaymentButton();
		 paymentsPage.modifyPayFrom("£");	
		 syncomPage.chooseAnExistingBeneficiary();
		 paymentsPage.selectGivenCurrencyBeneficiary("GBP");
		 accountsPage.enterYouSendAmount();
		 accountsPage.selectInterledgerPaymentScheme();
		 accountsPage.enterConcept();
		 accountsPage.clickOnImmediatePayment();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterNotifyByEmail();
		 accountsPage.clickOnConfirmButton();
		 accountsPage.enterSecurityCode();

	 }
	 
	 public void verifyAccountantCanNavigateThroughTheAccountsPage() throws InterruptedException {
		 test = extent.createTest("TOQ-587:Verify accounts page navigation is successful when logged in as an accountant user");
		 //accountsPage.loginAsAnAccountant();
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkAddAccountIsNotDisplayed();
	 }
	
	 public void verifyNewCancelAndCreateAccountPermissionsSMEwebWereAdded() throws InterruptedException {
		 test = extent.createTest("TOQ-736:Verify that new cancel and create account permissions SME web were added and are handled");
		 //accountsPage.loginAsAmanager();
		 accountsPage.clickOnAccountsMenuOption();
		 accountsPage.checkAddAccountIsNotDisplayed();
	 }

	 
	 
	 
	 
	 
	@AfterTest
	public void tearDown()
	{ 	
		accountsPage.tearDown();
	}
	 

	 
	 //@AfterTest
		public void shutDown() {
			driver.quit();
		}



}
