package com.consumerweb.Testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.UserRegistrationPage;

public class UserRegistrationPageTest extends ConsumerWebBaseClass{
	UserRegistrationPage userRegistrationPage;
	
 
	
	  @BeforeClass
	    public void setup() throws Exception{
		  init();
		  userRegistrationPage = new UserRegistrationPage(driver);
	    }
	  
		@Test(priority = 0)
		public void verifyUserCanRegister() throws InterruptedException {
			System.out.println("Started registering a user");
			test = extent.createTest("USER REGISTRATION PAGE :Verify User can register");
			userRegistrationPage.clickOnNoAccountYetRegisterHere();
           //Select a country		
			userRegistrationPage.selectAcountry();
			userRegistrationPage.clickOnContinue();
			// Add personal information
			userRegistrationPage.enterFirstName("Selpha");
			userRegistrationPage.enterSurName("Atemba");
			userRegistrationPage.enterDateOfBirth("01/01/2000");
			userRegistrationPage.enterEmail("selpharr@gmail.com");
			userRegistrationPage.enterPhonePrefix();
			userRegistrationPage.enterPhoneNumber("0706410315");
			userRegistrationPage.clickOnContinueButton();
			//Add user address information
			userRegistrationPage.selectAcountry1();
			userRegistrationPage.enterAddress("Test Address");
			userRegistrationPage.enterCity("Test City");
			userRegistrationPage.enterPostalCode("1234");
			userRegistrationPage.clickOnPrivacyPolicy();
			userRegistrationPage.clickOnTheFinalContinueButton();
			System.out.println("Completed registering a user");
		}
		
		@AfterTest
		public void shutDown() {
			driver.quit();
		}
   }
