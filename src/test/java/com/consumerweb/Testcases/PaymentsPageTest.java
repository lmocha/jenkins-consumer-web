package com.consumerweb.Testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PaymentsPage;
import com.consumerweb.Pages.SyncomPage;

public class PaymentsPageTest extends ConsumerWebBaseClass {
	LoginPage loginPage;
	AccountsPage accountsPage;
	PaymentsPage paymentsPage;
	SyncomPage syncomPage;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 paymentsPage = new PaymentsPage(driver);
		 accountsPage = new AccountsPage(driver);
		 syncomPage = new SyncomPage(driver);
	        
	    }
	 
	 
	@Test(priority = 0)
	public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("Payments PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifyUserCanAccessPaymentsManagementPage() {
		    test = extent.createTest("TOQ 206 :As a user with permissions I want to have a Payments sub-dashboard to have an overview and access to all the related actions to my payments");
		    paymentsPage.clickOnPayments();
		    paymentsPage.assertPaymentsPageIsDisplayed();

	 }
	 @Test(priority = 2)
	 public void verifyAmountAndDetailsPageInNewPaymentForm() {
		 test = extent.createTest("TOQ-612:Verify we display available funds in Payment form");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.assertAvailableBalanceIsDisplayed();
	 }
	 @Test(priority = 3)
	 public void verifyAuserCanChooseOriginAccount() throws InterruptedException {
		 test = extent.createTest("TOQ-382:FE: Verify that as a user i can choose the origin account(pay from) Consumer web");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.modifyPayFrom("£");	 
	 }
	 
	 @Test(priority = 4)
	 public void verifyCopyUpdatesRelatedToMultiaccount() throws InterruptedException {
		 test = extent.createTest("TOQ-452:Verify whether Copy updates related to multi-account are updated");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.clickOnChooseAnExistingBeneficiary();
		 paymentsPage.SelectRandomBeneficiary();
		 paymentsPage.assertPayFromIsDisplayed();
		 paymentsPage.assertPayToIsDisplayed();
		 paymentsPage.assertAmountAndDetailsIsDisplayed();
	 }
	 @Test(priority = 5)
	 public void verifysBenFirstAndLastNameDoesNotAcceptSpecialCharacters() throws InterruptedException {
		 test = extent.createTest("TOQ-749:Verify whether correct error message is shown in the First Name and Last Name fields in the Individual beneficiary creation form if special characters are provided");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewBeneficiaryIcon();
		 paymentsPage.enterFirstNameWithSpecialChars();
		 paymentsPage.enterLastNameWithSpecialChars();
		 paymentsPage.assertErrorOnName();
		 paymentsPage.assertErrorOnSurName();
		 paymentsPage.clickOnAButton("Continue");
	 }
	 
     @Test(priority = 6)
     public void verifyPaymentStatementIsAttachedToAPayment() throws Exception {
		 test = extent.createTest("TOQ-173:As a user I MUST see a payment statement document attached to a payment");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnAPayment();
		 paymentsPage.assertPaymentStatementIsAttachedToApayment();
   }
   @Test(priority = 7)
   public void verifyUserCanSeeUpdatedBeneficiaryDetail() throws InterruptedException {
		 test = extent.createTest("TOQ-172:As a user I want to see an updated beneficiary detail");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnBeneficiaries();
		 paymentsPage.clickOnRandomBeneficiary();
		 paymentsPage.assertBeneficiaryDetailsAreWellDisplayed();
   }
    public void verifyUserCanViewFailedPayments() throws Exception{
		 test = extent.createTest("TOQ-178:Verify user can view Failed payments list");
		 paymentsPage.clickOnPayments();
		 paymentsPage.assertFailedPaymentsAreDisplayed();
    }
	
   public void verifyUserCanViewScheduledOutboundPayments() throws Exception{
		 test = extent.createTest("TOQ-183:As a user I want to have an overview on the schedule outbound payments");
		 paymentsPage.clickOnPayments();
		 paymentsPage.assertScheduledPaymentsAreDisplayed();
	
	 }
   public void verifyUserCanViewRecentPayments() throws Exception{
		 test = extent.createTest("TOQ-205:FE: Payments dashboard - RECENT PAYMENTS section (Consumer app)");
		 paymentsPage.clickOnPayments();
		 paymentsPage.assertRecentPaymentsAreDisplayed();
	
	 }
   @Test(priority = 11)
   public void verifyUserCanSelectPaymentSchemeForSameCurrencyPayments() throws Exception{
		 test = extent.createTest("TOQ-64:Verify that for all payments in the same currency they have the capability of select the payment scheme available in their partner product");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnNewPaymentIcon();
		 paymentsPage.modifyPayFrom("£");	
		 syncomPage.chooseAnExistingBeneficiary();
		 paymentsPage.selectGivenCurrencyBeneficiary("GBP");
		 accountsPage.enterYouSendAmount();
		 accountsPage.selectInterledgerPaymentScheme();
		 accountsPage.enterConcept();    
	 }

   public void verifyUserCanMakePaymentForRBCustomersWithFXoN() throws InterruptedException {
		 test = extent.createTest("TOQ-65,TOQ-210,TOQ-212,TOQ-214:Verify that for RAILSBANK customers with Fx on, they should be able to make Fx payments successfully");
		 paymentsPage.makeApayment();

   }
	@Test(priority = 13)
	public void verifyUserCanCreateSpanishBeneficiary() throws Exception{
			test = extent.createTest("TOQ-174,TOQ-303:As a user I MUST be able of creating a beneficiary");
			 paymentsPage.clickOnPayments();
			 paymentsPage.clickOnNewBeneficiaryIcon();
			 paymentsPage.clickOnCompanyCombobox();
			 paymentsPage.enterCompanyName();
			 paymentsPage.selectEURCurrency();
			 paymentsPage.enterSpainResidenceCountry();
			 paymentsPage.enterCity();
			 paymentsPage.enterAddress();
			 paymentsPage.enterPostalCode();
			 paymentsPage.clickOnAButton("Continue");
			 paymentsPage.enterIBAN();
			 paymentsPage.clickOnBeneficiarySaveButton();
			 paymentsPage.deleteBeneficiary();
			 paymentsPage.acceptDeleteBeneficiary();
		 }
    @Test(priority = 14)
	public void verifyUserCanCreateUKBeneficiary() throws Exception{
			test = extent.createTest("TOQ-306,TOQ-297,TOQ-316:Verify in Beneficiary creation: If currency is GBP and bank country is UK show Account Number and Sort Code, regardless of the beneficiary address");
			 paymentsPage.clickOnPayments();
			 paymentsPage.clickOnNewBeneficiaryIcon();
			 paymentsPage.clickOnCompanyCombobox();
			 paymentsPage.enterCompanyName();
			 paymentsPage.selectGBPCurrency();
			 paymentsPage.enterUKResidenceCountry();
			 paymentsPage.enterCity();
			 paymentsPage.enterAddress();
			 paymentsPage.enterPostalCode();
			 paymentsPage.clickOnAButton("Continue");
			 paymentsPage.enterAccountNumber();
			 paymentsPage.enterSortCode();
			 paymentsPage.clickOnBeneficiarySaveButton();
			 paymentsPage.deleteBeneficiary();
			 paymentsPage.acceptDeleteBeneficiary();
		 }
	@Test(priority = 15)
	public void verifyTransactionDetailHasPaymentScheme() throws InterruptedException {
		test = extent.createTest("TOQ-213:As a user I MUST be shown the payment scheme in a transaction detail");
		paymentsPage.clickOnPayments();
		paymentsPage.clickOnAPayment();
		paymentsPage.assertPaymentSchemeIsDisplayed();
	}
	@Test(priority = 16) 
	 public void verifyUserCanViewBeneficiaryList() throws InterruptedException {
		 test = extent.createTest("TOQ-216:Verify user can view beneficiary list");
		 paymentsPage.clickOnPayments();
		 paymentsPage.clickOnBeneficiaries();
		 paymentsPage.viewBeneficiaryList();

	 } 
	 
	 @AfterTest
	 public void shutDown() {
			driver.quit();
		}


}
