package com.consumerweb.Testcases;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PersonalSettingsPage;

public class PersonalSettingsPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	AccountsPage accountsPage;
	PersonalSettingsPage personalSettionsPage;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 personalSettionsPage = new PersonalSettingsPage(driver);
		 accountsPage = new AccountsPage(driver);
	    }
	 
	 
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifyPersonalDetailsIsDisplayedCorrectly() throws InterruptedException {
		 test = extent.createTest("Verify search page is displayed correctly");
		 personalSettionsPage.clickOnPersonalSettings();
		 personalSettionsPage.clickOnPersonalDetails();
		 personalSettionsPage.listCardsThatAuserHas();
	 }
	 //@Test(priority = 2)
	 public void verifyUserCanEditProfilePicture() throws Exception{
		 test = extent.createTest("Verify user can edit profile picture");
		 personalSettionsPage.uploadProfilePic();
		 
	 }
	 @Test(priority = 3)
	 public void verifyUserCanViewTransactions() throws Exception{
		 test = extent.createTest("Verify user can view their transactions");
		 personalSettionsPage.clickOnTransactionsButton();
		 personalSettionsPage.displayUserTransactions();		 
	 }
	 @Test(priority = 4)
	 public void verifyUserCanEditPersonalDetails() throws Exception{
		 test = extent.createTest("TOQ-109:Verify user can access User settings");
		 personalSettionsPage.clickOnBackButton();
		 personalSettionsPage.clickOnEditButton();
		 personalSettionsPage.enterSurname();
		 personalSettionsPage.clickOnSaveButton();
		 personalSettionsPage.closeVerificationWindow();
	 }
	 @Test(priority = 5)
	 public void verifyUserCanEditNotificationsSettings() throws Exception{
		 test = extent.createTest("TOQ-112:Verify user can edit notifications settings");
		 personalSettionsPage.clickOnPersonalSettings();
		 personalSettionsPage.clickOnNotificationSetting();
		 personalSettionsPage.toggleEmailNotifications();
		 personalSettionsPage.clickOnSaveButton();
	 }
	 @Test(priority = 6)
	 public void verifyUserCanChangeSecurityCode() throws Exception{
		 test = extent.createTest("TOQ-113:Verify user can change security code");
		 personalSettionsPage.changeSecurityCode();
	 }
	 @Test(priority = 7)
	 public void verifyUserCanChangePassword() throws Exception{
		 test = extent.createTest("TOQ-114:Verify user can change password");
		 personalSettionsPage.changePassword();
	 }
	 @Test(priority = 8)
	 public void verifyUserCanChangeLanguage() throws Exception{
		 test = extent.createTest("Verify user can change Language");
		 personalSettionsPage.clickOnPersonalSettings();
		 personalSettionsPage.clickOnLanguage();
		 personalSettionsPage.selectSpanish();
		 personalSettionsPage.clickOnSaveButton();
		 personalSettionsPage.assertLanguageIsChangedToSpanish();
		 personalSettionsPage.selectEnglish();
		 personalSettionsPage.saveLanguageSelection();
		 
	 }
	  //@AfterTest
	 public void shutDown() {
			driver.quit();
		}

}
