package com.consumerweb.Testcases;

import java.util.ArrayList;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;
import com.consumerweb.Pages.PersonalSettingsPage;
import com.consumerweb.Pages.TranslationsPage;

public class TranslationsPageTest extends ConsumerWebBaseClass{
	TranslationsPage translationsPage;
	LoginPage loginPage;
	
	
	
	
	@BeforeClass
	    public void setup() throws Exception {
		    init();
			loginPage = new LoginPage(driver);
			translationsPage = new TranslationsPage(driver);
	    }
	
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("TRANSLATIONS PAGE :Verify successful login");
		    login();
		}
	    @Test(priority = 1)
        public void verifyEnglishTranslationsForPricingModelsIsCorrect() throws InterruptedException {
    	    test = extent.createTest("Verify English Translations For Pricing Models Is Correct");
    	    translationsPage.clickOnPricing("Pricing");
    	    translationsPage.switchToTheNextWindow();
    	    translationsPage.verifyEnglishTranslations();
    	    translationsPage.closeBrowserAndSwitchToWindowOne();

    	
    }
	    @Test(priority = 2)
		public void verifySpanishTranslationsForPricingModelsIsCorrect() throws InterruptedException {
		    test = extent.createTest("Verify Spanish Translations For Pricing Models Is Correct");
		    translationsPage.clickOnpersonalSettings();
		    translationsPage.clickOnLanguage();
		    translationsPage.selectLanguage("Español");
		    translationsPage.clickOnSave("Save");
		    translationsPage.clickOnPricing("Precios");
		    translationsPage.switchToTheNextWindow();
		    translationsPage.verifySpanishTranslations();
		    translationsPage.closeBrowserAndSwitchToWindowOne();
		    
		    
			
	 }

	    @Test(priority = 3)
	    public void verifyGermanTranslationsForPricingModelsIsCorrect() throws Exception{
	    	test = extent.createTest("Verify Germany Translations For Pricing Models Is Correct");
	    	translationsPage.selectLanguage("Deutsch");
	    	translationsPage.clickOnSave("Guardar");
	    	translationsPage.clickOnPricing("Preisgestaltung");
	    	translationsPage.switchToTheNextWindow();
	    	translationsPage.verifyGermanTranslations();
	    	translationsPage.closeBrowserAndSwitchToWindowOne();
    	    }
	   
	   @Test(priority = 4)
	  public void changeTheLanguageBackToEnglish() throws Exception{
		    test = extent.createTest("Change Language back to English");
	    	translationsPage.selectLanguage("English");
	    	translationsPage.clickOnSave("Speichern");
	    	translationsPage.verifyTranslationsToEnglish();

		  
	  }
	  
	  @AfterTest
	  public void shutDown() {
				driver.quit();
			}
	  


}
