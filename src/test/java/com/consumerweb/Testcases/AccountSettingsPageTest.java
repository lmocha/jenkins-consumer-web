package com.consumerweb.Testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.consumerweb.Pages.AccountSettingsPage;
import com.consumerweb.Pages.AccountsPage;
import com.consumerweb.Pages.ConsumerWebBaseClass;
import com.consumerweb.Pages.LoginPage;

public class AccountSettingsPageTest extends ConsumerWebBaseClass{
	LoginPage loginPage;
	AccountsPage accountsPage;
	AccountSettingsPage accountSettingsPage;
	String accountSettingsTitle,accountAlias,accountHolderName,AccountHolderAddress,Address2,poatalCode,city,country;
	Boolean saveButtonIsDisplayed;
	WebElement contentHolder;
	
	
	 @BeforeClass
	    public void setup() throws Exception{
		 init();
		 loginPage = new LoginPage(driver);
		 accountSettingsPage = new AccountSettingsPage(driver);
		 accountsPage = new AccountsPage(driver);
	 }
	 
	 
	 @Test(priority = 0)
		public void verifySuccessfulLogin() throws InterruptedException {
		    test = extent.createTest("EXPENSES PAGE :Verify successful login");
		    login();
		}
	 @Test(priority = 1)
	 public void verifyAccountSettingsPageIsDisplayedCorrectly() throws InterruptedException {
		 test = extent.createTest("Verify search page is displayed correctly");
		 accountSettingsPage.clickOnAccountSettings();
		 accountSettingsTitle = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#app-container-body > div > div > div > div.display-flex.title-container.m-y-2 > div"))).getText();
		 Assert.assertEquals(accountSettingsTitle, "Account settings");
		 accountAlias = wait.until(ExpectedConditions.presenceOfElementLocated(By.name("alias"))).getText();
		 test.log(Status.INFO, "Account Alias is  "+accountAlias);
		 accountHolderName = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(2) > div:nth-child(1) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "Account Holder name is  "+accountHolderName);
		 AccountHolderAddress = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(2) > div:nth-child(2) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "Account holder address is.."+AccountHolderAddress);
		 Address2 = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(3) > div:nth-child(1) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "Address 2 is..."+ Address2);
		 poatalCode = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(3) > div:nth-child(2) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "The postal code is  "+poatalCode);
		 city = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(4) > div:nth-child(1) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "The city is   "+city);
		 country = wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#box-container > form > div > div:nth-child(4) > div:nth-child(2) > div.info.lum-fs-16"))).getText();
		 test.log(Status.INFO, "The country is  "+country);
		 saveButtonIsDisplayed = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//button[contains(text(),'Save')]"))).isDisplayed();
		 Assert.assertTrue(saveButtonIsDisplayed);
	 }
	
	 
	    @AfterTest
		public void shutDown() {
			driver.quit();
		}
}
